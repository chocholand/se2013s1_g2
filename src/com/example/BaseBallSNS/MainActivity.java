/** MainActivity.java
 * @author 2조 돌직구
 * @version ver 1.0
 * @since 2013.4.18
 */
package com.example.BaseBallSNS;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.BaseBallSNS.R;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapView;
/**
 * @version ver 1.0
 * @since 2013.4.18
 * @author 2조 돌직구
 * @author LeeJungsu
 * @param  m_hHandler : Handler Type.
 * @param m_bFlag : boolean Type.
 * @param kbutton4,kbutton5 : ImageButton.
 */
public class MainActivity extends Activity implements OnClickListener {
	private boolean m_bFlag = false;
	private Handler m_hHandler;
	private ImageButton kbutton,sbutton,lbutton,skbutton,doosanbutton,ncbutton,lgbutton,hanwhabutton,heroesbutton ;
	public static MediaPlayer player=null;
    

    /**void onCreate(Bundle savedInstanceState) 
     * Called when the activity is first created.
     */
	@Override
	 protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        startActivity(new Intent(this, StartLogo.class));
        kbutton = (ImageButton)findViewById(R.id.kimagebutton);//기아 이미지 버튼 생성
        kbutton.setOnClickListener(this);
        //sbutton = (ImageButton)findViewById(R.id.lionsbutton);//삼성 이미지 버튼 생성
       // sbutton.setOnClickListener(this);
        lbutton = (ImageButton)findViewById(R.id.lottebutton);
        lbutton.setOnClickListener(this);
        sbutton = (ImageButton)findViewById(R.id.lionsbutton);
        sbutton.setOnClickListener(this);
        skbutton = (ImageButton)findViewById(R.id.wyverns);
        skbutton.setOnClickListener(this);
        doosanbutton = (ImageButton)findViewById(R.id.bears);
        doosanbutton.setOnClickListener(this);
        ncbutton = (ImageButton)findViewById(R.id.nc);
        ncbutton.setOnClickListener(this);
        lgbutton = (ImageButton)findViewById(R.id.lg);
        lgbutton.setOnClickListener(this);
        hanwhabutton = (ImageButton)findViewById(R.id.hanwha);
        hanwhabutton.setOnClickListener(this);
        heroesbutton = (ImageButton)findViewById(R.id.heroes);
        heroesbutton.setOnClickListener(this);
        
	
	m_hHandler = new Handler() {
    	
    	public void handleMessage(Message msg) {
			if(msg.what == 0)
			{
				m_bFlag = false;
			}
    	}
	};	
	}
	/** onClick(View v)
	 * Setting click of ImageButton
	 * @param myIntent : Intent Type.
	 * @param myIntent2 : Intent Type. 
	 * @return void
	 */
	//이미지 버튼 클릭시 Activity 들이 작동.
    	public void onClick(View v) {
    		int id = v.getId();//
    		switch(id){
    		case R.id.kimagebutton://기아 버튼이 눌렸을때 기아 레이아웃으로 이동한다.
    		Intent myIntent = new Intent(this,KiaLayout.class);
    		startActivity(myIntent);
    		break;
    		
    		case R.id.lottebutton://롯데 버튼을 눌러을때 삼성 레이아웃으로 이동한다.
    		Intent myIntent2 = new Intent(this,LotteLayout.class);
    		startActivity(myIntent2);
    		break;
    		
    		case R.id.lionsbutton://삼성 버튼을 눌러을때 삼성 레이아웃으로 이동한다.
    		Intent myIntent3 = new Intent(this,SamSungLayout.class);
    		startActivity(myIntent3);
    		break;
    		
    		case R.id.wyverns://sk 버튼을 눌러을때 삼성 레이아웃으로 이동한다.
    		Intent myIntent4 = new Intent(this,SKLayout.class);
    		startActivity(myIntent4);
    		break;
    		
    		case R.id.bears://두산 버튼을 눌러을때 삼성 레이아웃으로 이동한다.
    		Intent myIntent5 = new Intent(this,doosanLayout.class);
    		startActivity(myIntent5);
    		break;
    		
    		case R.id.nc://nc 버튼을 눌러을때 삼성 레이아웃으로 이동한다.
    		Intent myIntent6 = new Intent(this,ncLayout.class);
    		startActivity(myIntent6);
    		break;
    		default:
    			break;
    		}
	
    	}
    	public boolean onKeyDown(int KeyCode, KeyEvent event){	
    		super.onKeyDown(KeyCode, event);
    		if(event.getAction() == KeyEvent.ACTION_DOWN) {	
    			switch(KeyCode) {
    			case KeyEvent.KEYCODE_BACK:	// `뒤로` 키와 같은 기능을 한다.		
    				if(!m_bFlag) {
    					Toast.makeText(getApplicationContext(), "뒤로 버튼을 한번 더 누르시면 종료됩니다",     Toast.LENGTH_SHORT).show();
    					m_bFlag = true;
    					m_hHandler.sendEmptyMessageDelayed(0, 2000);
    					return false;
    				}
    				else
    				{	
    					if(player!=null){
    						player.release();
    						player =null;
    					}
    					finish();//Avtivity 를 종료한다.
    				}
    				return true;
    			}
    		return false;
    		}
    		return m_bFlag;
    	
    	
    	}
    	protected boolean isRouteDisplayed() {
    		// TODO Auto-generated method stub
    		return false;

    	}

    	

    }

