package com.example.BaseBallSNS;

import java.util.*;
import java.util.zip.Inflater;

import com.example.BaseBallSNS.R;
import com.google.android.maps.*;

import android.app.*;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import android.location.*;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.*;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.*;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.graphics.drawable.Drawable;

import com.google.android.maps.*;
import com.google.android.maps.MapView.LayoutParams;

/**
 * @author LeeJungsu
 * @param kiaSelect
 *            : boolean Type.
 */

public class KiaLayout extends MapActivity implements OnClickListener {
	View top, TimeLine2, menu2, menu3;
	ListView listview;
	MapView mapView; // //맵뷰 객체
	MapController mc;
	GeoPoint kiapoint,kiapoint2,kiapoint3,kiapoint4,kiapoint5,kiapoint6,kiapoint7,kiapoint8,kiapoint9,kiapoint10,kiapoint11,kiapoint12,kiapoint13;
	List<Overlay> kialist; // 맵 오버레이 포함하는 리스트
	EditText comment;
	ArrayList<MyItem> arItem;
	private ArrayList<HashMap<String, String>> data;
	private Button bt_open;
	private ListView listview2;
	private SimpleAdapter sa;
	private Net_HTMLParse hp;
	public static HashMap<String, String> kiamap = new HashMap<String, String>();
	private final Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			listUpdate();
		}
	};

	public static boolean kiaSelect = true;

	@Override
	/**
	 * onCreate()
	 * 화면 생성 함수, void type
	 * @author Lee Jung Su
	 * @param Bundle savedInstanceState
	 */
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.kia);

		// top = findViewById(R.id.top);
		// top.setVisibility(View.INVISIBLE);
		mapView = (MapView) findViewById(R.id.mapview);
		mapView.setVisibility(View.INVISIBLE);
		// TimeLine2 = findViewById(R.id.list);
		// TimeLine2.setVisibility(View.INVISIBLE);
		menu2 = findViewById(R.id.menu2);
		menu2.setVisibility(View.INVISIBLE);
		menu3 = findViewById(R.id.menu3);
		menu3.setVisibility(View.INVISIBLE);
		listview = (ListView) findViewById(R.id.list);
		listview.setVisibility(View.INVISIBLE);
		listview2 = (ListView) findViewById(R.id.list2);
		listview2.setVisibility(View.VISIBLE);

		// hp.open();

		data = new ArrayList<HashMap<String, String>>();

		// bt_open = (Button)findViewById(R.id.bt_open);

		hp = new Net_HTMLParse(this, handler, data);


		sa = new SimpleAdapter(this, data, R.layout.homeview, new String[] {
				"date", "team1", "score","team2","time","where" }, new int[] { R.id.date, R.id.team1,
				R.id.score, R.id.team2, R.id.time, R.id.where });

		// listview2.setOnClickListener(listener);
		listview2.setAdapter(sa);
        hp.open(389);
		/* 홈버튼 */
		ImageButton kbutton1 = (ImageButton) findViewById(R.id.kbutton1);
		kbutton1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				listview.setVisibility(View.INVISIBLE);
				mapView.setVisibility(View.INVISIBLE);
				// top.setVisibility(View.INVISIBLE);
				menu2.setVisibility(View.INVISIBLE);
				menu3.setVisibility(View.INVISIBLE);

				hp.open(389);

				listview2.setVisibility(View.VISIBLE);
				// startActivity(myIntent);
			}
		});
		TeamAudio Kiaaudio = new TeamAudio();
		arItem = new ArrayList<MyItem>();
		MyItem mi;
		mi = new MyItem(R.drawable.kiasun, "선동열", "감독", "1984년 12월19일",
				"184cm / 79kg",kiamap.get("선동열"),"You are my sunshine~ 광주의 태양!" +
						"기아의 선동열! 오오오오~" +
						"You are my sunshine~ 광주의 태양!" +
						"기아의 선동열 감독님!");
		arItem.add(mi);
		mi = new MyItem(R.drawable.kiaplayer1, "양현종", "투수", "1986년 07월24일",
				"184cm / 85kg",kiamap.get("양현종"),"가사가사가사");
		arItem.add(mi);
		mi = new MyItem(R.drawable.kiaplayer2, "윤석민", "투수", "1999년 12월19일",
				"184cm / 79kg",kiamap.get("윤석민"),"가사가사가사");
		arItem.add(mi);
		mi = new MyItem(R.drawable.kiaplayer3, "헨리 소사", "투수", "1985년 07월28일",
				"186cm / 95kg",kiamap.get("헨리 소사"),"가사가사가사");
		arItem.add(mi);
		mi = new MyItem(R.drawable.kiaplayer4, "서재응", "투수", "1977년 05월24일",
				"181cm / 97kg",kiamap.get("선동열"),"가사가사가사");
		arItem.add(mi);
		mi = new MyItem(R.drawable.kiaplayer5, "김진우", "투수", "1983년 03월 07일",
				"193cm / 95kg",kiamap.get("선동열"),"가사가사가사");
		arItem.add(mi);
		mi = new MyItem(R.drawable.kiaplayer10, "차일목", "포수", "1981년 01월 26일",
				"176cm / 81kg",kiamap.get("선동열"),"가사가사가사");
		arItem.add(mi);
		mi = new MyItem(R.drawable.kiaplayer6, "홍재호", "내야수", "1987년 05월 10일",
				"179cm / 77kg",kiamap.get("선동열"),"가사가사가사");
		arItem.add(mi);
		mi = new MyItem(R.drawable.kiaplayer7, "고영우", "내야수", "1990년 01월 08일",
				"183cm / 80kg",kiamap.get("선동열"),"가사가사가사");
		arItem.add(mi);
		mi = new MyItem(R.drawable.kiaplayer8, "이범호", "내야수", "1981년 11월 25일",
				"183cm / 93kg",kiamap.get("선동열"),"가사가사가사");
		arItem.add(mi);
		mi = new MyItem(R.drawable.kiaplayer12, "최희섭", "내야수", "1979년 03월 16일",
				"196cm / 123kg",kiamap.get("최희섭"),"최희섭~ 오오오오~ 최희섭~ 오오~ (방망이를 머리 위에서 크게 돌리며)" +
						"최희섭~ 오오오오~ 최희섭~ 오오~ (방망이를 머리 위에서 크게 돌리며)" +
						"빅초이! 오오오오오오오오~ 빅초이! 오오오오오오오오~" +
						"빅초이! 오오오오오오오오~ 타.이.거.즈. 빅.초이!");
		arItem.add(mi);
		mi = new MyItem(R.drawable.kiaplayer13, "김선빈", "내야수", "1989년 12월 18일",
				"165cm / 70kg",kiamap.get("선동열"),"가사가사가사");
		arItem.add(mi);
		mi = new MyItem(R.drawable.kiaplayer9, "신종길", "외야수", "1983년 12월 31일",
				"183cm / 85kg",kiamap.get("선동열"),"가사가사가사");
		arItem.add(mi);
		mi = new MyItem(R.drawable.kiaplayer11, "이용규", "외야수", "1985년 08월 26일",
				"175cm / 70kg",kiamap.get("선동열"),"가사가사가사");
		arItem.add(mi);
		mi = new MyItem(R.drawable.kiaplayer14, "나지완", "외야수", "1985년 05월 19일",
				"182cm / 95kg",kiamap.get("선동열"),"가사가사가사");
		arItem.add(mi);
		mi = new MyItem(R.drawable.kiaplayer15, "안치홍", "내야수", "1990년 07월 02일",
				"178cm / 80kg",kiamap.get("선동열"),"가사가사가사");
		arItem.add(mi);
		MyListAdapter MyAdapter = new MyListAdapter(this, R.layout.kiaplayer,
				arItem);

		ListView MyList;
		MyList = (ListView) findViewById(R.id.list);
		MyList.setAdapter(MyAdapter);

		/** 선수정보 버튼 */
		ImageButton kbutton2 = (ImageButton) findViewById(R.id.kbutton2);
		kbutton2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				listview.setVisibility(View.VISIBLE);
				mapView.setVisibility(View.INVISIBLE);
				// top.setVisibility(View.INVISIBLE);
				menu2.setVisibility(View.INVISIBLE);
				menu3.setVisibility(View.INVISIBLE);

				// startActivity(myIntent);
			}
		});

		/** 트위터 버튼 */
		ImageButton kbutton3 = (ImageButton) findViewById(R.id.kbutton3);
		kbutton3.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				menu2.setVisibility(View.VISIBLE);
				menu3.setVisibility(View.VISIBLE);
				mapView.setVisibility(View.INVISIBLE);
				listview.setVisibility(View.INVISIBLE);
				listview2.setVisibility(View.INVISIBLE);
				// TimeLine2.setVisibility(View.INVISIBLE);

			}
		});
		/** 트위터 로그인버튼 */
		ImageButton button = (ImageButton) findViewById(R.id.tlogin);
		button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent myIntent = new Intent(KiaLayout.this,
						TwitterActivity.class);
				startActivityForResult(myIntent, 1004);
				// startActivity(myIntent);
			}
		});
		/** 트위터 글쓰기버튼 */
		final HashTag hash = new HashTag("");
		ImageButton button1 = (ImageButton) findViewById(R.id.tweet);
		comment = (EditText) findViewById(R.id.comment);
		button1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				String commentString = comment.getText().toString();
				if (STATICVALUES.token != null
						&& STATICVALUES.tokenSecret != null) {
					RegistAsyncTask registAsyncTask = new RegistAsyncTask(
							KiaLayout.this);
					registAsyncTask.execute(STATICVALUES.token,
							STATICVALUES.tokenSecret, commentString);
				} else {
					Toast.makeText(KiaLayout.this, "로그인해주세요!",
							Toast.LENGTH_SHORT).show();
				}
				comment.setText("");

			}
		});
		CheckBox Rbutton1 = (CheckBox) findViewById(R.id.checkBox1);
		Rbutton1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				ImageButton button1 = (ImageButton) findViewById(R.id.tweet);
				button1.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						hash.ResultTag = "#기아타이거즈";
						String commentString = hash.MakeHash("#기아타이거즈") + " "
								+ comment.getText().toString();
						if (STATICVALUES.token != null
								&& STATICVALUES.tokenSecret != null) {
							RegistAsyncTask registAsyncTask = new RegistAsyncTask(
									KiaLayout.this);
							registAsyncTask.execute(STATICVALUES.token,
									STATICVALUES.tokenSecret, commentString);
						} else {
							Toast.makeText(KiaLayout.this, "로그인해주세요!",
									Toast.LENGTH_SHORT).show();
						}
						comment.setText("");
					}
				});
			}
		});
		CheckBox Rbutton2 = (CheckBox) findViewById(R.id.checkBox2);
		Rbutton2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				ImageButton button1 = (ImageButton) findViewById(R.id.tweet);
				button1.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						hash.ResultTag = "#기아타이거즈2";
						String commentString = hash.ResultTag + " "
								+ comment.getText().toString();
						if (STATICVALUES.token != null
								&& STATICVALUES.tokenSecret != null) {
							RegistAsyncTask registAsyncTask = new RegistAsyncTask(
									KiaLayout.this);
							registAsyncTask.execute(STATICVALUES.token,
									STATICVALUES.tokenSecret, commentString);
						} else {
							Toast.makeText(KiaLayout.this, "로그인해주세요",
									Toast.LENGTH_SHORT).show();
						}
						comment.setText("");
					}
				});
			}
		});
		ImageButton tbutton = (ImageButton) findViewById(R.id.tweet2);
		tbutton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				menu2.setVisibility(View.VISIBLE);
				menu3.setVisibility(View.VISIBLE);

			}
		});

		ImageButton Timebutton = (ImageButton) findViewById(R.id.timeline);
		Timebutton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// menu2.setVisibility(View.VISIBLE);
				// menu3.setVisibility(View.VISIBLE);
				timeline.searchTerm="#기아타이거즈";
				Intent intent = new Intent(KiaLayout.this, timeline.class);
				startActivity(intent);
				// top.setVisibility(View.INVISIBLE);
				// menu2.setVisibility(View.VISIBLE);
				// mapView.setVisibility(View.INVISIBLE);
				// TimeLine2.setVisibility(View.VISIBLE);
				// listview.setVisibility(View.INVISIBLE);

			}
		});

		/** 구글 맵 버튼 */
		ImageButton kbutton4 = (ImageButton) findViewById(R.id.kbutton4);
		kbutton4.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// top.setVisibility(View.INVISIBLE);
				menu2.setVisibility(View.INVISIBLE);
				menu3.setVisibility(View.INVISIBLE);
				mapView.setVisibility(View.VISIBLE);
				mapView.setBuiltInZoomControls(true);// zoom controller
				mc = mapView.getController();

				if (KiaLayout.kiaSelect == true) {
					kiapoint = new GeoPoint(35168809, 126887063);// 기아 경기장 좌표 설정
					mc.animateTo(kiapoint);// 좌표점 이동
					mc.setZoom(18);// 확대 1~21

					kialist = mapView.getOverlays();
					// 위치에 이미지 설정한다.
					Drawable drawable = getResources().getDrawable(
							R.drawable.ex2);
					OverlayItem item = new OverlayItem(kiapoint, "광주무등야구장",
							"기아타이거즈");
					MapOverlay map = new MapOverlay(drawable,KiaLayout.this);
					map.addItem(item);
					kialist.add(map);

					kiapoint2 = new GeoPoint(35169178,126886307);// 기아 경기장 좌표
					// 설정
					Drawable drawable2 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item2 = new OverlayItem(kiapoint2, "맛집",
							"장충동왕족발");
					MapOverlay map2 = new MapOverlay(drawable2,KiaLayout.this);
					map2.addItem(item2);
					kialist.add(map2);

					kiapoint3 = new GeoPoint(35169588,12688);// 기아 경기장 좌표
					// 설정
					Drawable drawable3 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item3 = new OverlayItem(kiapoint3, "맛집",
							"족발보쌈전문");
					MapOverlay map3 = new MapOverlay(drawable3,KiaLayout.this);
					map3.addItem(item3);
					kialist.add(map3);

					kiapoint4 = new GeoPoint(35169500,126884743);// 기아 경기장 좌표
					// 설정
					Drawable drawable4 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item4 = new OverlayItem(kiapoint4, "맛집",
							"양동사장통닭");
					MapOverlay map4 = new MapOverlay(drawable4,KiaLayout.this);
					map4.addItem(item4);
					kialist.add(map4);

					kiapoint5 = new GeoPoint(35169553,126884676);
					Drawable drawable5 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item5 = new OverlayItem(kiapoint5, "맛집",
							"운암고가식당");
					MapOverlay map5 = new MapOverlay(drawable5,KiaLayout.this);
					map5.addItem(item5);
					kialist.add(map5);

					kiapoint6 = new GeoPoint(35169706,126884671);
					Drawable drawable6 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item6 = new OverlayItem(kiapoint6, "맛집",
							"통묵은지감자탕");
					MapOverlay map6 = new MapOverlay(drawable6,KiaLayout.this);
					map6.addItem(item6);
					kialist.add(map6);

					kiapoint7 = new GeoPoint(35168244,12688410);
					Drawable drawable7 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item7 = new OverlayItem(kiapoint7, "맛집",
							"남원자장");
					MapOverlay map7 = new MapOverlay(drawable7,KiaLayout.this);
					map7.addItem(item7);
					kialist.add(map7);

					kiapoint8 = new GeoPoint(35169588,126889732);
					Drawable drawable8 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item8 = new OverlayItem(kiapoint8, "맛집",
							"원점해장국");
					MapOverlay map8 = new MapOverlay(drawable8,KiaLayout.this);
					map8.addItem(item8);
					kialist.add(map8);

					kiapoint9 = new GeoPoint(35170064,126890014);
					Drawable drawable9 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item9 = new OverlayItem(kiapoint9, "맛집",
							"만리장성");
					MapOverlay map9 = new MapOverlay(drawable9,KiaLayout.this);
					map9.addItem(item9);
					kialist.add(map9);

					kiapoint10 = new GeoPoint(35170919,126884998);
					Drawable drawable10 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item10 = new OverlayItem(kiapoint10, "맛집",
							"춘천호반닭갈비");
					MapOverlay map10 = new MapOverlay(drawable10,KiaLayout.this);
					map10.addItem(item10);
					kialist.add(map10);

					kiapoint11 = new GeoPoint(35166654,126886712);
					Drawable drawable11 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item11 = new OverlayItem(kiapoint11, "맛집",
							"우림회관");
					MapOverlay map11 = new MapOverlay(drawable11,KiaLayout.this);
					map11.addItem(item11);
					kialist.add(map11);

					kiapoint12 = new GeoPoint(35171006,126886806);
					Drawable drawable12 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item12 = new OverlayItem(kiapoint12, "맛집",
							"대우식육식당");
					MapOverlay map12 = new MapOverlay(drawable12,KiaLayout.this);
					map12.addItem(item12);
					kialist.add(map12);

					kiapoint13 = new GeoPoint(35166393,126886624);
					Drawable drawable13 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item13 = new OverlayItem(kiapoint13, "맛집",
							"창평국밥");
					MapOverlay map13 = new MapOverlay(drawable13,KiaLayout.this);
					map13.addItem(item13);
					kialist.add(map13);

				}

				// startLocationService();

			}

		});
		/** 홈으로 가기 버튼 */
		ImageButton kbutton5 = (ImageButton) findViewById(R.id.kbutton5);
		kbutton5.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent myIntent3 = new Intent(KiaLayout.this,
						MainActivity.class);
				myIntent3.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				myIntent3.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
				startActivity(myIntent3);
				finish();
			}
		});
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

	}

	private LocalActivityManager getLocalActivityManager() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected boolean isRouteDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}

	private void startLocationService() {
		LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		Location location = manager
				.getLastKnownLocation(LocationManager.GPS_PROVIDER);

		GPSListener gpsListener = new GPSListener();
		long minTime = 10000;
		float minDistance = 0;

		manager.requestLocationUpdates(LocationManager.GPS_PROVIDER, minTime,
				minDistance, gpsListener);

	}

	private class GPSListener implements LocationListener {

		public void onLocationChanged(Location location) {
			Double latitude = location.getLatitude();
			Double longitude = location.getLongitude();

			showCurrentLocation(latitude, longitude);
		}

		public void onProviderDisabled(String provider) {
		}

		public void onProviderEnabled(String provider) {
		}

		public void onStatusChanged(String provider, int status, Bundle extras) {
		}
	}

	/**
	 * void showCurrentLocation
	 * 
	 * @param latitude
	 *            : Double type
	 * @param longitude
	 *            : Double type
	 */

	private void showCurrentLocation(Double latitude, Double longitude) {
		double intLatitude = latitude.doubleValue() * 1000000;
		double inLongitude = longitude.doubleValue() * 1000000;

		GeoPoint geoPt = new GeoPoint((int) intLatitude, (int) inLongitude);

		MapController controller = mapView.getController();
		controller.animateTo(geoPt);

		int maxZoomlevel = mapView.getMaxZoomLevel();
		int zoomLevel = (int) ((maxZoomlevel + 1) / 1.15);

		controller.setZoom(zoomLevel);
		controller.setCenter(geoPt);
		mapView.setSatellite(false);
		mapView.setTraffic(false);

	}

	/**
	 * void onActivityResult save result of twitter activity
	 * 
	 * @param requestCode
	 *            : int type
	 * @param resultCode
	 *            : int type
	 * @param data
	 *            : Intent type
	 */
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		Log.e("DAY", "Resultcode : " + resultCode);

		if (resultCode == RESULT_OK) {

			STATICVALUES.token = data.getStringExtra("token");
			STATICVALUES.tokenSecret = data.getStringExtra("tokenSecret");
			STATICVALUES.nick = data.getStringExtra("nick");

			Toast.makeText(this, STATICVALUES.nick + "로그인 되었습니다.!",
					Toast.LENGTH_SHORT).show();

			Log.e("DAY", STATICVALUES.token);
			Log.e("DAY", STATICVALUES.tokenSecret);
			Log.e("DAY", STATICVALUES.nick);

		}

	}

	// 버튼 리스너
	OnClickListener listener = new OnClickListener() {

		@Override
		public void onClick(View arg0) {
			hp.open(389);
		}

	};

	// 업데이트하기
	private void listUpdate() {
		sa.notifyDataSetChanged();
	}

}
