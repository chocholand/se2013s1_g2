package com.example.BaseBallSNS;

import java.util.ArrayList;
import java.util.Date;



import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Tweet;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import android.util.Log;


public class HashTag {
	//hash tag 기본값
	public String ResultTag;
	private QueryResult _query;
	private Query tag;
	private String Tag = "a"; 
	
	/**HashTag Constructor
	 * 객체를 선언하는 즉시 생성자로 해시태그를 완성하고 쿼리에 해시태그를 넣음.
	 * @param CityName : String type
	 */
	public HashTag(String cheer) {
		ResultTag = MakeHash(cheer); //검색할 해시태그 생성
		tag = new Query(new String(ResultTag));
	}
	
	/**
	 * MakeHash
	 * 검색한 도시명을 받아 고유의 HashTag로 만들어주는 메서드
	 * @param CityName : String Type
	 * @return String  
	 */
	public String MakeHash(String cheer)
	{
		ResultTag = cheer+" ";
		Log.d("result Tag",ResultTag);
		return ResultTag;
	}
	
	/**
	 * SearchTweet
	 * 검색한 HashTag 결과를 받아오는 함수.
	 * @param HashTag
	 * @return String[]
	 */
	public void SearchTweet(String cheer, ArrayList<String> data) 
	{
		if(cheer != null) //만약 받아온 CityName이 없다면(검색을 안한 상태) 해시태그 검색하지 않음.
		{
			
			Twitter twitter = new TwitterFactory().getInstance();
			try {
				Log.d(Tag, ResultTag);
				_query = twitter.search(tag);
			} catch (TwitterException e1) {
				e1.printStackTrace();
			} catch (RuntimeException e2){
				e2.printStackTrace();
			}

			}
		}
	}


