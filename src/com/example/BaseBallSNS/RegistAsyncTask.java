
package com.example.BaseBallSNS;


import twitter4j.Twitter;

import twitter4j.TwitterFactory;


import twitter4j.TwitterException;
import twitter4j.http.AccessToken;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

/**
 * class RegistAsyncTask
 * 글쓰기 확인에 대한 클래스.
 * @see : AsynTask<String, Void, AccessToken>, TwitterActivity.java
 * @author : <Open Source> Day young ( dryudryu@gmail.com )
 * @version : 3.0
 * @since : 2011. 3. 4. 오후 5:51:49 update history
 */
public class RegistAsyncTask extends AsyncTask<String, Void, Boolean> {
	
	private static final String TAG = "RegistAsyncTask";	
	
	Twitter twitter;	
	
	Context context;	
	
	ProgressDialog dialog;	
	
	/**RegistAsynTask
	 * 생성자
	 * @param context : Context type
	 */

	public RegistAsyncTask(Context context) {		
		
		Log.e(TAG, "RegistAsyncTask");
		
		this.context = context;				
	}		

	/**void onPreExecute
	 * comment 쓰기에 대한 프로그래스
	 */
	@Override
	protected void onPreExecute() {
		
		super.onPreExecute();	
		
		dialog = new ProgressDialog(context);		
		dialog.setMessage("Writing a comment . . .");		
		dialog.show();	
		
	}	
	
	/**void onCancelled
	 * 글쓰기 취소.
	 */
	@Override
	protected void onCancelled() {		
		super.onCancelled();
		
		dialog.dismiss();
	}
	
	/**Boolean doInBackground
	 * background에서 수행할 작업을 구현해야 함. execute(…) 메소드에 입력된 인자들을 전달 받음.
	 * @param params[] : String type
	 * @return isSuccess
	 */
	@Override
	protected Boolean doInBackground(String... params) {
		
		// TODO Auto-generated method stub
				
		Boolean isSuccess = false;
			
		try {
			
			//Consumer key
			String consumerKey = STATICVALUES.consumerKey;
			//Consumer secret		
			String consumerSecret = STATICVALUES.consumerSecret;	
			
			twitter = new TwitterFactory().getInstance();			
			
			twitter.setOAuthConsumer(consumerKey ,consumerSecret);
						
			AccessToken accessToken = new AccessToken(params[0], params[1]);
			
			twitter.setOAuthAccessToken(accessToken);				
			
			twitter.updateStatus(params[2]);
			
			isSuccess = true;
		} 		
		catch (TwitterException e) {
			
			isSuccess = false;
			
			e.printStackTrace();
			
			
		}		
		
		return isSuccess;
		
	}

	
	/**void onPostExecute
	 * 글쓰기에 대한 메시지 출력 
	 * @param isSuccess : Boolean type
	 */	
	
	@Override
	protected void onPostExecute(Boolean isSuccess) {
		
		super.onPostExecute(isSuccess);
		
		if(isSuccess)
		{		
			Toast.makeText(context, "글쓰기 성공!", Toast.LENGTH_SHORT).show();
			
		}
		else
		{
			Toast.makeText(context, "글쓰기 실패!", Toast.LENGTH_SHORT).show();
			
		}		
		
		dialog.dismiss();
	}
}
