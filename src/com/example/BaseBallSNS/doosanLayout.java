package com.example.BaseBallSNS;

import java.util.*;
import java.util.zip.Inflater;

import com.example.BaseBallSNS.R;
import com.google.android.maps.*;

import android.app.*;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import android.location.*;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.*;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.*;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.graphics.drawable.Drawable;

import com.google.android.maps.*;
import com.google.android.maps.MapView.LayoutParams;

/**
 * @author LeeJungsu
 * @param kiaSelect
 *            : boolean Type.
 */

public class doosanLayout extends MapActivity implements OnClickListener {
	View doosanTimeLine2, doosanmenu2, doosanmenu3;
	ListView doosanlistview;
	MapView doosanmapView; // //맵뷰 객체
	MapController doosanmc;
	GeoPoint doosanpoint, doosanpoint2, doosanpoint3, doosanpoint4, doosanpoint5,
			doosanpoint6, doosanpoint7, doosanpoint8, doosanpoint9, doosanpoint10,
			doosanpoint11, doosanpoint12, doosanpoint13, doosanpoint14,
			doosanpoint15, doosanpoint16, doosanpoint17;
	List<Overlay> doosanlist; // 맵 오버레이 포함하는 리스트
	EditText doosancomment;
	ArrayList<MyItem> doosanarItem;
	private ArrayList<HashMap<String, String>> doosandata;
	private Button doosanbt_open;
	private ListView doosanlistview2;
	private SimpleAdapter doosansa;
	private Net_HTMLParse doosanhp;
	public static HashMap<String, String> doosanmap = new HashMap<String, String>();
	private final Handler doosanhandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			listUpdate();
		}
	};

	public static boolean doosanSelect = true;

	@Override
	/**
	 * onCreate()
	 * 화면 생성 함수, void type
	 * @author Lee Jung Su
	 * @param Bundle savedInstanceState
	 */
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.doosan);

		// top = findViewById(R.id.top);
		// top.setVisibility(View.INVISIBLE);
		doosanmapView = (MapView) findViewById(R.id.doosanmapview);
		doosanmapView.setVisibility(View.INVISIBLE);
		// TimeLine2 = findViewById(R.id.list);
		// TimeLine2.setVisibility(View.INVISIBLE);
		doosanmenu2 = findViewById(R.id.doosanmenu2);
		doosanmenu2.setVisibility(View.INVISIBLE);
		doosanmenu3 = findViewById(R.id.doosanmenu3);
		doosanmenu3.setVisibility(View.INVISIBLE);
		doosanlistview = (ListView) findViewById(R.id.doosanlist);
		doosanlistview.setVisibility(View.INVISIBLE);
		doosanlistview2 = (ListView) findViewById(R.id.doosanlist2);
		doosanlistview2.setVisibility(View.VISIBLE);

		// hp.open();

		doosandata = new ArrayList<HashMap<String, String>>();

		// bt_open = (Button)findViewById(R.id.bt_open);

		doosanhp = new Net_HTMLParse(this, doosanhandler, doosandata);

		doosansa = new SimpleAdapter(this, doosandata, R.layout.homeview, new String[] {
				"date", "team1", "score","team2","time","where" }, new int[] { R.id.date, R.id.team1,
				R.id.score, R.id.team2, R.id.time, R.id.where });

		// listview2.setOnClickListener(listener);
		doosanlistview2.setAdapter(doosansa);
		doosanhp.open(385);
		/* 홈버튼 */
		ImageButton doosanbutton1 = (ImageButton) findViewById(R.id.doosanbutton1);
		doosanbutton1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				doosanlistview.setVisibility(View.INVISIBLE);
				doosanmapView.setVisibility(View.INVISIBLE);
				// top.setVisibility(View.INVISIBLE);
				doosanmenu2.setVisibility(View.INVISIBLE);
				doosanmenu3.setVisibility(View.INVISIBLE);

				doosanhp.open(385);

				doosanlistview2.setVisibility(View.VISIBLE);
				// startActivity(myIntent);
			}
		});
		TeamAudio doosanaudio = new TeamAudio();
		doosanarItem = new ArrayList<MyItem>();
		MyItem mi;
		mi = new MyItem(R.drawable.doosankim, "김진욱", "감독", "1960년 08월05일",
				"182cm / 87kg",doosanmap.get("선동열"),"가사가사가사");
		doosanarItem.add(mi);
		mi = new MyItem(R.drawable.doosanplayer1, "정재훈", "투수", "1980년 01월01일",
				"179cm / 81kg",doosanmap.get("선동열"),"가사가사가사");
		doosanarItem.add(mi);
		mi = new MyItem(R.drawable.doosanplayer2, "김선우", "투수", "1977년 09월04일",
				"184cm / 87kg",doosanmap.get("선동열"),"가사가사가사");
		doosanarItem.add(mi);
		mi = new MyItem(R.drawable.doosanplayer3, "김상현", "투수", "1980년04월07일",
				"181cm / 82kg",doosanmap.get("선동열"),"가사가사가사");
		doosanarItem.add(mi);
		mi = new MyItem(R.drawable.doosanplayer4, "김강률", "투수", "1988년 08월28일",
				"187cm / 95kg",doosanmap.get("선동열"),"가사가사가사");
		doosanarItem.add(mi);
		mi = new MyItem(R.drawable.doosanplayer5, "올슨", "투수", "1983년 10월18일",
				"185cm / 93kg",doosanmap.get("선동열"),"가사가사가사");
		doosanarItem.add(mi);
		mi = new MyItem(R.drawable.doosanplayer6, "임태훈", "투수", " 1988년 09월28일",
				"182cm / 83kg",doosanmap.get("선동열"),"가사가사가사");
		doosanarItem.add(mi);
		mi = new MyItem(R.drawable.doosanplayer7, "이재우", "투수", " 1980년 02월09일",
				"183cm / 82kg",doosanmap.get("선동열"),"가사가사가사");
		doosanarItem.add(mi);
		mi = new MyItem(R.drawable.doosanplayer8, "니퍼드", "투수", "1981년 05월06일",
				"203cm / 103kg",doosanmap.get("선동열"),"가사가사가사");
		doosanarItem.add(mi);
		mi = new MyItem(R.drawable.doosanplayer9, "오현택", "투수", "1985년 07월17일",
				"180cm / 73kg",doosanmap.get("선동열"),"가사가사가사");
		doosanarItem.add(mi);
		mi = new MyItem(R.drawable.doosanplayer10, "양의지	", "포수", "1987년 06월05일",
				"179cm / 85kg",doosanmap.get("선동열"),"가사가사가사");
		doosanarItem.add(mi);
		mi = new MyItem(R.drawable.doosanplayer11, "최재훈", "포수",
				"1989년 08월 27일", "178cm / 76kg",doosanmap.get("선동열"),"가사가사가사");
		doosanarItem.add(mi);
		mi = new MyItem(R.drawable.doosanplayer12, "최준석", "내야수",
				"1983년 02월15일", "185cm / 115kg",doosanmap.get("선동열"),"가사가사가사");
		doosanarItem.add(mi);
		mi = new MyItem(R.drawable.doosanplayer13, "김재호", "내야수",
				"1985년 03월21일", "181cm / 75kg",doosanmap.get("선동열"),"가사가사가사");
		doosanarItem.add(mi);
		mi = new MyItem(R.drawable.doosanplayer14, "홍성흔", "내야수",
				"1977년 02월28일", "180cm / 96kg",doosanmap.get("선동열"),"가사가사가사");
		doosanarItem.add(mi);
		mi = new MyItem(R.drawable.doosanplayer15, "최주환", "내야수",
				"1988년 02월28일", "178cm / 73kg",doosanmap.get("선동열"),"가사가사가사");
		doosanarItem.add(mi);
		mi = new MyItem(R.drawable.doosanplayer16, "허경민", "내야수",
				"1990년 08월26일", "176cm / 69kg",doosanmap.get("선동열"),"가사가사가사");
		doosanarItem.add(mi);
		mi = new MyItem(R.drawable.doosanplayer17, "윤석민", "내야수",
				"1985년 09월04일", "180cm / 86kg",doosanmap.get("선동열"),"가사가사가사");
		doosanarItem.add(mi);
		mi = new MyItem(R.drawable.doosanplayer18, "오재원", "내야수",
				"1985년 02월09일", "185cm / 75kg",doosanmap.get("선동열"),"가사가사가사");
		doosanarItem.add(mi);
		mi = new MyItem(R.drawable.doosanplayer19, "박건우", "외야수",
				"1990년 09월08일", "184cm / 80kg",doosanmap.get("선동열"),"가사가사가사");
		doosanarItem.add(mi);
		mi = new MyItem(R.drawable.doosanplayer20, "민병헌", "외야수",
				"1987년 03월10일", "178cm / 78kg",doosanmap.get("선동열"),"가사가사가사");
		doosanarItem.add(mi);
		mi = new MyItem(R.drawable.doosanplayer21, "김현수", "외야수",
				"1988년 01월12일", "188cm / 100kg",doosanmap.get("선동열"),"가사가사가사");
		doosanarItem.add(mi);
		mi = new MyItem(R.drawable.doosanplayer22, "이종욱", "외야수",
				"1980년 06월18일", "176cm / 77kg",doosanmap.get("선동열"),"가사가사가사");
		doosanarItem.add(mi);
		mi = new MyItem(R.drawable.doosanplayer23, "정수빈", "외야수",
				"1990년 10월07일", "175cm / 70kg",doosanmap.get("선동열"),"가사가사가사");
		doosanarItem.add(mi);
		MyListAdapter MyAdapter = new MyListAdapter(this, R.layout.kiaplayer,
				doosanarItem);

		ListView doosanMyList;
		doosanMyList = (ListView) findViewById(R.id.doosanlist);
		doosanMyList.setAdapter(MyAdapter);

		/** 선수정보 버튼 */
		ImageButton doosanbutton2 = (ImageButton) findViewById(R.id.doosanbutton2);
		doosanbutton2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				doosanlistview.setVisibility(View.VISIBLE);
				doosanmapView.setVisibility(View.INVISIBLE);
				// top.setVisibility(View.INVISIBLE);
				doosanmenu2.setVisibility(View.INVISIBLE);
				doosanmenu3.setVisibility(View.INVISIBLE);

				// startActivity(myIntent);
			}
		});

		/** 트위터 버튼 */
		ImageButton doosanbutton3 = (ImageButton) findViewById(R.id.doosanbutton3);
		doosanbutton3.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				doosanmenu2.setVisibility(View.VISIBLE);
				doosanmenu3.setVisibility(View.VISIBLE);
				doosanmapView.setVisibility(View.INVISIBLE);
				doosanlistview.setVisibility(View.INVISIBLE);
				doosanlistview2.setVisibility(View.INVISIBLE);
				// TimeLine2.setVisibility(View.INVISIBLE);

			}
		});
		/** 트위터 로그인버튼 */
		ImageButton button = (ImageButton) findViewById(R.id.doosanlogin);
		button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent myIntent = new Intent(doosanLayout.this,
						TwitterActivity.class);
				startActivityForResult(myIntent, 1004);
				// startActivity(myIntent);
			}
		});
		/** 트위터 글쓰기버튼 */
		final HashTag hash = new HashTag("");
		ImageButton doosanButton1 = (ImageButton) findViewById(R.id.doosantweet);
		doosancomment = (EditText) findViewById(R.id.doosancomment);
		doosanButton1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				String commentString = doosancomment.getText().toString();
				if (STATICVALUES.token != null
						&& STATICVALUES.tokenSecret != null) {
					RegistAsyncTask registAsyncTask = new RegistAsyncTask(
							doosanLayout.this);
					registAsyncTask.execute(STATICVALUES.token,
							STATICVALUES.tokenSecret, commentString);
				} else {
					Toast.makeText(doosanLayout.this, "로그인해주세요!",
							Toast.LENGTH_SHORT).show();
				}
				doosancomment.setText("");

			}
		});
		CheckBox doosanRbutton1 = (CheckBox) findViewById(R.id.doosancheckBox1);
		doosanRbutton1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				ImageButton doosanButton1 = (ImageButton) findViewById(R.id.doosantweet);
				doosanButton1.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						hash.ResultTag = "#기아타이거즈";
						String commentString = hash.MakeHash("#기아타이거즈") + " "
								+ doosancomment.getText().toString();
						if (STATICVALUES.token != null
								&& STATICVALUES.tokenSecret != null) {
							RegistAsyncTask registAsyncTask = new RegistAsyncTask(
									doosanLayout.this);
							registAsyncTask.execute(STATICVALUES.token,
									STATICVALUES.tokenSecret, commentString);
						} else {
							Toast.makeText(doosanLayout.this, "로그인해주세요!",
									Toast.LENGTH_SHORT).show();
						}
						doosancomment.setText("");
					}
				});
			}
		});
		CheckBox doosanRbutton2 = (CheckBox) findViewById(R.id.doosancheckBox2);
		doosanRbutton2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				ImageButton doosanButton1 = (ImageButton) findViewById(R.id.doosantweet);
				doosanButton1.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						hash.ResultTag = "#기아타이거즈2";
						String commentString = hash.ResultTag + " "
								+ doosancomment.getText().toString();
						if (STATICVALUES.token != null
								&& STATICVALUES.tokenSecret != null) {
							RegistAsyncTask registAsyncTask = new RegistAsyncTask(
									doosanLayout.this);
							registAsyncTask.execute(STATICVALUES.token,
									STATICVALUES.tokenSecret, commentString);
						} else {
							Toast.makeText(doosanLayout.this, "로그인해주세요",
									Toast.LENGTH_SHORT).show();
						}
						doosancomment.setText("");
					}
				});
			}
		});
		ImageButton doosanTbutton = (ImageButton) findViewById(R.id.doosantweet);
		doosanTbutton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				doosanmenu2.setVisibility(View.VISIBLE);
				doosanmenu3.setVisibility(View.VISIBLE);

			}
		});

		ImageButton doosanTimebutton = (ImageButton) findViewById(R.id.doosantimeline);
		doosanTimebutton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				//doosanmenu2.setVisibility(View.VISIBLE);
				// menu3.setVisibility(View.VISIBLE);
				timeline.searchTerm="#두산베어스";
				Intent intent = new Intent(doosanLayout.this, timeline.class);
				startActivity(intent);
				// top.setVisibility(View.INVISIBLE);
				// menu2.setVisibility(View.VISIBLE);
				//doosanmapView.setVisibility(View.INVISIBLE);
				// TimeLine2.setVisibility(View.VISIBLE);
				//doosanlistview.setVisibility(View.INVISIBLE);

			}
		});

		/** 구글 맵 버튼 */
		ImageButton doosanMbutton4 = (ImageButton) findViewById(R.id.doosanbutton4);
		doosanMbutton4.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// top.setVisibility(View.INVISIBLE);
				doosanmenu2.setVisibility(View.INVISIBLE);
				doosanmenu3.setVisibility(View.INVISIBLE);
				doosanmapView.setVisibility(View.VISIBLE);
				doosanmapView.setBuiltInZoomControls(true);// zoom controller
				doosanmc = doosanmapView.getController();

				if (doosanLayout.doosanSelect == true) {
					doosanpoint = new GeoPoint(35193819, 129061471);// 기아 경기장 좌표
																	// 설정
					doosanmc.animateTo(doosanpoint);// 좌표점 이동
					doosanmc.setZoom(17);// 확대 1~21

					doosanlist = doosanmapView.getOverlays();
					// 위치에 이미지 설정한다.
					Drawable drawable = getResources().getDrawable(
							R.drawable.mapdoosan);
					OverlayItem item = new OverlayItem(doosanpoint, "부산사직야구장",
							"롯데자이언츠");
					MapOverlay map = new MapOverlay(drawable,doosanLayout.this);
					map.addItem(item);
					doosanlist.add(map);

					doosanpoint2 = new GeoPoint(35196074, 129059442);// 기아 경기장 좌표
																	// 설정
					Drawable drawable2 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item2 = new OverlayItem(doosanpoint2, "맛집",
							"주문진막국수");
					MapOverlay map2 = new MapOverlay(drawable2,doosanLayout.this);
					map2.addItem(item2);
					doosanlist.add(map2);

					doosanpoint3 = new GeoPoint(35195541, 129058750);// 기아 경기장 좌표
																	// 설정
					Drawable drawable3 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item3 = new OverlayItem(doosanpoint3, "맛집",
							"안양해물탕");
					MapOverlay map3 = new MapOverlay(drawable3,doosanLayout.this);
					map3.addItem(item3);
					doosanlist.add(map3);

					doosanpoint4 = new GeoPoint(35198551, 129064490);// 기아 경기장 좌표
																	// 설정
					Drawable drawable4 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item4 = new OverlayItem(doosanpoint4, "맛집",
							"금강만두");
					MapOverlay map4 = new MapOverlay(drawable4,doosanLayout.this);
					map4.addItem(item4);
					doosanlist.add(map4);

					doosanpoint5 = new GeoPoint(35193521,129062002);
					Drawable drawable5 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item5 = new OverlayItem(doosanpoint5, "맛집",
							"롯데리아");
					MapOverlay map5 = new MapOverlay(drawable5,doosanLayout.this);
					map5.addItem(item5);
					doosanlist.add(map5);

					doosanpoint6 = new GeoPoint(35193706,129064776);
					Drawable drawable6 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item6 = new OverlayItem(doosanpoint6, "맛집",
							"서영식당");
					MapOverlay map6 = new MapOverlay(drawable6,doosanLayout.this);
					map6.addItem(item6);
					doosanlist.add(map6);

					doosanpoint7 = new GeoPoint(35195506,129064009);
					Drawable drawable7 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item7 = new OverlayItem(doosanpoint7, "맛집",
							"마포진연탄구이");
					MapOverlay map7 = new MapOverlay(drawable7,doosanLayout.this);
					map7.addItem(item7);
					doosanlist.add(map7);

					doosanpoint8 = new GeoPoint(35195726,129063939);
					Drawable drawable8 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item8 = new OverlayItem(doosanpoint8, "맛집",
							"비비큐치킨");
					MapOverlay map8 = new MapOverlay(drawable8,doosanLayout.this);
					map8.addItem(item8);
					doosanlist.add(map8);

					doosanpoint9 = new GeoPoint(35195423,129065452);
					Drawable drawable9 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item9 = new OverlayItem(doosanpoint9, "맛집",
							"류가네더덕삼겹살");
					MapOverlay map9 = new MapOverlay(drawable9,doosanLayout.this);
					map9.addItem(item9);
					doosanlist.add(map9);

					doosanpoint10 = new GeoPoint(35195528,129064164);
					Drawable drawable10 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item10 = new OverlayItem(doosanpoint10, "맛집",
							"조방낙지");
					MapOverlay map10 = new MapOverlay(drawable10,doosanLayout.this);
					map10.addItem(item10);
					doosanlist.add(map10);

					doosanpoint11 = new GeoPoint(35195436,129064577);
					Drawable drawable11 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item11 = new OverlayItem(doosanpoint11, "맛집",
							"서원숯불바베큐");
					MapOverlay map11 = new MapOverlay(drawable11,doosanLayout.this);
					map11.addItem(item11);
					doosanlist.add(map11);

					doosanpoint12 = new GeoPoint(35193871,129064577);
					Drawable drawable12 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item12 = new OverlayItem(doosanpoint12, "맛집",
							"대구막창왕갈비");
					MapOverlay map12 = new MapOverlay(drawable12,doosanLayout.this);
					map12.addItem(item12);
					doosanlist.add(map12);

					doosanpoint13 = new GeoPoint(35193538,129064583);
					Drawable drawable13 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item13 = new OverlayItem(doosanpoint13, "맛집",
							"진우식당");
					MapOverlay map13 = new MapOverlay(drawable13,doosanLayout.this);
					map13.addItem(item13);
					doosanlist.add(map13);

					doosanpoint14 = new GeoPoint(35195940,129060618);
					Drawable drawable14 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item14 = new OverlayItem(doosanpoint14, "맛집",
							"맥도날드");
					MapOverlay map14 = new MapOverlay(drawable14,doosanLayout.this);
					map14.addItem(item14);
					doosanlist.add(map14);

					doosanpoint15 = new GeoPoint(35196300,129060640);
					Drawable drawable15 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item15 = new OverlayItem(doosanpoint15, "맛집",
							"미스터피자");
					MapOverlay map15 = new MapOverlay(drawable15,doosanLayout.this);
					map15.addItem(item15);
					doosanlist.add(map15);

					doosanpoint16 = new GeoPoint(35195971,129059567);
					Drawable drawable16 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item16 = new OverlayItem(doosanpoint16, "맛집",
							"주문진식당");
					MapOverlay map16 = new MapOverlay(drawable16,doosanLayout.this);
					map16.addItem(item16);
					doosanlist.add(map16);



				}

				// startLocationService();

			}

		});
		/** 홈으로 가기 버튼 */
		ImageButton doosanHbutton5 = (ImageButton) findViewById(R.id.doosanbutton5);
		doosanHbutton5.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent myIntent3 = new Intent(doosanLayout.this,
						MainActivity.class);
				myIntent3.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				myIntent3.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
				startActivity(myIntent3);
				finish();
			}
		});
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

	}

	private LocalActivityManager getLocalActivityManager() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected boolean isRouteDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}

	private void startLocationService() {
		LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		Location location = manager
				.getLastKnownLocation(LocationManager.GPS_PROVIDER);

		GPSListener gpsListener = new GPSListener();
		long minTime = 10000;
		float minDistance = 0;

		manager.requestLocationUpdates(LocationManager.GPS_PROVIDER, minTime,
				minDistance, gpsListener);

	}

	private class GPSListener implements LocationListener {

		public void onLocationChanged(Location location) {
			Double latitude = location.getLatitude();
			Double longitude = location.getLongitude();

			showCurrentLocation(latitude, longitude);
		}

		public void onProviderDisabled(String provider) {
		}

		public void onProviderEnabled(String provider) {
		}

		public void onStatusChanged(String provider, int status, Bundle extras) {
		}
	}

	/**
	 * void showCurrentLocation
	 * 
	 * @param latitude
	 *            : Double type
	 * @param longitude
	 *            : Double type
	 */

	private void showCurrentLocation(Double latitude, Double longitude) {
		double intLatitude = latitude.doubleValue() * 1000000;
		double inLongitude = longitude.doubleValue() * 1000000;

		GeoPoint geoPt = new GeoPoint((int) intLatitude, (int) inLongitude);

		MapController controller = doosanmapView.getController();
		controller.animateTo(geoPt);

		int maxZoomlevel = doosanmapView.getMaxZoomLevel();
		int zoomLevel = (int) ((maxZoomlevel + 1) / 1.15);

		controller.setZoom(zoomLevel);
		controller.setCenter(geoPt);
		doosanmapView.setSatellite(false);
		doosanmapView.setTraffic(false);

	}

	/**
	 * void onActivityResult save result of twitter activity
	 * 
	 * @param requestCode
	 *            : int type
	 * @param resultCode
	 *            : int type
	 * @param data
	 *            : Intent type
	 */
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		Log.e("DAY", "Resultcode : " + resultCode);

		if (resultCode == RESULT_OK) {

			STATICVALUES.token = data.getStringExtra("token");
			STATICVALUES.tokenSecret = data.getStringExtra("tokenSecret");
			STATICVALUES.nick = data.getStringExtra("nick");

			Toast.makeText(this, STATICVALUES.nick + "로그인 되었습니다.!",
					Toast.LENGTH_SHORT).show();

			Log.e("DAY", STATICVALUES.token);
			Log.e("DAY", STATICVALUES.tokenSecret);
			Log.e("DAY", STATICVALUES.nick);

		}

	}

	// 버튼 리스너
	OnClickListener listener = new OnClickListener() {

		@Override
		public void onClick(View arg0) {
			doosanhp.open(385);
		}

	};

	// 업데이트하기
	private void listUpdate() {
		doosansa.notifyDataSetChanged();
	}

}
