package com.example.BaseBallSNS;

import java.util.*;
import java.util.zip.Inflater;

import com.example.BaseBallSNS.R;
import com.google.android.maps.*;

import android.app.*;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import android.location.*;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.*;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.*;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.graphics.drawable.Drawable;

import com.google.android.maps.*;
import com.google.android.maps.MapView.LayoutParams;

/**
 * @author LeeJungsu
 * @param kiaSelect
 *            : boolean Type.
 */

public class LotteLayout extends MapActivity implements OnClickListener {
	View lotteTimeLine2, lottemenu2, lottemenu3;
	ListView lottelistview;
	MapView lottemapView; // //맵뷰 객체
	MapController lottemc;
	GeoPoint lottepoint, lottepoint2, lottepoint3, lottepoint4, lottepoint5,
			lottepoint6, lottepoint7, lottepoint8, lottepoint9, lottepoint10,
			lottepoint11, lottepoint12, lottepoint13, lottepoint14,
			lottepoint15, lottepoint16, lottepoint17;
	List<Overlay> lottelist; // 맵 오버레이 포함하는 리스트
	EditText lottecomment;
	ArrayList<MyItem> lottearItem;
	private ArrayList<HashMap<String, String>> lottedata;
	private Button lottebt_open;
	private ListView lottelistview2;
	private SimpleAdapter lottesa;
	private Net_HTMLParse lottehp;
	public static HashMap<String, String> lottemap = new HashMap<String, String>();
	private final Handler lottehandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			listUpdate();
		}
	};

	public static boolean lotteSelect = true;

	@Override
	/**
	 * onCreate()
	 * 화면 생성 함수, void type
	 * @author Lee Jung Su
	 * @param Bundle savedInstanceState
	 */
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lotte);

		// top = findViewById(R.id.top);
		// top.setVisibility(View.INVISIBLE);
		lottemapView = (MapView) findViewById(R.id.lottemapview);
		lottemapView.setVisibility(View.INVISIBLE);
		// TimeLine2 = findViewById(R.id.list);
		// TimeLine2.setVisibility(View.INVISIBLE);
		lottemenu2 = findViewById(R.id.lottemenu2);
		lottemenu2.setVisibility(View.INVISIBLE);
		lottemenu3 = findViewById(R.id.lottemenu3);
		lottemenu3.setVisibility(View.INVISIBLE);
		lottelistview = (ListView) findViewById(R.id.lottelist);
		lottelistview.setVisibility(View.INVISIBLE);
		lottelistview2 = (ListView) findViewById(R.id.lottelist2);
		lottelistview2.setVisibility(View.VISIBLE);

		// hp.open();

		lottedata = new ArrayList<HashMap<String, String>>();

		// bt_open = (Button)findViewById(R.id.bt_open);

		lottehp = new Net_HTMLParse(this, lottehandler, lottedata);

		lottesa = new SimpleAdapter(this, lottedata, R.layout.homeview, new String[] {
				"date", "team1", "score","team2","time","where" }, new int[] { R.id.date, R.id.team1,
				R.id.score, R.id.team2, R.id.time, R.id.where });

		// listview2.setOnClickListener(listener);
		lottelistview2.setAdapter(lottesa);
		lottehp.open(386);
		/* 홈버튼 */
		ImageButton lottebutton1 = (ImageButton) findViewById(R.id.lottebutton1);
		lottebutton1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				lottelistview.setVisibility(View.INVISIBLE);
				lottemapView.setVisibility(View.INVISIBLE);
				// top.setVisibility(View.INVISIBLE);
				lottemenu2.setVisibility(View.INVISIBLE);
				lottemenu3.setVisibility(View.INVISIBLE);

				lottehp.open(386);

				lottelistview2.setVisibility(View.VISIBLE);
				// startActivity(myIntent);
			}
		});
		TeamAudio Kiaaudio = new TeamAudio();
		lottearItem = new ArrayList<MyItem>();
		MyItem mi;
		mi = new MyItem(R.drawable.lottekim, "김시진", "감독", "1958년 03월20일",
				"183cm / 87kg",lottemap.get("김시진"),"가사가사가사");
		lottearItem.add(mi);
		mi = new MyItem(R.drawable.lotteplayer1, "강영식", "투수", "1981년 06월17일",
				"189cm / 98kg",lottemap.get("김시진"),"가사가사가사");
		lottearItem.add(mi);
		mi = new MyItem(R.drawable.lotteplayer2, "김승희", "투수", "1981년 02월11일",
				"177cm / 78kg",lottemap.get("김시진"),"가사가사가사");
		lottearItem.add(mi);
		mi = new MyItem(R.drawable.lotteplayer3, "송승준", "투수", "1980년 06월29일",
				"184cm / 105kg",lottemap.get("김시진"),"가사가사가사");
		lottearItem.add(mi);
		mi = new MyItem(R.drawable.lotteplayer4, "정대현", "투수", "1978년 11월10일",
				"185cm / 101kg",lottemap.get("김시진"),"가사가사가사");
		lottearItem.add(mi);
		mi = new MyItem(R.drawable.lotteplayer5, "쉐인유먼", "투수", "1979년 10월 11일",
				"195cm / 100kg",lottemap.get("김시진"),"가사가사가사");
		lottearItem.add(mi);
		mi = new MyItem(R.drawable.lotteplayer6, "강민호", "포수", "1985년 08월 18일",
				"186cm / 100kg",lottemap.get("김시진"),"가사가사가사");
		lottearItem.add(mi);
		mi = new MyItem(R.drawable.lotteplayer7, "문규현", "내야수", "1983년 07월 05일",
				"183cm / 82kg",lottemap.get("김시진"),"가사가사가사");
		lottearItem.add(mi);
		mi = new MyItem(R.drawable.lotteplayer8, "박준서", "내야수", "1981년 12월 11일",
				"181cm / 88kg",lottemap.get("김시진"),"가사가사가사");
		lottearItem.add(mi);
		mi = new MyItem(R.drawable.lotteplayer9, "황재균", "내야수", "1987년 07월 28일",
				"183cm / 90kg",lottemap.get("김시진"),"가사가사가사");
		lottearItem.add(mi);
		mi = new MyItem(R.drawable.lotteplayer10, "정훈", "내야수", "1987년 07월 18일",
				"183cm / 80kg",lottemap.get("김시진"),"가사가사가사");
		lottearItem.add(mi);
		mi = new MyItem(R.drawable.lotteplayer11, "박종윤", "내야수",
				"1982년 04월 11일", "188cm / 90kg",lottemap.get("김시진"),"가사가사가사");
		lottearItem.add(mi);
		mi = new MyItem(R.drawable.lotteplayer12, "신본기", "내야수",
				"1989년 03월 21일", "178cm / 81kg",lottemap.get("김시진"),"가사가사가사");
		lottearItem.add(mi);
		mi = new MyItem(R.drawable.lotteplayer13, "정보명", "외야수",
				"1980년 08월 08일", "177cm / 86kg",lottemap.get("김시진"),"가사가사가사");
		lottearItem.add(mi);
		mi = new MyItem(R.drawable.lotteplayer14, "전준우", "외야수",
				"1986년 02월 25일", "184cm / 93kg",lottemap.get("김시진"),"가사가사가사");
		lottearItem.add(mi);
		mi = new MyItem(R.drawable.lotteplayer15, "김대우", "외야수",
				"1984년 07월 26일", "189cm / 94kg",lottemap.get("김시진"),"가사가사가사");
		lottearItem.add(mi);
		mi = new MyItem(R.drawable.lotteplayer16, "손아섭", "외야수",
				"1988년 03월 28일", "175cm / 90kg",lottemap.get("김시진"),"가사가사가사");
		lottearItem.add(mi);
		mi = new MyItem(R.drawable.lotteplayer17, "황성용", "외야수",
				"1983년 07월 04일", "180cm / 75kg",lottemap.get("김시진"),"가사가사가사");
		lottearItem.add(mi);
		MyListAdapter MyAdapter = new MyListAdapter(this, R.layout.kiaplayer,
				lottearItem);

		ListView lotteMyList;
		lotteMyList = (ListView) findViewById(R.id.lottelist);
		lotteMyList.setAdapter(MyAdapter);

		/** 선수정보 버튼 */
		ImageButton lottebutton2 = (ImageButton) findViewById(R.id.lottebutton2);
		lottebutton2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				lottelistview.setVisibility(View.VISIBLE);
				lottemapView.setVisibility(View.INVISIBLE);
				// top.setVisibility(View.INVISIBLE);
				lottemenu2.setVisibility(View.INVISIBLE);
				lottemenu3.setVisibility(View.INVISIBLE);

				// startActivity(myIntent);
			}
		});

		/** 트위터 버튼 */
		ImageButton lottebutton3 = (ImageButton) findViewById(R.id.lottebutton3);
		lottebutton3.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				lottemenu2.setVisibility(View.VISIBLE);
				lottemenu3.setVisibility(View.VISIBLE);
				lottemapView.setVisibility(View.INVISIBLE);
				lottelistview.setVisibility(View.INVISIBLE);
				lottelistview2.setVisibility(View.INVISIBLE);
				// TimeLine2.setVisibility(View.INVISIBLE);

			}
		});
		/** 트위터 로그인버튼 */
		ImageButton button = (ImageButton) findViewById(R.id.lottelogin);
		button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent myIntent = new Intent(LotteLayout.this,
						TwitterActivity.class);
				startActivityForResult(myIntent, 1004);
				// startActivity(myIntent);
			}
		});
		/** 트위터 글쓰기버튼 */
		final HashTag hash = new HashTag("");
		ImageButton lotteButton1 = (ImageButton) findViewById(R.id.lottetweet);
		lottecomment = (EditText) findViewById(R.id.lottecomment);
		lotteButton1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				String commentString = lottecomment.getText().toString();
				if (STATICVALUES.token != null
						&& STATICVALUES.tokenSecret != null) {
					RegistAsyncTask registAsyncTask = new RegistAsyncTask(
							LotteLayout.this);
					registAsyncTask.execute(STATICVALUES.token,
							STATICVALUES.tokenSecret, commentString);
				} else {
					Toast.makeText(LotteLayout.this, "로그인해주세요!",
							Toast.LENGTH_SHORT).show();
				}
				lottecomment.setText("");

			}
		});
		CheckBox lotteRbutton1 = (CheckBox) findViewById(R.id.lottecheckBox1);
		lotteRbutton1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				ImageButton lotteButton1 = (ImageButton) findViewById(R.id.lottetweet);
				lotteButton1.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						hash.ResultTag = "#기아타이거즈";
						String commentString = hash.MakeHash("#기아타이거즈") + " "
								+ lottecomment.getText().toString();
						if (STATICVALUES.token != null
								&& STATICVALUES.tokenSecret != null) {
							RegistAsyncTask registAsyncTask = new RegistAsyncTask(
									LotteLayout.this);
							registAsyncTask.execute(STATICVALUES.token,
									STATICVALUES.tokenSecret, commentString);
						} else {
							Toast.makeText(LotteLayout.this, "로그인해주세요!",
									Toast.LENGTH_SHORT).show();
						}
						lottecomment.setText("");
					}
				});
			}
		});
		CheckBox lotteRbutton2 = (CheckBox) findViewById(R.id.lottecheckBox2);
		lotteRbutton2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				ImageButton lotteButton1 = (ImageButton) findViewById(R.id.lottetweet);
				lotteButton1.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						hash.ResultTag = "#기아타이거즈2";
						String commentString = hash.ResultTag + " "
								+ lottecomment.getText().toString();
						if (STATICVALUES.token != null
								&& STATICVALUES.tokenSecret != null) {
							RegistAsyncTask registAsyncTask = new RegistAsyncTask(
									LotteLayout.this);
							registAsyncTask.execute(STATICVALUES.token,
									STATICVALUES.tokenSecret, commentString);
						} else {
							Toast.makeText(LotteLayout.this, "로그인해주세요",
									Toast.LENGTH_SHORT).show();
						}
						lottecomment.setText("");
					}
				});
			}
		});
		ImageButton lotteTbutton = (ImageButton) findViewById(R.id.lottetweet);
		lotteTbutton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				lottemenu2.setVisibility(View.VISIBLE);
				lottemenu3.setVisibility(View.VISIBLE);

			}
		});

		ImageButton lotteTimebutton = (ImageButton) findViewById(R.id.lottetimeline);
		lotteTimebutton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				lottemenu2.setVisibility(View.VISIBLE);
				// menu3.setVisibility(View.VISIBLE);
				Intent intent = new Intent(LotteLayout.this, timeline.class);
				startActivityForResult(intent, 1004);
				// top.setVisibility(View.INVISIBLE);
				// menu2.setVisibility(View.VISIBLE);
				lottemapView.setVisibility(View.INVISIBLE);
				// TimeLine2.setVisibility(View.VISIBLE);
				lottelistview.setVisibility(View.INVISIBLE);

			}
		});

		/** 구글 맵 버튼 */
		ImageButton lotteMbutton4 = (ImageButton) findViewById(R.id.lottebutton4);
		lotteMbutton4.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// top.setVisibility(View.INVISIBLE);
				lottemenu2.setVisibility(View.INVISIBLE);
				lottemenu3.setVisibility(View.INVISIBLE);
				lottemapView.setVisibility(View.VISIBLE);
				lottemapView.setBuiltInZoomControls(true);// zoom controller
				lottemc = lottemapView.getController();

				if (LotteLayout.lotteSelect == true) {
					lottepoint = new GeoPoint(35193819, 129061471);// 기아 경기장 좌표
																	// 설정
					lottemc.animateTo(lottepoint);// 좌표점 이동
					lottemc.setZoom(17);// 확대 1~21

					lottelist = lottemapView.getOverlays();
					// 위치에 이미지 설정한다.
					Drawable drawable = getResources().getDrawable(
							R.drawable.maplotte);
					OverlayItem item = new OverlayItem(lottepoint, "부산사직야구장",
							"롯데자이언츠");
					MapOverlay map = new MapOverlay(drawable,LotteLayout.this);
					map.addItem(item);
					lottelist.add(map);

					lottepoint2 = new GeoPoint(35196074, 129059442);// 기아 경기장 좌표
																	// 설정
					Drawable drawable2 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item2 = new OverlayItem(lottepoint2, "맛집",
							"주문진막국수");
					MapOverlay map2 = new MapOverlay(drawable2,LotteLayout.this);
					map2.addItem(item2);
					lottelist.add(map2);

					lottepoint3 = new GeoPoint(35195541, 129058750);// 기아 경기장 좌표
																	// 설정
					Drawable drawable3 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item3 = new OverlayItem(lottepoint3, "맛집",
							"안양해물탕");
					MapOverlay map3 = new MapOverlay(drawable3,LotteLayout.this);
					map3.addItem(item3);
					lottelist.add(map3);

					lottepoint4 = new GeoPoint(35198551, 129064490);// 기아 경기장 좌표
																	// 설정
					Drawable drawable4 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item4 = new OverlayItem(lottepoint4, "맛집",
							"금강만두");
					MapOverlay map4 = new MapOverlay(drawable4,LotteLayout.this);
					map4.addItem(item4);
					lottelist.add(map4);

					lottepoint5 = new GeoPoint(35193521,129062002);
					Drawable drawable5 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item5 = new OverlayItem(lottepoint5, "맛집",
							"롯데리아");
					MapOverlay map5 = new MapOverlay(drawable5,LotteLayout.this);
					map5.addItem(item5);
					lottelist.add(map5);

					lottepoint6 = new GeoPoint(35193706,129064776);
					Drawable drawable6 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item6 = new OverlayItem(lottepoint6, "맛집",
							"서영식당");
					MapOverlay map6 = new MapOverlay(drawable6,LotteLayout.this);
					map6.addItem(item6);
					lottelist.add(map6);

					lottepoint7 = new GeoPoint(35195506,129064009);
					Drawable drawable7 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item7 = new OverlayItem(lottepoint7, "맛집",
							"마포진연탄구이");
					MapOverlay map7 = new MapOverlay(drawable7,LotteLayout.this);
					map7.addItem(item7);
					lottelist.add(map7);

					lottepoint8 = new GeoPoint(35195726,129063939);
					Drawable drawable8 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item8 = new OverlayItem(lottepoint8, "맛집",
							"비비큐치킨");
					MapOverlay map8 = new MapOverlay(drawable8,LotteLayout.this);
					map8.addItem(item8);
					lottelist.add(map8);

					lottepoint9 = new GeoPoint(35195423,129065452);
					Drawable drawable9 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item9 = new OverlayItem(lottepoint9, "맛집",
							"류가네더덕삼겹살");
					MapOverlay map9 = new MapOverlay(drawable9,LotteLayout.this);
					map9.addItem(item9);
					lottelist.add(map9);

					lottepoint10 = new GeoPoint(35195528,129064164);
					Drawable drawable10 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item10 = new OverlayItem(lottepoint10, "맛집",
							"조방낙지");
					MapOverlay map10 = new MapOverlay(drawable10,LotteLayout.this);
					map10.addItem(item10);
					lottelist.add(map10);

					lottepoint11 = new GeoPoint(35195436,129064577);
					Drawable drawable11 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item11 = new OverlayItem(lottepoint11, "맛집",
							"서원숯불바베큐");
					MapOverlay map11 = new MapOverlay(drawable11,LotteLayout.this);
					map11.addItem(item11);
					lottelist.add(map11);

					lottepoint12 = new GeoPoint(35193871,129064577);
					Drawable drawable12 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item12 = new OverlayItem(lottepoint12, "맛집",
							"대구막창왕갈비");
					MapOverlay map12 = new MapOverlay(drawable12,LotteLayout.this);
					map12.addItem(item12);
					lottelist.add(map12);

					lottepoint13 = new GeoPoint(35193538,129064583);
					Drawable drawable13 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item13 = new OverlayItem(lottepoint13, "맛집",
							"진우식당");
					MapOverlay map13 = new MapOverlay(drawable13,LotteLayout.this);
					map13.addItem(item13);
					lottelist.add(map13);

					lottepoint14 = new GeoPoint(35195940,129060618);
					Drawable drawable14 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item14 = new OverlayItem(lottepoint14, "맛집",
							"맥도날드");
					MapOverlay map14 = new MapOverlay(drawable14,LotteLayout.this);
					map14.addItem(item14);
					lottelist.add(map14);

					lottepoint15 = new GeoPoint(35196300,129060640);
					Drawable drawable15 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item15 = new OverlayItem(lottepoint15, "맛집",
							"미스터피자");
					MapOverlay map15 = new MapOverlay(drawable15,LotteLayout.this);
					map15.addItem(item15);
					lottelist.add(map15);

					lottepoint16 = new GeoPoint(35195971,129059567);
					Drawable drawable16 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item16 = new OverlayItem(lottepoint16, "맛집",
							"주문진식당");
					MapOverlay map16 = new MapOverlay(drawable16,LotteLayout.this);
					map16.addItem(item16);
					lottelist.add(map16);



				}

				// startLocationService();

			}

		});
		/** 홈으로 가기 버튼 */
		ImageButton lotteHbutton5 = (ImageButton) findViewById(R.id.lottebutton5);
		lotteHbutton5.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent myIntent3 = new Intent(LotteLayout.this,
						MainActivity.class);
				myIntent3.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				myIntent3.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
				startActivity(myIntent3);
				finish();
			}
		});
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

	}

	private LocalActivityManager getLocalActivityManager() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected boolean isRouteDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}

	private void startLocationService() {
		LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		Location location = manager
				.getLastKnownLocation(LocationManager.GPS_PROVIDER);

		GPSListener gpsListener = new GPSListener();
		long minTime = 10000;
		float minDistance = 0;

		manager.requestLocationUpdates(LocationManager.GPS_PROVIDER, minTime,
				minDistance, gpsListener);

	}

	private class GPSListener implements LocationListener {

		public void onLocationChanged(Location location) {
			Double latitude = location.getLatitude();
			Double longitude = location.getLongitude();

			showCurrentLocation(latitude, longitude);
		}

		public void onProviderDisabled(String provider) {
		}

		public void onProviderEnabled(String provider) {
		}

		public void onStatusChanged(String provider, int status, Bundle extras) {
		}
	}

	/**
	 * void showCurrentLocation
	 * 
	 * @param latitude
	 *            : Double type
	 * @param longitude
	 *            : Double type
	 */

	private void showCurrentLocation(Double latitude, Double longitude) {
		double intLatitude = latitude.doubleValue() * 1000000;
		double inLongitude = longitude.doubleValue() * 1000000;

		GeoPoint geoPt = new GeoPoint((int) intLatitude, (int) inLongitude);

		MapController controller = lottemapView.getController();
		controller.animateTo(geoPt);

		int maxZoomlevel = lottemapView.getMaxZoomLevel();
		int zoomLevel = (int) ((maxZoomlevel + 1) / 1.15);

		controller.setZoom(zoomLevel);
		controller.setCenter(geoPt);
		lottemapView.setSatellite(false);
		lottemapView.setTraffic(false);

	}

	/**
	 * void onActivityResult save result of twitter activity
	 * 
	 * @param requestCode
	 *            : int type
	 * @param resultCode
	 *            : int type
	 * @param data
	 *            : Intent type
	 */
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		Log.e("DAY", "Resultcode : " + resultCode);

		if (resultCode == RESULT_OK) {

			STATICVALUES.token = data.getStringExtra("token");
			STATICVALUES.tokenSecret = data.getStringExtra("tokenSecret");
			STATICVALUES.nick = data.getStringExtra("nick");

			Toast.makeText(this, STATICVALUES.nick + "로그인 되었습니다.!",
					Toast.LENGTH_SHORT).show();

			Log.e("DAY", STATICVALUES.token);
			Log.e("DAY", STATICVALUES.tokenSecret);
			Log.e("DAY", STATICVALUES.nick);

		}

	}

	// 버튼 리스너
	OnClickListener listener = new OnClickListener() {

		@Override
		public void onClick(View arg0) {
			lottehp.open(386);
		}

	};

	// 업데이트하기
	private void listUpdate() {
		lottesa.notifyDataSetChanged();
	}

}
