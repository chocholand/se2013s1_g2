package com.example.BaseBallSNS;

import com.example.BaseBallSNS.R;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
/** class StartLogo extends Activity
 * Start Logo page
 * @author LeeJungSu
 * @see MainActivity.java
 */

public class StartLogo extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState); 
        //Set Content View
        setContentView(R.layout.startlogo);      
        initialize();
    }
       
    /**void initialize()
     * 시작 페이지 로고 시간 설정
     */
    private void initialize()
        {
        Handler handler = new Handler () 
        {	
        	@Override
        	public void handleMessage(Message msg) {
        		finish();// Activity 종료
        	}
        };
        handler.sendEmptyMessageDelayed(0, 3000);// 3초후 종료시킴
        }

}
 

