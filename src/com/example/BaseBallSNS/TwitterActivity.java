
package com.example.BaseBallSNS;


import twitter4j.Twitter;

import twitter4j.TwitterFactory;
import twitter4j.http.AccessToken;
import twitter4j.http.RequestToken;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
/**class TwitterActivity
 * Login Twitter
 * @author SE2013S1_2조(돌직구),Lee Jung su
 * @version 3.0
 * @since 2013. 05. 19.
 * @see KiaLayout.java
 */

public class TwitterActivity extends Activity {

	Twitter twitter;
	RequestToken requestToken;
	AccessToken accessToken;
	
	String accTok;
	String accTokSec;
	WebView webView;
	
	String pin = null;
	
	
	public Handler mHandler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case 0:		
				//종료를 태우던지 다시 시작한다.	
				Toast.makeText(TwitterActivity.this, "인증 실패!", Toast.LENGTH_SHORT).show();				
				setResult(0);		
				finish();		
				break;
			case 1:				
				requestToken = (RequestToken)msg.obj;				
				webView.loadUrl(requestToken.getAuthorizationURL());
				webView.addJavascriptInterface(new MyJavaScriptInterface(), "GETPINNUMBER");			
				break;
			case 2:				
				accessToken = (AccessToken)msg.obj;				   		
		   		Intent intent = getIntent();		   		
		   		intent.putExtra("token", accessToken.getToken());
		   		intent.putExtra("tokenSecret", accessToken.getTokenSecret());
		   		intent.putExtra("nick", accessToken.getScreenName());	   				   				   		
		   		setResult(-1 ,intent);		   			   		
		   		finish();			
				break;
			}
		}

	};
		
	@Override
	protected void onCreate(Bundle savedInstanceState) {		
		super.onCreate(savedInstanceState);	
		requestWindowFeature(Window.FEATURE_NO_TITLE);			
		setContentView(R.layout.twitter);				
		webView = (WebView) findViewById(R.id.webView);				
		webView.setWebViewClient(new CallBack());		
		webView.getSettings().setJavaScriptEnabled(true);		
		twitter = new TwitterFactory().getInstance();	
		//Consumer key
		String consumerKey = STATICVALUES.consumerKey;
		//Consumer secret		
		String consumerSecret = STATICVALUES.consumerSecret;		
		RequestTokenAsyncTask requestTokenAsyncTask = new RequestTokenAsyncTask(this,twitter, mHandler);			
		requestTokenAsyncTask.execute(consumerKey,consumerSecret);				
	}
	/**class MyJavaScriptInterface
	 * @author SE2013S1_2조(돌직구),Lee Jung Su
	 * @version 3.0
	 * @since 2013. 05. 19.
	 */
	class MyJavaScriptInterface{
		/**
		 * void showHTML
		 * @param pin : String Type
		 */
		public void showHTML(String pin){
			if(pin != null && !pin.equals("")){
				//getAccessToken(pin);
				//mListener.onComplete();				
				Log.e("DAY", "PIN : " + pin);					
				final String realpin = parserNumber(pin);					
				try {				
					runOnUiThread(new Runnable() {
						public void run() {							
							OAuthTokenAsyncTask oAuthTokenAsyncTask = new OAuthTokenAsyncTask(TwitterActivity.this, twitter ,mHandler);								
							oAuthTokenAsyncTask.execute(requestToken.getToken(),requestToken.getTokenSecret(), realpin );
						}
					});									
				} catch (Exception e) {
					// TODO: handle exception
					Log.e("DAY", e.getMessage());
				}			
			}		
		}
	}	
	/**
	 * String parserNumber
	 * @param a
	 * @return pin
	 */
	
	public String parserNumber(String a) {
		String pin = a;
		String s = a;
		String[] tokens = s.split(">");		
		pin = tokens[5].substring(0,7);			
		Log.e("DAY", "Parse PIN : " +  pin);		
		return pin;
	}		  
	
	/**
	 * class CallBack
	 * @author SE2013S1_2조(돌직구),Lee Jung Su
	 * @version 3.0
	 */
	private class CallBack extends WebViewClient {
		
		ProgressDialog dialog = new ProgressDialog(TwitterActivity.this);		
		
		/**
		 * boolean shouldOverrideUrlLoading
		 * @param view : WebView type
		 * @param url : String Type
		 * @return true
		 */
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			view.loadUrl(url);			
			Log.e("DAY", "shouldOverrideUrlLoading!"); 
			return true;
		}

		/**
		 * void onLoadResource
		 * @param view : WebView Type
		 * @param url :String Type
		 */
		@Override
		public void onLoadResource(WebView view, String url) {
			super.onLoadResource(view, url);
			
			  Log.e("DAY", "onLoadResource!"); 
		}
		
		/**
		 * void onReceivedError
		 * @param view : WebView Type
		 * @param errorCode: int Type
		 * @param description : String Type
		 * @param failingurl : String Type
		 */
		@Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            super.onReceivedError(view, errorCode, description, failingUrl);
            
            Log.e("DAY", "onReceivedError!"); 
  
        }
		/**void onPageStarted
		 * @param view : WebView type
		 * @param url : String type
		 * @param favicon : Bitmap type
		 */
		@Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
			
            super.onPageStarted(view, url, favicon);            
            
            Log.e("DAY", "onPageStarted!");                        
           
			dialog.setMessage("Loading page . . .");		
			if(!dialog.isShowing())
			{
				try {
					dialog.show(); 
				} catch (Exception e) {
					// TODO: handle exception
				}				          
			}
        }		
		/**void onPageFinished
		 * @param view : WebView
		 * @param url : String
		 */
		@Override
		public void onPageFinished(WebView view, String url) {
			super.onPageFinished(view, url);
			
			Log.e("DAY", "onPageFinished!");			
			dialog.dismiss();		
			
			view.loadUrl("javascript:window.GETPINNUMBER.showHTML(document.getElementById('oauth_pin').innerHTML);");
			
		}
	}	
}