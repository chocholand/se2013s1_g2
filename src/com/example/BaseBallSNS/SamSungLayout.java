package com.example.BaseBallSNS;

import java.util.*;
import java.util.zip.Inflater;

import com.example.BaseBallSNS.R;
import com.google.android.maps.*;

import android.app.*;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import android.location.*;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.*;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.*;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.graphics.drawable.Drawable;
import com.google.android.maps.*;
import com.google.android.maps.MapView.LayoutParams;


/**
 * @author LeeJungsu
 * @param kiaSelect
 *            : boolean Type.
 */

public class SamSungLayout extends MapActivity implements OnClickListener {
	View top, TimeLine2, menu2, menu3;
	ListView listview;
	MapView SamsungmapView; // //맵뷰 객체
	MapController mc;
	GeoPoint Samsungpoint, Samsungpoint2, Samsungpoint3, Samsungpoint4,
			Samsungpoint5, Samsungpoint6, Samsungpoint7, Samsungpoint8,
			Samsungpoint9, Samsungpoint10, Samsungpoint11, Samsungpoint12,
			Samsungpoint13, Samsungpoint14, Samsungpoint15, Samsungpoint16,
			Samsungpoint17, Samsungpoint18, Samsungpoint19, Samsungpoint20,
			Samsungpoint21, Samsungpoint22;
	List<Overlay> Samsunglist; // 맵 오버레이 포함하는 리스트
	EditText comment;
	ArrayList<MyItem> arItem;
	private ArrayList<HashMap<String, String>> data;
	private Button bt_open;
	private ListView listview2;
	private SimpleAdapter sa;
	private Net_HTMLParse hp;
	public static HashMap<String, String> samsungmap = new HashMap<String, String>();
	private final Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			listUpdate();
		}
	};

	public static boolean SamSungSelect = true;

	@Override
	/**
	 * onCreate()
	 * 화면 생성 함수, void type
	 * @author Lee Jung Su
	 * @param Bundle savedInstanceState
	 */
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.samsung);

		// top = findViewById(R.id.top);
		// top.setVisibility(View.INVISIBLE);
		SamsungmapView = (MapView) findViewById(R.id.smapview);
		SamsungmapView.setVisibility(View.INVISIBLE);
		// TimeLine2 = findViewById(R.id.list);
		// TimeLine2.setVisibility(View.INVISIBLE);
		menu2 = findViewById(R.id.smenu2);
		menu2.setVisibility(View.INVISIBLE);
		menu3 = findViewById(R.id.smenu3);
		menu3.setVisibility(View.INVISIBLE);
		listview = (ListView) findViewById(R.id.slist);
		listview.setVisibility(View.INVISIBLE);
		listview2 = (ListView) findViewById(R.id.slist2);
		listview2.setVisibility(View.VISIBLE);

		// hp.open();

		data = new ArrayList<HashMap<String, String>>();

		// bt_open = (Button)findViewById(R.id.bt_open);

		hp = new Net_HTMLParse(this, handler, data);

		sa = new SimpleAdapter(this, data, R.layout.homeview, new String[] {
				"date", "team1", "score","team2","time","where" }, new int[] { R.id.date, R.id.team1,
				R.id.score, R.id.team2, R.id.time, R.id.where });

		// listview2.setOnClickListener(listener);
		listview2.setAdapter(sa);
		hp.open(383);
		/* 홈버튼 */
		ImageButton kbutton1 = (ImageButton) findViewById(R.id.sbutton1);
		kbutton1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				listview.setVisibility(View.INVISIBLE);
				SamsungmapView.setVisibility(View.INVISIBLE);
				// top.setVisibility(View.INVISIBLE);
				menu2.setVisibility(View.INVISIBLE);
				menu3.setVisibility(View.INVISIBLE);

				hp.open(383);

				listview2.setVisibility(View.VISIBLE);
				// startActivity(myIntent);
			}
		});

		TeamAudio Kiaaudio = new TeamAudio();
		arItem = new ArrayList<MyItem>();
		MyItem mi;
		mi = new MyItem(R.drawable.samsungryu, "류중일", "감독", "1963년 04월28일",
				"176cm / 70kg",samsungmap.get("이승엽"),"가사가사가사");
		arItem.add(mi);
		mi = new MyItem(R.drawable.samsungplayer1, "김희걸", "투수", "1981년 03월26일",
				"182cm / 82kg",samsungmap.get("이승엽"),"가사가사가사");
		arItem.add(mi);
		mi = new MyItem(R.drawable.samsungplayer2, "로드리게스", "투수",
				"1987년 12월13일", "183cm / 91kg",samsungmap.get("이승엽"),"가사가사가사");
		arItem.add(mi);
		mi = new MyItem(R.drawable.samsungplayer3, "배영수", "투수", "1981년 05월04일",
				"184cm / 84kg",samsungmap.get("이승엽"),"가사가사가사");
		arItem.add(mi);
		mi = new MyItem(R.drawable.samsungplayer4, "백정현", "투수", "1987년 07월13일",
				"184cm / 80kg",samsungmap.get("이승엽"),"가사가사가사");
		arItem.add(mi);
		mi = new MyItem(R.drawable.samsungplayer5, "벤젠헐크", "투수",
				"1985년 05월 22일", "196cm / 98kg",samsungmap.get("이승엽"),"가사가사가사");
		arItem.add(mi);
		mi = new MyItem(R.drawable.samsungplayer6, "안지만", "투수", "1983년 10월 1일",
				"180cm / 83kg",samsungmap.get("이승엽"),"가사가사가사");
		arItem.add(mi);
		mi = new MyItem(R.drawable.samsungplayer7, "오승환", "투수",
				"1982년 07월 15일", "178cm / 91kg",samsungmap.get("이승엽"),"가사가사가사");
		arItem.add(mi);
		mi = new MyItem(R.drawable.samsungplayer8, "윤성환", "투수", "1981년 10월 8일",
				"193cm / 88kg",samsungmap.get("이승엽"),"가사가사가사");
		arItem.add(mi);
		mi = new MyItem(R.drawable.samsungplayer9, "장원삼", "투수", "1983년 06월 9일",
				"181cm / 81kg",samsungmap.get("이승엽"),"가사가사가사");
		arItem.add(mi);
		mi = new MyItem(R.drawable.samsungplayer10, "진갑용", "포수",
				"1974년 05월 08일", "182cm / 90kg",samsungmap.get("이승엽"),"가사가사가사");
		arItem.add(mi);
		mi = new MyItem(R.drawable.samsungplayer11, "강명구", "내야수",
				"1980년 10월 25일", "181cm / 73kg",samsungmap.get("이승엽"),"가사가사가사");
		arItem.add(mi);
		mi = new MyItem(R.drawable.samsungplayer12, "김상수", "내야수",
				"1990년 03월 23일", "177cm / 68kg",samsungmap.get("이승엽"),"가사가사가사");
		arItem.add(mi);
		mi = new MyItem(R.drawable.samsungplayer13, "김태완", "내야수",
				"1981년 09월 19일", "174cm / 81kg",samsungmap.get("이승엽"),"가사가사가사");
		arItem.add(mi);
		mi = new MyItem(R.drawable.samsungplayer14, "박석민", "내야수",
				"1985년 06월 22일", "178cm / 98kg",samsungmap.get("이승엽"),"가사가사가사");
		arItem.add(mi);
		mi = new MyItem(R.drawable.samsungplayer15, "신명철", "내야수",
				"1978년 08월 06일", "181cm / 80kg",samsungmap.get("이승엽"),"가사가사가사");
		arItem.add(mi);
		mi = new MyItem(R.drawable.samsungplayer16, "이승엽", "내야수",
				"1976년 08월 18일", "183cm / 93kg",samsungmap.get("이승엽"),"가사가사가사");
		arItem.add(mi);
		mi = new MyItem(R.drawable.samsungplayer17, "조동찬", "내야수",
				"1983년 07월 27일", "185cm / 88kg",samsungmap.get("이승엽"),"가사가사가사");
		arItem.add(mi);
		mi = new MyItem(R.drawable.samsungplayer18, "채태인", "내야수",
				"1982년 10월 11일", "188cm / 100kg",samsungmap.get("이승엽"),"가사가사가사");
		arItem.add(mi);
		mi = new MyItem(R.drawable.samsungplayer19, "최형우", "외야수",
				"1983년 12월 16일", "179cm / 86kg",samsungmap.get("이승엽"),"가사가사가사");
		arItem.add(mi);
		mi = new MyItem(R.drawable.samsungplayer20, "배영섭", "외야수",
				"1986년 06월 27일", "179cm / 86kg",samsungmap.get("이승엽"),"가사가사가사");
		arItem.add(mi);
		mi = new MyItem(R.drawable.samsungplayer21, "정형식", "외야수",
				"1991년 01월 28일", "179cm / 86kg",samsungmap.get("이승엽"),"가사가사가사");
		arItem.add(mi);
		mi = new MyItem(R.drawable.samsungplayer22, "우동균", "외야수",
				"1989년 12월 3일", "175cm / 76kg",samsungmap.get("이승엽"),"가사가사가사");
		arItem.add(mi);
		MyListAdapter MyAdapter = new MyListAdapter(this, R.layout.kiaplayer,
				arItem);

		ListView MyList;
		MyList = (ListView) findViewById(R.id.slist);
		MyList.setAdapter(MyAdapter);

		/** 선수정보 버튼 */
		ImageButton kbutton2 = (ImageButton) findViewById(R.id.sbutton2);
		kbutton2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				listview.setVisibility(View.VISIBLE);
				SamsungmapView.setVisibility(View.INVISIBLE);
				// top.setVisibility(View.INVISIBLE);
				menu2.setVisibility(View.INVISIBLE);
				menu3.setVisibility(View.INVISIBLE);

				// startActivity(myIntent);
			}
		});

		/** 트위터 버튼 */
		ImageButton kbutton3 = (ImageButton) findViewById(R.id.sbutton3);
		kbutton3.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				menu2.setVisibility(View.VISIBLE);
				menu3.setVisibility(View.VISIBLE);
				SamsungmapView.setVisibility(View.INVISIBLE);
				listview.setVisibility(View.INVISIBLE);
				listview2.setVisibility(View.INVISIBLE);
				// TimeLine2.setVisibility(View.INVISIBLE);

			}
		});
		/** 트위터 로그인버튼 */
		ImageButton button = (ImageButton) findViewById(R.id.slogin);
		button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent myIntent = new Intent(SamSungLayout.this,
						TwitterActivity.class);
				startActivityForResult(myIntent, 1004);
				// startActivity(myIntent);
			}
		});
		/** 트위터 글쓰기버튼 */
		final HashTag hash = new HashTag("");
		ImageButton button1 = (ImageButton) findViewById(R.id.stweet);
		comment = (EditText) findViewById(R.id.scomment);
		button1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				String commentString = comment.getText().toString();
				if (STATICVALUES.token != null
						&& STATICVALUES.tokenSecret != null) {
					RegistAsyncTask registAsyncTask = new RegistAsyncTask(
							SamSungLayout.this);
					registAsyncTask.execute(STATICVALUES.token,
							STATICVALUES.tokenSecret, commentString);
				} else {
					Toast.makeText(SamSungLayout.this, "로그인해주세요!",
							Toast.LENGTH_SHORT).show();
				}
				comment.setText("");

			}
		});
		CheckBox Rbutton1 = (CheckBox) findViewById(R.id.scheckBox1);
		Rbutton1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				ImageButton button1 = (ImageButton) findViewById(R.id.stweet);
				button1.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						hash.ResultTag = "#기아타이거즈";
						String commentString = hash.MakeHash("#기아타이거즈") + " "
								+ comment.getText().toString();
						if (STATICVALUES.token != null
								&& STATICVALUES.tokenSecret != null) {
							RegistAsyncTask registAsyncTask = new RegistAsyncTask(
									SamSungLayout.this);
							registAsyncTask.execute(STATICVALUES.token,
									STATICVALUES.tokenSecret, commentString);
						} else {
							Toast.makeText(SamSungLayout.this, "로그인해주세요!",
									Toast.LENGTH_SHORT).show();
						}
						comment.setText("");
					}
				});
			}
		});
		CheckBox Rbutton2 = (CheckBox) findViewById(R.id.scheckBox2);
		Rbutton2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				ImageButton button1 = (ImageButton) findViewById(R.id.stweet);
				button1.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						hash.ResultTag = "#기아타이거즈2";
						String commentString = hash.ResultTag + " "
								+ comment.getText().toString();
						if (STATICVALUES.token != null
								&& STATICVALUES.tokenSecret != null) {
							RegistAsyncTask registAsyncTask = new RegistAsyncTask(
									SamSungLayout.this);
							registAsyncTask.execute(STATICVALUES.token,
									STATICVALUES.tokenSecret, commentString);
						} else {
							Toast.makeText(SamSungLayout.this, "로그인해주세요",
									Toast.LENGTH_SHORT).show();
						}
						comment.setText("");
					}
				});
			}
		});
		ImageButton tbutton = (ImageButton) findViewById(R.id.stweet2);
		tbutton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				menu2.setVisibility(View.VISIBLE);
				menu3.setVisibility(View.VISIBLE);

			}
		});

		ImageButton Timebutton = (ImageButton) findViewById(R.id.stimeline);
		Timebutton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// menu2.setVisibility(View.VISIBLE);
				// menu3.setVisibility(View.VISIBLE);
				AlertDialog diaBox = createDialogBox();
				diaBox.show();
				// top.setVisibility(View.INVISIBLE);
				// menu2.setVisibility(View.VISIBLE);
				// mapView.setVisibility(View.INVISIBLE);
				// TimeLine2.setVisibility(View.VISIBLE);
				// listview.setVisibility(View.INVISIBLE);

			}
		});

		/** 구글 맵 버튼 */
		ImageButton kbutton4 = (ImageButton) findViewById(R.id.sbutton4);
		kbutton4.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// top.setVisibility(View.INVISIBLE);
				menu2.setVisibility(View.INVISIBLE);
				menu3.setVisibility(View.INVISIBLE);
				SamsungmapView.setVisibility(View.VISIBLE);
				SamsungmapView.setBuiltInZoomControls(true);// zoom controller
				mc = SamsungmapView.getController();

				if (SamSungLayout.SamSungSelect == true) {
					Samsungpoint = new GeoPoint(35881227, 128586211);// 삼성 경기장
																		// 좌표 설정
					mc.animateTo(Samsungpoint);// 좌표점 이동
					mc.setZoom(18);// 확대 1~21

					Samsunglist = SamsungmapView.getOverlays();
					// 위치에 이미지 설정한다.
					Drawable drawable = getResources().getDrawable(
							R.drawable.mapsamsung);
					OverlayItem item = new OverlayItem(Samsungpoint,
							"대구시민야구경기장", "삼성라이온즈");
					MapOverlay map = new MapOverlay(drawable,SamSungLayout.this);
					map.addItem(item);
					Samsunglist.add(map);

					Samsungpoint2 = new GeoPoint(35880238, 128586076);// 우성식당
					Drawable drawable2 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item2 = new OverlayItem(Samsungpoint2, "맛집",
							"우성식당");
					MapOverlay map2 = new MapOverlay(drawable2,SamSungLayout.this);
					map2.addItem(item2);
					Samsunglist.add(map2);

					Samsungpoint3 = new GeoPoint(35880153, 128586318);
					Drawable drawable3 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item3 = new OverlayItem(Samsungpoint3, "맛집",
							"아송식당");
					MapOverlay map3 = new MapOverlay(drawable3,SamSungLayout.this);
					map3.addItem(item3);
					Samsunglist.add(map3);

					Samsungpoint4 = new GeoPoint(35880255, 128585706);
					Drawable drawable4 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item4 = new OverlayItem(Samsungpoint4, "맛집",
							"청정우");
					MapOverlay map4 = new MapOverlay(drawable4,SamSungLayout.this);
					map4.addItem(item4);
					Samsunglist.add(map4);

					Samsungpoint5 = new GeoPoint(35880179, 128585055);
					Drawable drawable5 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item5 = new OverlayItem(Samsungpoint5, "맛집",
							"종국이두마리치킨");
					MapOverlay map5 = new MapOverlay(drawable5,SamSungLayout.this);
					map5.addItem(item5);
					Samsunglist.add(map5);

					Samsungpoint6 = new GeoPoint(35880277, 128584886);
					Drawable drawable6 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item6 = new OverlayItem(Samsungpoint6, "맛집",
							"영천식당");
					MapOverlay map6 = new MapOverlay(drawable6,SamSungLayout.this);
					map6.addItem(item6);
					Samsunglist.add(map6);

					Samsungpoint7 = new GeoPoint(35880536, 129594709);
					Drawable drawable7 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item7 = new OverlayItem(Samsungpoint7, "맛집",
							"석미궁");
					MapOverlay map7 = new MapOverlay(drawable7,SamSungLayout.this);
					map7.addItem(item7);
					Samsunglist.add(map7);

					Samsungpoint8 = new GeoPoint(35880364, 128584352);
					Drawable drawable8 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item8 = new OverlayItem(Samsungpoint8, "맛집",
							"지붕위에파닭");
					MapOverlay map8 = new MapOverlay(drawable8,SamSungLayout.this);
					map8.addItem(item8);
					Samsunglist.add(map8);

					Samsungpoint9 = new GeoPoint(35880673, 128584065);
					Drawable drawable9 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item9 = new OverlayItem(Samsungpoint9, "맛집",
							"본가조방낙지");
					MapOverlay map9 = new MapOverlay(drawable9,SamSungLayout.this);
					map9.addItem(item9);
					Samsunglist.add(map9);

					Samsungpoint10 = new GeoPoint(35880734, 128584719);
					Drawable drawable10 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item10 = new OverlayItem(Samsungpoint10, "맛집",
							"솔밭");
					MapOverlay map10 = new MapOverlay(drawable10,SamSungLayout.this);
					map10.addItem(item10);
					Samsunglist.add(map10);

					Samsungpoint11 = new GeoPoint(35881233, 128585100);
					Drawable drawable11 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item11 = new OverlayItem(Samsungpoint11, "맛집",
							"예가손가손칼국수");
					MapOverlay map11 = new MapOverlay(drawable11,SamSungLayout.this);
					map11.addItem(item11);
					Samsunglist.add(map11);

					Samsungpoint12 = new GeoPoint(35882231, 128586557);
					Drawable drawable12 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item12 = new OverlayItem(Samsungpoint12, "맛집",
							"셔브티쓰리");
					MapOverlay map12 = new MapOverlay(drawable12,SamSungLayout.this);
					map12.addItem(item12);
					Samsunglist.add(map12);

					Samsungpoint13 = new GeoPoint(35883974, 128586323);
					Drawable drawable13 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item13 = new OverlayItem(Samsungpoint13, "맛집",
							"놀부부대찌게엔철판구이");
					MapOverlay map13 = new MapOverlay(drawable13,SamSungLayout.this);
					map13.addItem(item13);
					Samsunglist.add(map13);

					Samsungpoint14 = new GeoPoint(35883898, 128586066);
					Drawable drawable14 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item14 = new OverlayItem(Samsungpoint14, "맛집",
							"왕가네손짜장");
					MapOverlay map14 = new MapOverlay(drawable14,SamSungLayout.this);
					map14.addItem(item14);
					Samsunglist.add(map14);

					Samsungpoint15 = new GeoPoint(35883391, 128588757);
					Drawable drawable15 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item15 = new OverlayItem(Samsungpoint15, "맛집",
							"둘둘치킨");
					MapOverlay map15 = new MapOverlay(drawable15,SamSungLayout.this);
					map15.addItem(item15);
					Samsunglist.add(map15);

					Samsungpoint16 = new GeoPoint(3588261, 128590422);
					Drawable drawable16 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item16 = new OverlayItem(Samsungpoint16, "맛집",
							"대루강자탕");
					MapOverlay map16 = new MapOverlay(drawable16,SamSungLayout.this);
					map16.addItem(item16);
					Samsunglist.add(map16);

					Samsungpoint17 = new GeoPoint(35882913, 128590856);
					Drawable drawable17 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item17 = new OverlayItem(Samsungpoint17, "맛집",
							"어븐에꾸운닭");
					MapOverlay map17 = new MapOverlay(drawable17,SamSungLayout.this);
					map17.addItem(item17);
					Samsunglist.add(map17);

					Samsungpoint18 = new GeoPoint(35882896, 128591071);
					Drawable drawable18 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item18 = new OverlayItem(Samsungpoint18, "맛집",
							"피자헛");
					MapOverlay map18 = new MapOverlay(drawable18,SamSungLayout.this);
					map18.addItem(item18);
					Samsunglist.add(map18);

					Samsungpoint19 = new GeoPoint(35879949, 128587149);
					Drawable drawable19 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item19 = new OverlayItem(Samsungpoint19, "맛집",
							"조아조아");
					MapOverlay map19 = new MapOverlay(drawable19,SamSungLayout.this);
					map19.addItem(item19);
					Samsunglist.add(map19);

					Samsungpoint20 = new GeoPoint(35880058, 128586903);
					Drawable drawable20 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item20 = new OverlayItem(Samsungpoint20, "맛집",
							"부국통닭");
					MapOverlay map20 = new MapOverlay(drawable20,SamSungLayout.this);
					map20.addItem(item20);
					Samsunglist.add(map20);

					Samsungpoint21 = new GeoPoint(35881985, 128584776);
					Drawable drawable21 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item21 = new OverlayItem(Samsungpoint21, "맛집",
							"안동찜닭");
					MapOverlay map21 = new MapOverlay(drawable21,SamSungLayout.this);
					map21.addItem(item21);
					Samsunglist.add(map21);

					Samsungpoint22 = new GeoPoint(35882231, 128587375);
					Drawable drawable22 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item22 = new OverlayItem(Samsungpoint22, "맛집",
							"미즈치킨");
					MapOverlay map22 = new MapOverlay(drawable22,SamSungLayout.this);
					map22.addItem(item22);
					Samsunglist.add(map22);

				}

				// startLocationService();

			}

		});
		/** 홈으로 가기 버튼 */
		ImageButton kbutton5 = (ImageButton) findViewById(R.id.sbutton5);
		kbutton5.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent myIntent3 = new Intent(SamSungLayout.this,
						MainActivity.class);
				myIntent3.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				myIntent3.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
				startActivity(myIntent3);
				finish();
			}
		});
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

	}

	private LocalActivityManager getLocalActivityManager() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected boolean isRouteDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}

	private void startLocationService() {
		LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		Location location = manager
				.getLastKnownLocation(LocationManager.GPS_PROVIDER);

		GPSListener gpsListener = new GPSListener();
		long minTime = 10000;
		float minDistance = 0;

		manager.requestLocationUpdates(LocationManager.GPS_PROVIDER, minTime,
				minDistance, gpsListener);

	}

	private class GPSListener implements LocationListener {

		public void onLocationChanged(Location location) {
			Double latitude = location.getLatitude();
			Double longitude = location.getLongitude();

			showCurrentLocation(latitude, longitude);
		}

		public void onProviderDisabled(String provider) {
		}

		public void onProviderEnabled(String provider) {
		}

		public void onStatusChanged(String provider, int status, Bundle extras) {
		}
	}

	/**
	 * void showCurrentLocation
	 * 
	 * @param latitude
	 *            : Double type
	 * @param longitude
	 *            : Double type
	 */

	private void showCurrentLocation(Double latitude, Double longitude) {
		double intLatitude = latitude.doubleValue() * 1000000;
		double inLongitude = longitude.doubleValue() * 1000000;

		GeoPoint geoPt = new GeoPoint((int) intLatitude, (int) inLongitude);

		MapController controller = SamsungmapView.getController();
		controller.animateTo(geoPt);

		int maxZoomlevel = SamsungmapView.getMaxZoomLevel();
		int zoomLevel = (int) ((maxZoomlevel + 1) / 1.15);

		controller.setZoom(zoomLevel);
		controller.setCenter(geoPt);
		SamsungmapView.setSatellite(false);
		SamsungmapView.setTraffic(false);

	}

	/**
	 * void onActivityResult save result of twitter activity
	 * 
	 * @param requestCode
	 *            : int type
	 * @param resultCode
	 *            : int type
	 * @param data
	 *            : Intent type
	 */
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		Log.e("DAY", "Resultcode : " + resultCode);

		if (resultCode == RESULT_OK) {

			STATICVALUES.token = data.getStringExtra("token");
			STATICVALUES.tokenSecret = data.getStringExtra("tokenSecret");
			STATICVALUES.nick = data.getStringExtra("nick");

			Toast.makeText(this, STATICVALUES.nick + "로그인 되었습니다.!",
					Toast.LENGTH_SHORT).show();

			Log.e("DAY", STATICVALUES.token);
			Log.e("DAY", STATICVALUES.tokenSecret);
			Log.e("DAY", STATICVALUES.nick);

		}

	}

	// 버튼 리스너
	OnClickListener listener = new OnClickListener() {

		@Override
		public void onClick(View arg0) {
			hp.open(383);
		}

	};

	// 업데이트하기
	private void listUpdate() {
		sa.notifyDataSetChanged();
	}
	public AlertDialog createDialogBox() {
		// TODO Auto-generated method stub
		AlertDialog myDialog = new AlertDialog.Builder(this){
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(SamSungLayout.this, timeline.class);
				startActivityForResult(intent, 1004);
			}
		}
		.create();

		return myDialog;
	}
	

}
