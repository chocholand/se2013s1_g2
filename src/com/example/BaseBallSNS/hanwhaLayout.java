package com.example.BaseBallSNS;

import java.util.*;
import java.util.zip.Inflater;

import com.example.BaseBallSNS.R;
import com.google.android.maps.*;

import android.app.*;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import android.location.*;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.*;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.*;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.graphics.drawable.Drawable;

import com.google.android.maps.*;
import com.google.android.maps.MapView.LayoutParams;

/**
 * @author LeeJungsu
 * @param kiaSelect
 *            : boolean Type.
 */

public class hanwhaLayout extends MapActivity implements OnClickListener {
	View hanwhaTimeLine2, hanwhamenu2, hanwhamenu3;
	ListView hanwhalistview;
	MapView hanwhamapView; // //맵뷰 객체
	MapController hanwhamc;
	GeoPoint hanwhapoint, hanwhapoint2, hanwhapoint3, hanwhapoint4, hanwhapoint5,
			hanwhapoint6, hanwhapoint7, hanwhapoint8, hanwhapoint9, hanwhapoint10,
			hanwhapoint11, hanwhapoint12, hanwhapoint13, hanwhapoint14,
			hanwhapoint15, hanwhapoint16, hanwhapoint17;
	List<Overlay> hanwhalist; // 맵 오버레이 포함하는 리스트
	EditText hanwhacomment;
	ArrayList<MyItem> hanwhaarItem;
	private ArrayList<HashMap<String, String>> hanwhadata;
	private Button hanwhabt_open;
	private ListView hanwhalistview2;
	private SimpleAdapter hanwhasa;
	private Net_HTMLParse hanwhahp;
	public static HashMap<String, String> hanwhamap = new HashMap<String, String>();	
	private final Handler hanwhahandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			listUpdate();
		}
	};

	public static boolean hanwhaSelect = true;

	@Override
	/**
	 * onCreate()
	 * 화면 생성 함수, void type
	 * @author Lee Jung Su
	 * @param Bundle savedInstanceState
	 */
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.hanwha);

		// top = findViewById(R.id.top);
		// top.setVisibility(View.INVISIBLE);
		hanwhamapView = (MapView) findViewById(R.id.hanwhamapview);
		hanwhamapView.setVisibility(View.INVISIBLE);
		// TimeLine2 = findViewById(R.id.list);
		// TimeLine2.setVisibility(View.INVISIBLE);
		hanwhamenu2 = findViewById(R.id.hanwhamenu2);
		hanwhamenu2.setVisibility(View.INVISIBLE);
		hanwhamenu3 = findViewById(R.id.hanwhamenu3);
		hanwhamenu3.setVisibility(View.INVISIBLE);
		hanwhalistview = (ListView) findViewById(R.id.hanwhalist);
		hanwhalistview.setVisibility(View.INVISIBLE);
		hanwhalistview2 = (ListView) findViewById(R.id.hanwhalist2);
		hanwhalistview2.setVisibility(View.VISIBLE);

		// hp.open();

		hanwhadata = new ArrayList<HashMap<String, String>>();

		// bt_open = (Button)findViewById(R.id.bt_open);

		hanwhahp = new Net_HTMLParse(this, hanwhahandler, hanwhadata);

		hanwhasa = new SimpleAdapter(this, hanwhadata, R.layout.homeview, new String[] {
				"date", "team1", "score","team2","time","where" }, new int[] { R.id.date, R.id.team1,
				R.id.score, R.id.team2, R.id.time, R.id.where });

		// listview2.setOnClickListener(listener);
		hanwhalistview2.setAdapter(hanwhasa);
		hanwhahp.open(385);
		/* 홈버튼 */
		ImageButton hanwhabutton1 = (ImageButton) findViewById(R.id.hanwhabutton1);
		hanwhabutton1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				hanwhalistview.setVisibility(View.INVISIBLE);
				hanwhamapView.setVisibility(View.INVISIBLE);
				// top.setVisibility(View.INVISIBLE);
				hanwhamenu2.setVisibility(View.INVISIBLE);
				hanwhamenu3.setVisibility(View.INVISIBLE);

				hanwhahp.open(385);

				hanwhalistview2.setVisibility(View.VISIBLE);
				// startActivity(myIntent);
			}
		});
//		TeamAudio hanwhaaudio = new TeamAudio();
//		hanwhaarItem = new ArrayList<MyItem>();
//		MyItem mi;
//		mi = new MyItem(R.drawable.hanwhakim, "김진욱", "감독", "1960년 08월05일",
//				"182cm / 87kg",hanwhamap.get("이승엽"),"가사가사가사");
//		hanwhaarItem.add(mi);
//		mi = new MyItem(R.drawable.hanwhaplayer1, "정재훈", "투수", "1980년 01월01일",
//				"179cm / 81kg",hanwhamap.get("이승엽"),"가사가사가사");
//		hanwhaarItem.add(mi);
//		mi = new MyItem(R.drawable.hanwhaplayer2, "김선우", "투수", "1977년 09월04일",
//				"184cm / 87kg",hanwhamap.get("이승엽"),"가사가사가사");
//		hanwhaarItem.add(mi);
//		mi = new MyItem(R.drawable.hanwhaplayer3, "김상현", "투수", "1980년04월07일",
//				"181cm / 82kg",hanwhamap.get("이승엽"),"가사가사가사");
//		hanwhaarItem.add(mi);
//		mi = new MyItem(R.drawable.hanwhaplayer4, "김강률", "투수", "1988년 08월28일",
//				"187cm / 95kg",hanwhamap.get("이승엽"),"가사가사가사");
//		hanwhaarItem.add(mi);
//		mi = new MyItem(R.drawable.hanwhaplayer5, "올슨", "투수", "1983년 10월18일",
//				"185cm / 93kg",hanwhamap.get("이승엽"),"가사가사가사");
//		hanwhaarItem.add(mi);
//		mi = new MyItem(R.drawable.hanwhaplayer6, "임태훈", "투수", " 1988년 09월28일",
//				"182cm / 83kg",hanwhamap.get("이승엽"),"가사가사가사");
//		hanwhaarItem.add(mi);
//		mi = new MyItem(R.drawable.hanwhaplayer7, "이재우", "투수", " 1980년 02월09일",
//				"183cm / 82kg",hanwhamap.get("이승엽"),"가사가사가사");
//		hanwhaarItem.add(mi);
//		mi = new MyItem(R.drawable.hanwhaplayer8, "니퍼드", "투수", "1981년 05월06일",
//				"203cm / 103kg",hanwhamap.get("이승엽"),"가사가사가사");
//		hanwhaarItem.add(mi);
//		mi = new MyItem(R.drawable.hanwhaplayer9, "오현택", "투수", "1985년 07월17일",
//				"180cm / 73kg",hanwhamap.get("이승엽"),"가사가사가사");
//		hanwhaarItem.add(mi);
//		mi = new MyItem(R.drawable.hanwhaplayer10, "양의지	", "포수", "1987년 06월05일",
//				"179cm / 85kg",hanwhamap.get("이승엽"),"가사가사가사");
//		hanwhaarItem.add(mi);
//		mi = new MyItem(R.drawable.hanwhaplayer11, "최재훈", "포수",
//				"1989년 08월 27일", "178cm / 76kg",hanwhamap.get("이승엽"),"가사가사가사");
//		hanwhaarItem.add(mi);
//		mi = new MyItem(R.drawable.hanwhaplayer12, "최준석", "내야수",
//				"1983년 02월15일", "185cm / 115kg",hanwhamap.get("이승엽"),"가사가사가사");
//		hanwhaarItem.add(mi);
//		mi = new MyItem(R.drawable.hanwhaplayer13, "김재호", "내야수",
//				"1985년 03월21일", "181cm / 75kg",hanwhamap.get("이승엽"),"가사가사가사");
//		hanwhaarItem.add(mi);
//		mi = new MyItem(R.drawable.hanwhaplayer14, "홍성흔", "내야수",
//				"1977년 02월28일", "180cm / 96kg",hanwhamap.get("이승엽"),"가사가사가사");
//		hanwhaarItem.add(mi);
//		mi = new MyItem(R.drawable.hanwhaplayer15, "최주환", "내야수",
//				"1988년 02월28일", "178cm / 73kg",hanwhamap.get("이승엽"),"가사가사가사");
//		hanwhaarItem.add(mi);
//		mi = new MyItem(R.drawable.hanwhaplayer16, "허경민", "내야수",
//				"1990년 08월26일", "176cm / 69kg",hanwhamap.get("이승엽"),"가사가사가사");
//		hanwhaarItem.add(mi);
//		mi = new MyItem(R.drawable.hanwhaplayer17, "윤석민", "내야수",
//				"1985년 09월04일", "180cm / 86kg",hanwhamap.get("이승엽"),"가사가사가사");
//		hanwhaarItem.add(mi);
//		mi = new MyItem(R.drawable.hanwhaplayer18, "오재원", "내야수",
//				"1985년 02월09일", "185cm / 75kg",hanwhamap.get("이승엽"),"가사가사가사");
//		hanwhaarItem.add(mi);
//		mi = new MyItem(R.drawable.hanwhaplayer19, "박건우", "외야수",
//				"1990년 09월08일", "184cm / 80kg",hanwhamap.get("이승엽"),"가사가사가사");
//		hanwhaarItem.add(mi);
//		mi = new MyItem(R.drawable.hanwhaplayer20, "민병헌", "외야수",
//				"1987년 03월10일", "178cm / 78kg",hanwhamap.get("이승엽"),"가사가사가사");
//		hanwhaarItem.add(mi);
//		mi = new MyItem(R.drawable.hanwhaplayer21, "김현수", "외야수",
//				"1988년 01월12일", "188cm / 100kg",hanwhamap.get("이승엽"),"가사가사가사");
//		hanwhaarItem.add(mi);
//		mi = new MyItem(R.drawable.hanwhaplayer22, "이종욱", "외야수",
//				"1980년 06월18일", "176cm / 77kg",hanwhamap.get("이승엽"),"가사가사가사");
//		hanwhaarItem.add(mi);
//		mi = new MyItem(R.drawable.hanwhaplayer23, "정수빈", "외야수",
//				"1990년 10월07일", "175cm / 70kg",hanwhamap.get("이승엽"),"가사가사가사");
//		hanwhaarItem.add(mi);
//		MyListAdapter MyAdapter = new MyListAdapter(this, R.layout.kiaplayer,
//				hanwhaarItem);
//
//		ListView hanwhaMyList;
//		hanwhaMyList = (ListView) findViewById(R.id.hanwhalist);
//		hanwhaMyList.setAdapter(MyAdapter);

		/** 선수정보 버튼 */
		ImageButton hanwhabutton2 = (ImageButton) findViewById(R.id.hanwhabutton2);
		hanwhabutton2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				hanwhalistview.setVisibility(View.VISIBLE);
				hanwhamapView.setVisibility(View.INVISIBLE);
				// top.setVisibility(View.INVISIBLE);
				hanwhamenu2.setVisibility(View.INVISIBLE);
				hanwhamenu3.setVisibility(View.INVISIBLE);

				// startActivity(myIntent);
			}
		});

		/** 트위터 버튼 */
		ImageButton hanwhabutton3 = (ImageButton) findViewById(R.id.hanwhabutton3);
		hanwhabutton3.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				hanwhamenu2.setVisibility(View.VISIBLE);
				hanwhamenu3.setVisibility(View.VISIBLE);
				hanwhamapView.setVisibility(View.INVISIBLE);
				hanwhalistview.setVisibility(View.INVISIBLE);
				hanwhalistview2.setVisibility(View.INVISIBLE);
				// TimeLine2.setVisibility(View.INVISIBLE);

			}
		});
		/** 트위터 로그인버튼 */
		ImageButton button = (ImageButton) findViewById(R.id.hanwhalogin);
		button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent myIntent = new Intent(hanwhaLayout.this,
						TwitterActivity.class);
				startActivityForResult(myIntent, 1004);
				// startActivity(myIntent);
			}
		});
		/** 트위터 글쓰기버튼 */
		final HashTag hash = new HashTag("");
		ImageButton hanwhaButton1 = (ImageButton) findViewById(R.id.hanwhatweet);
		hanwhacomment = (EditText) findViewById(R.id.hanwhacomment);
		hanwhaButton1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				String commentString = hanwhacomment.getText().toString();
				if (STATICVALUES.token != null
						&& STATICVALUES.tokenSecret != null) {
					RegistAsyncTask registAsyncTask = new RegistAsyncTask(
							hanwhaLayout.this);
					registAsyncTask.execute(STATICVALUES.token,
							STATICVALUES.tokenSecret, commentString);
				} else {
					Toast.makeText(hanwhaLayout.this, "로그인해주세요!",
							Toast.LENGTH_SHORT).show();
				}
				hanwhacomment.setText("");

			}
		});
		CheckBox hanwhaRbutton1 = (CheckBox) findViewById(R.id.hanwhacheckBox1);
		hanwhaRbutton1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				ImageButton hanwhaButton1 = (ImageButton) findViewById(R.id.hanwhatweet);
				hanwhaButton1.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						hash.ResultTag = "#기아타이거즈";
						String commentString = hash.MakeHash("#기아타이거즈") + " "
								+ hanwhacomment.getText().toString();
						if (STATICVALUES.token != null
								&& STATICVALUES.tokenSecret != null) {
							RegistAsyncTask registAsyncTask = new RegistAsyncTask(
									hanwhaLayout.this);
							registAsyncTask.execute(STATICVALUES.token,
									STATICVALUES.tokenSecret, commentString);
						} else {
							Toast.makeText(hanwhaLayout.this, "로그인해주세요!",
									Toast.LENGTH_SHORT).show();
						}
						hanwhacomment.setText("");
					}
				});
			}
		});
		CheckBox hanwhaRbutton2 = (CheckBox) findViewById(R.id.hanwhacheckBox2);
		hanwhaRbutton2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				ImageButton hanwhaButton1 = (ImageButton) findViewById(R.id.hanwhatweet);
				hanwhaButton1.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						hash.ResultTag = "#기아타이거즈2";
						String commentString = hash.ResultTag + " "
								+ hanwhacomment.getText().toString();
						if (STATICVALUES.token != null
								&& STATICVALUES.tokenSecret != null) {
							RegistAsyncTask registAsyncTask = new RegistAsyncTask(
									hanwhaLayout.this);
							registAsyncTask.execute(STATICVALUES.token,
									STATICVALUES.tokenSecret, commentString);
						} else {
							Toast.makeText(hanwhaLayout.this, "로그인해주세요",
									Toast.LENGTH_SHORT).show();
						}
						hanwhacomment.setText("");
					}
				});
			}
		});
		ImageButton hanwhaTbutton = (ImageButton) findViewById(R.id.hanwhatweet);
		hanwhaTbutton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				hanwhamenu2.setVisibility(View.VISIBLE);
				hanwhamenu3.setVisibility(View.VISIBLE);

			}
		});

		ImageButton hanwhaTimebutton = (ImageButton) findViewById(R.id.hanwhatimeline);
		hanwhaTimebutton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				//hanwhamenu2.setVisibility(View.VISIBLE);
				// menu3.setVisibility(View.VISIBLE);
				timeline.searchTerm="#두산베어스";
				Intent intent = new Intent(hanwhaLayout.this, timeline.class);
				startActivity(intent);
				// top.setVisibility(View.INVISIBLE);
				// menu2.setVisibility(View.VISIBLE);
				//hanwhamapView.setVisibility(View.INVISIBLE);
				// TimeLine2.setVisibility(View.VISIBLE);
				//hanwhalistview.setVisibility(View.INVISIBLE);

			}
		});

		/** 구글 맵 버튼 */
		ImageButton hanwhaMbutton4 = (ImageButton) findViewById(R.id.hanwhabutton4);
		hanwhaMbutton4.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// top.setVisibility(View.INVISIBLE);
				hanwhamenu2.setVisibility(View.INVISIBLE);
				hanwhamenu3.setVisibility(View.INVISIBLE);
				hanwhamapView.setVisibility(View.VISIBLE);
				hanwhamapView.setBuiltInZoomControls(true);// zoom controller
				hanwhamc = hanwhamapView.getController();

				if (hanwhaLayout.hanwhaSelect == true) {
					hanwhapoint = new GeoPoint(35193819, 129061471);// 기아 경기장 좌표
																	// 설정
					hanwhamc.animateTo(hanwhapoint);// 좌표점 이동
					hanwhamc.setZoom(17);// 확대 1~21

					hanwhalist = hanwhamapView.getOverlays();
					// 위치에 이미지 설정한다.
					Drawable drawable = getResources().getDrawable(
							R.drawable.maphanwha);
					OverlayItem item = new OverlayItem(hanwhapoint, "부산사직야구장",
							"롯데자이언츠");
					MapOverlay map = new MapOverlay(drawable,hanwhaLayout.this);
					map.addItem(item);
					hanwhalist.add(map);

					hanwhapoint2 = new GeoPoint(35196074, 129059442);// 기아 경기장 좌표
																	// 설정
					Drawable drawable2 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item2 = new OverlayItem(hanwhapoint2, "맛집",
							"주문진막국수");
					MapOverlay map2 = new MapOverlay(drawable2,hanwhaLayout.this);
					map2.addItem(item2);
					hanwhalist.add(map2);

					hanwhapoint3 = new GeoPoint(35195541, 129058750);// 기아 경기장 좌표
																	// 설정
					Drawable drawable3 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item3 = new OverlayItem(hanwhapoint3, "맛집",
							"안양해물탕");
					MapOverlay map3 = new MapOverlay(drawable3,hanwhaLayout.this);
					map3.addItem(item3);
					hanwhalist.add(map3);

					hanwhapoint4 = new GeoPoint(35198551, 129064490);// 기아 경기장 좌표
																	// 설정
					Drawable drawable4 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item4 = new OverlayItem(hanwhapoint4, "맛집",
							"금강만두");
					MapOverlay map4 = new MapOverlay(drawable4,hanwhaLayout.this);
					map4.addItem(item4);
					hanwhalist.add(map4);

					hanwhapoint5 = new GeoPoint(35193521,129062002);
					Drawable drawable5 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item5 = new OverlayItem(hanwhapoint5, "맛집",
							"롯데리아");
					MapOverlay map5 = new MapOverlay(drawable5,hanwhaLayout.this);
					map5.addItem(item5);
					hanwhalist.add(map5);

					hanwhapoint6 = new GeoPoint(35193706,129064776);
					Drawable drawable6 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item6 = new OverlayItem(hanwhapoint6, "맛집",
							"서영식당");
					MapOverlay map6 = new MapOverlay(drawable6,hanwhaLayout.this);
					map6.addItem(item6);
					hanwhalist.add(map6);

					hanwhapoint7 = new GeoPoint(35195506,129064009);
					Drawable drawable7 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item7 = new OverlayItem(hanwhapoint7, "맛집",
							"마포진연탄구이");
					MapOverlay map7 = new MapOverlay(drawable7,hanwhaLayout.this);
					map7.addItem(item7);
					hanwhalist.add(map7);

					hanwhapoint8 = new GeoPoint(35195726,129063939);
					Drawable drawable8 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item8 = new OverlayItem(hanwhapoint8, "맛집",
							"비비큐치킨");
					MapOverlay map8 = new MapOverlay(drawable8,hanwhaLayout.this);
					map8.addItem(item8);
					hanwhalist.add(map8);

					hanwhapoint9 = new GeoPoint(35195423,129065452);
					Drawable drawable9 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item9 = new OverlayItem(hanwhapoint9, "맛집",
							"류가네더덕삼겹살");
					MapOverlay map9 = new MapOverlay(drawable9,hanwhaLayout.this);
					map9.addItem(item9);
					hanwhalist.add(map9);

					hanwhapoint10 = new GeoPoint(35195528,129064164);
					Drawable drawable10 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item10 = new OverlayItem(hanwhapoint10, "맛집",
							"조방낙지");
					MapOverlay map10 = new MapOverlay(drawable10,hanwhaLayout.this);
					map10.addItem(item10);
					hanwhalist.add(map10);

					hanwhapoint11 = new GeoPoint(35195436,129064577);
					Drawable drawable11 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item11 = new OverlayItem(hanwhapoint11, "맛집",
							"서원숯불바베큐");
					MapOverlay map11 = new MapOverlay(drawable11,hanwhaLayout.this);
					map11.addItem(item11);
					hanwhalist.add(map11);

					hanwhapoint12 = new GeoPoint(35193871,129064577);
					Drawable drawable12 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item12 = new OverlayItem(hanwhapoint12, "맛집",
							"대구막창왕갈비");
					MapOverlay map12 = new MapOverlay(drawable12,hanwhaLayout.this);
					map12.addItem(item12);
					hanwhalist.add(map12);

					hanwhapoint13 = new GeoPoint(35193538,129064583);
					Drawable drawable13 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item13 = new OverlayItem(hanwhapoint13, "맛집",
							"진우식당");
					MapOverlay map13 = new MapOverlay(drawable13,hanwhaLayout.this);
					map13.addItem(item13);
					hanwhalist.add(map13);

					hanwhapoint14 = new GeoPoint(35195940,129060618);
					Drawable drawable14 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item14 = new OverlayItem(hanwhapoint14, "맛집",
							"맥도날드");
					MapOverlay map14 = new MapOverlay(drawable14,hanwhaLayout.this);
					map14.addItem(item14);
					hanwhalist.add(map14);

					hanwhapoint15 = new GeoPoint(35196300,129060640);
					Drawable drawable15 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item15 = new OverlayItem(hanwhapoint15, "맛집",
							"미스터피자");
					MapOverlay map15 = new MapOverlay(drawable15,hanwhaLayout.this);
					map15.addItem(item15);
					hanwhalist.add(map15);

					hanwhapoint16 = new GeoPoint(35195971,129059567);
					Drawable drawable16 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item16 = new OverlayItem(hanwhapoint16, "맛집",
							"주문진식당");
					MapOverlay map16 = new MapOverlay(drawable16,hanwhaLayout.this);
					map16.addItem(item16);
					hanwhalist.add(map16);



				}

				// startLocationService();

			}

		});
		/** 홈으로 가기 버튼 */
		ImageButton hanwhaHbutton5 = (ImageButton) findViewById(R.id.hanwhabutton5);
		hanwhaHbutton5.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent myIntent3 = new Intent(hanwhaLayout.this,
						MainActivity.class);
				myIntent3.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				myIntent3.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
				startActivity(myIntent3);
				finish();
			}
		});
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

	}

	private LocalActivityManager getLocalActivityManager() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected boolean isRouteDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}

	private void startLocationService() {
		LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		Location location = manager
				.getLastKnownLocation(LocationManager.GPS_PROVIDER);

		GPSListener gpsListener = new GPSListener();
		long minTime = 10000;
		float minDistance = 0;

		manager.requestLocationUpdates(LocationManager.GPS_PROVIDER, minTime,
				minDistance, gpsListener);

	}

	private class GPSListener implements LocationListener {

		public void onLocationChanged(Location location) {
			Double latitude = location.getLatitude();
			Double longitude = location.getLongitude();

			showCurrentLocation(latitude, longitude);
		}

		public void onProviderDisabled(String provider) {
		}

		public void onProviderEnabled(String provider) {
		}

		public void onStatusChanged(String provider, int status, Bundle extras) {
		}
	}

	/**
	 * void showCurrentLocation
	 * 
	 * @param latitude
	 *            : Double type
	 * @param longitude
	 *            : Double type
	 */

	private void showCurrentLocation(Double latitude, Double longitude) {
		double intLatitude = latitude.doubleValue() * 1000000;
		double inLongitude = longitude.doubleValue() * 1000000;

		GeoPoint geoPt = new GeoPoint((int) intLatitude, (int) inLongitude);

		MapController controller = hanwhamapView.getController();
		controller.animateTo(geoPt);

		int maxZoomlevel = hanwhamapView.getMaxZoomLevel();
		int zoomLevel = (int) ((maxZoomlevel + 1) / 1.15);

		controller.setZoom(zoomLevel);
		controller.setCenter(geoPt);
		hanwhamapView.setSatellite(false);
		hanwhamapView.setTraffic(false);

	}

	/**
	 * void onActivityResult save result of twitter activity
	 * 
	 * @param requestCode
	 *            : int type
	 * @param resultCode
	 *            : int type
	 * @param data
	 *            : Intent type
	 */
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		Log.e("DAY", "Resultcode : " + resultCode);

		if (resultCode == RESULT_OK) {

			STATICVALUES.token = data.getStringExtra("token");
			STATICVALUES.tokenSecret = data.getStringExtra("tokenSecret");
			STATICVALUES.nick = data.getStringExtra("nick");

			Toast.makeText(this, STATICVALUES.nick + "로그인 되었습니다.!",
					Toast.LENGTH_SHORT).show();

			Log.e("DAY", STATICVALUES.token);
			Log.e("DAY", STATICVALUES.tokenSecret);
			Log.e("DAY", STATICVALUES.nick);

		}

	}

	// 버튼 리스너
	OnClickListener listener = new OnClickListener() {

		@Override
		public void onClick(View arg0) {
			hanwhahp.open(385);
		}

	};

	// 업데이트하기
	private void listUpdate() {
		hanwhasa.notifyDataSetChanged();
	}

}
