package com.example.BaseBallSNS;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

import net.htmlparser.jericho.Element;
import net.htmlparser.jericho.HTMLElementName;
import net.htmlparser.jericho.Source;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;

public class Net_HTMLParse
{
	private String url,date,team1,team2,score,score1,score2,sMonth,time,where;
	private Context context;
	private Handler handler;
	private ProgressDialog progressDialog;
	private Source source;
	private ArrayList<HashMap<String, String>> data;
	private int month, year,day;
	
	public static int teamNumber=0;
	//int teamId=386;
	
	public Net_HTMLParse(Context context, Handler handler, ArrayList<HashMap<String, String>> data)
	{
		this.context = context;
		this.handler = handler;
		this.data = data;
	}
	
	public void open(int teamId)
	{
		Calendar cal = new GregorianCalendar();
		month = cal.get(Calendar.MONTH)+1;
		year = cal.get(Calendar.YEAR);
		day = cal.get(Calendar.DAY_OF_MONTH)-1;
		if(month<10) sMonth="0"+month;
		else sMonth=""+month;
		 url = "http://sports.media.daum.net/baseball/kbo/record/tinf_schres.daum?team_id="+teamId+"+&game_year="+year+"&game_month="+sMonth+"&type=list";
		
		
		try
		{
			process();
		}catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	
	private void process() throws IOException
	{
		//상태 Progress 띄우기 위해서 사용함!
		final Handler mHandler = new Handler();
		new Thread()
		{

			@Override
			public void run()
			{
				URL nURL;
				try
				{
					nURL = new URL(url);
					mHandler.post(new Runnable(){

						@Override
						public void run()
						{
							progressDialog = ProgressDialog.show(context, "", "Data loding...");
						}
					});
					
					//모든 데이터 초기화
					data.clear();
					
					InputStream html = nURL.openStream();
					//가져오는 HTML의 인코딩형식
					source = new Source(new InputStreamReader(html, "UTF-8"));
					
					
					Element table = (Element) source.getAllElements(HTMLElementName.TABLE).get(0);
					
					
					
					int tr_count = table.getAllElements(HTMLElementName.TR).size();
					System.out.println(tr_count);
					Element tr = null;
					Element td = null;
					
					
					
					HashMap<String, String> hm = null;
					
					for(int i=1; i<tr_count; i++)
					{
						tr = (Element) table.getAllElements(HTMLElementName.TR).get(i);
						//td = (Element) tr.getAllElements(HTMLElementName.TR).get(4);
						hm = new HashMap<String, String>();							
						date = ((Element) tr.getAllElements(HTMLElementName.TD).get(0)).getContent().toString();
						try{
						team1 = ((Element) tr.getAllElements(HTMLElementName.A).get(0)).getContent().toString();
						team2 = ((Element) tr.getAllElements(HTMLElementName.A).get(1)).getContent().toString();
						}
						catch(Exception e){
							continue;
						}
						try{
							score1=((Element) tr.getAllElements(HTMLElementName.EM).get(0)).getContent().toString();
							score2=((Element) tr.getAllElements(HTMLElementName.EM).get(1)).getContent().toString();
							score= score1+":"+score2;
							time =((Element) tr.getAllElements(HTMLElementName.TD).get(4)).getContent().toString();
							where=((Element) tr.getAllElements(HTMLElementName.TD).get(7)).getContent().toString();
							}
						catch(Exception e){
							score="";
						
						
						time =((Element) tr.getAllElements(HTMLElementName.TD).get(2)).getContent().toString();
						where=((Element) tr.getAllElements(HTMLElementName.TD).get(5)).getContent().toString();
						}
						//	if(date2.equals(date.substring(2,5))){
						where =where +" 구장";
						System.out.println(score);
						hm.put("date", date);
						hm.put("team1", team1);
						hm.put("score",score);
						hm.put("team2", team2);
						hm.put("time", time);
						hm.put("where", where);
						System.out.println(score);
					//	hm.put("score", ((Element) tr.getAllElements(HTMLElementName.TD).get(3)).getContent().toString());
					//	hm.put("picher", ((Element) tr.getAllElements(HTMLElementName.TD).get(5)).getContent().toString());
						data.add(hm);
					//break;
					//	}
					
					}
					
					mHandler.post(new Runnable()
					{
						public void run()
						{
							progressDialog.cancel();
							//업데이트 완료를 핸들러로 보내줌
							handler.sendEmptyMessage(0);
						}
					});
				}catch (MalformedURLException e)
				{
					e.printStackTrace();
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
				
			}
			
		}.start();
	}
}
