package com.example.BaseBallSNS;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.OverlayItem;
//�������� �̹����� �ٿ쵵��  �����ִ� Ŭ����

public class MapOverlay extends ItemizedOverlay{
	//�������� �ѷ��� ���� �̹����� �����͸� �������ִ� Ŭ����
	ArrayList<OverlayItem> arrlist = new ArrayList<OverlayItem>();
	Context mcontext;
	// �������� �ѷ��� �̹����� �޾��ִ� ����
	/** MapOverlay(Drawable drawble, Context context)
	 *  sets Marker
	 * @param drawable :Drawable Type
	 * @param onClickListener : Context Type
	 * @return void
	 */	
	public MapOverlay(Drawable drawable ,Context context) {
		super(boundCenterBottom(drawable));
		mcontext = context;
		// TODO Auto-generated constructor stub
	}
	//�������� �̹����� �׷��ִ� �޼��� =size()
	/**createItem(int i)
	 * @param i : int Type
	 * @return arrlist.get(i)
	 */
	@Override
	protected OverlayItem createItem(int i) {
		// TODO Auto-generated method stub
		return arrlist.get(i);
	}
	/** size()
	 *  @return arrlist.size()
	 */
	@Override
	public int size() {
		// TODO Auto-generated method stub
		return arrlist.size();
	}
	/** addItem(OverlayItem item)
	 * Adds item
	 * @param item : OverlayItem Type
	 * @return void
	 */
	
	public void addItem(OverlayItem item) {
		arrlist.add(item);
		this.populate();
		// TODO Auto-generated method stub
		
	}
	
	protected boolean onTap(int arg0)
	{
	
		OverlayItem item = arrlist.get(arg0);
		AlertDialog.Builder dialog = new AlertDialog.Builder(mcontext);
		dialog.setTitle(item.getTitle());
		dialog.setMessage(item.getSnippet());
		dialog.show();		
		return true;			
	}
}
