package com.example.BaseBallSNS;

import java.util.*;
import java.util.zip.Inflater;

import com.example.BaseBallSNS.R;
import com.google.android.maps.*;

import android.app.*;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import android.location.*;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.*;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.*;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.graphics.drawable.Drawable;

import com.google.android.maps.*;
import com.google.android.maps.MapView.LayoutParams;

/**
 * @author LeeJungsu
 * @param kiaSelect
 *            : boolean Type.
 */

public class ncLayout extends MapActivity implements OnClickListener {
	View ncTimeLine2, ncmenu2, ncmenu3;
	ListView nclistview;
	MapView ncmapView; // //맵뷰 객체
	MapController ncmc;
	GeoPoint ncpoint, ncpoint2, ncpoint3, ncpoint4, ncpoint5,
			ncpoint6, ncpoint7, ncpoint8, ncpoint9, ncpoint10,
			ncpoint11, ncpoint12, ncpoint13, ncpoint14,
			ncpoint15, ncpoint16, ncpoint17;
	List<Overlay> nclist; // 맵 오버레이 포함하는 리스트
	EditText nccomment;
	ArrayList<MyItem> ncarItem;
	private ArrayList<HashMap<String, String>> ncdata;
	private Button ncbt_open;
	private ListView nclistview2;
	private SimpleAdapter ncsa;
	private Net_HTMLParse nchp;
	public static HashMap<String, String> ncmap = new HashMap<String, String>();
	private final Handler nchandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			listUpdate();
		}
	};

	public static boolean ncSelect = true;

	@Override
	/**
	 * onCreate()
	 * 화면 생성 함수, void type
	 * @author Lee Jung Su
	 * @param Bundle savedInstanceState
	 */
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.nc);

		// top = findViewById(R.id.top);
		// top.setVisibility(View.INVISIBLE);
		ncmapView = (MapView) findViewById(R.id.ncmapview);
		ncmapView.setVisibility(View.INVISIBLE);
		// TimeLine2 = findViewById(R.id.list);
		// TimeLine2.setVisibility(View.INVISIBLE);
		ncmenu2 = findViewById(R.id.ncmenu2);
		ncmenu2.setVisibility(View.INVISIBLE);
		ncmenu3 = findViewById(R.id.ncmenu3);
		ncmenu3.setVisibility(View.INVISIBLE);
		nclistview = (ListView) findViewById(R.id.nclist);
		nclistview.setVisibility(View.INVISIBLE);
		nclistview2 = (ListView) findViewById(R.id.nclist2);
		nclistview2.setVisibility(View.VISIBLE);

		// hp.open();

		ncdata = new ArrayList<HashMap<String, String>>();

		// bt_open = (Button)findViewById(R.id.bt_open);

		nchp = new Net_HTMLParse(this, nchandler, ncdata);

		ncsa = new SimpleAdapter(this, ncdata, R.layout.homeview, new String[] {
				"date", "team1", "score","team2","time","where" }, new int[] { R.id.date, R.id.team1,
				R.id.score, R.id.team2, R.id.time, R.id.where });

		// listview2.setOnClickListener(listener);
		nclistview2.setAdapter(ncsa);
		nchp.open(385);
		/* 홈버튼 */
		ImageButton ncbutton1 = (ImageButton) findViewById(R.id.ncbutton1);
		ncbutton1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				nclistview.setVisibility(View.INVISIBLE);
				ncmapView.setVisibility(View.INVISIBLE);
				// top.setVisibility(View.INVISIBLE);
				ncmenu2.setVisibility(View.INVISIBLE);
				ncmenu3.setVisibility(View.INVISIBLE);

				nchp.open(385);

				nclistview2.setVisibility(View.VISIBLE);
				// startActivity(myIntent);
			}
		});
		TeamAudio Kiaaudio = new TeamAudio();
		ncarItem = new ArrayList<MyItem>();
		MyItem mi;
		mi = new MyItem(R.drawable.nckim, "김진욱", "감독", "1960년 08월05일",
				"182cm / 87kg",ncmap.get("이승엽"),"가사가사가사");
		ncarItem.add(mi);
		mi = new MyItem(R.drawable.ncplayer1, "아담", "투수", "1980년 01월01일",
				"179cm / 81kg",ncmap.get("이승엽"),"가사가사가사");
		ncarItem.add(mi);
		mi = new MyItem(R.drawable.ncplayer2, "이창호", "투수", "1977년 09월04일",
				"184cm / 87kg",ncmap.get("이승엽"),"가사가사가사");
		ncarItem.add(mi);
		mi = new MyItem(R.drawable.ncplayer3, "에릭", "투수", "1980년04월07일",
				"181cm / 82kg",ncmap.get("이승엽"),"가사가사가사");
		ncarItem.add(mi);
		mi = new MyItem(R.drawable.ncplayer4, "황덕균", "투수", "1988년 08월28일",
				"187cm / 95kg",ncmap.get("이승엽"),"가사가사가사");
		ncarItem.add(mi);
		mi = new MyItem(R.drawable.ncplayer5, "김태군", "포수", "1983년 10월18일",
				"185cm / 93kg",ncmap.get("이승엽"),"가사가사가사");
		ncarItem.add(mi);
		mi = new MyItem(R.drawable.ncplayer6, "이호준", "내야수", " 1988년 09월28일",
				"182cm / 83kg",ncmap.get("이승엽"),"가사가사가사");
		ncarItem.add(mi);
		mi = new MyItem(R.drawable.ncplayer7, "조영훈", "내야수", " 1980년 02월09일",
				"183cm / 82kg",ncmap.get("이승엽"),"가사가사가사");
		ncarItem.add(mi);
		mi = new MyItem(R.drawable.ncplayer8, "모창민", "내야수", "1981년 05월06일",
				"203cm / 103kg",ncmap.get("이승엽"),"가사가사가사");
		ncarItem.add(mi);
		mi = new MyItem(R.drawable.ncplayer9, "지석훈", "내야수", "1985년 07월17일",
				"180cm / 73kg",ncmap.get("이승엽"),"가사가사가사");
		ncarItem.add(mi);
		mi = new MyItem(R.drawable.ncplayer10, "노진혁	", "외야수", "1987년 06월05일",
				"179cm / 85kg",ncmap.get("이승엽"),"가사가사가사");
		ncarItem.add(mi);
		mi = new MyItem(R.drawable.ncplayer11, "나성범", "외야수",
				"1989년 08월 27일", "178cm / 76kg",ncmap.get("이승엽"),"가사가사가사");
		ncarItem.add(mi);
		mi = new MyItem(R.drawable.ncplayer12, "김종호", "외야수",
				"1983년 02월15일", "185cm / 115kg",ncmap.get("이승엽"),"가사가사가사");
		ncarItem.add(mi);
		mi = new MyItem(R.drawable.ncplayer13, "마낙길", "외야수",
				"1985년 03월21일", "181cm / 75kg",ncmap.get("이승엽"),"가사가사가사");
		ncarItem.add(mi);
		mi = new MyItem(R.drawable.ncplayer14, "깅종찬", "외야수",
				"1977년 02월28일", "180cm / 96kg",ncmap.get("이승엽"),"가사가사가사");
		ncarItem.add(mi);
		mi = new MyItem(R.drawable.ncplayer15, "권희동", "외야수",
				"1988년 02월28일", "178cm / 73kg",ncmap.get("이승엽"),"가사가사가사");
		ncarItem.add(mi);

		MyListAdapter MyAdapter = new MyListAdapter(this, R.layout.kiaplayer,
				ncarItem);

		ListView ncMyList;
		ncMyList = (ListView) findViewById(R.id.nclist);
		ncMyList.setAdapter(MyAdapter);

		/** 선수정보 버튼 */
		ImageButton ncbutton2 = (ImageButton) findViewById(R.id.ncbutton2);
		ncbutton2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				nclistview.setVisibility(View.VISIBLE);
				ncmapView.setVisibility(View.INVISIBLE);
				// top.setVisibility(View.INVISIBLE);
				ncmenu2.setVisibility(View.INVISIBLE);
				ncmenu3.setVisibility(View.INVISIBLE);

				// startActivity(myIntent);
			}
		});

		/** 트위터 버튼 */
		ImageButton ncbutton3 = (ImageButton) findViewById(R.id.ncbutton3);
		ncbutton3.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				ncmenu2.setVisibility(View.VISIBLE);
				ncmenu3.setVisibility(View.VISIBLE);
				ncmapView.setVisibility(View.INVISIBLE);
				nclistview.setVisibility(View.INVISIBLE);
				nclistview2.setVisibility(View.INVISIBLE);
				// TimeLine2.setVisibility(View.INVISIBLE);

			}
		});
		/** 트위터 로그인버튼 */
		ImageButton button = (ImageButton) findViewById(R.id.nclogin);
		button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent myIntent = new Intent(ncLayout.this,
						TwitterActivity.class);
				startActivityForResult(myIntent, 1004);
				// startActivity(myIntent);
			}
		});
		/** 트위터 글쓰기버튼 */
		final HashTag hash = new HashTag("");
		ImageButton ncButton1 = (ImageButton) findViewById(R.id.nctweet);
		nccomment = (EditText) findViewById(R.id.nccomment);
		ncButton1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				String commentString = nccomment.getText().toString();
				if (STATICVALUES.token != null
						&& STATICVALUES.tokenSecret != null) {
					RegistAsyncTask registAsyncTask = new RegistAsyncTask(
							ncLayout.this);
					registAsyncTask.execute(STATICVALUES.token,
							STATICVALUES.tokenSecret, commentString);
				} else {
					Toast.makeText(ncLayout.this, "로그인해주세요!",
							Toast.LENGTH_SHORT).show();
				}
				nccomment.setText("");

			}
		});
		CheckBox ncRbutton1 = (CheckBox) findViewById(R.id.nccheckBox1);
		ncRbutton1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				ImageButton ncButton1 = (ImageButton) findViewById(R.id.nctweet);
				ncButton1.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						hash.ResultTag = "#기아타이거즈";
						String commentString = hash.MakeHash("#기아타이거즈") + " "
								+ nccomment.getText().toString();
						if (STATICVALUES.token != null
								&& STATICVALUES.tokenSecret != null) {
							RegistAsyncTask registAsyncTask = new RegistAsyncTask(
									ncLayout.this);
							registAsyncTask.execute(STATICVALUES.token,
									STATICVALUES.tokenSecret, commentString);
						} else {
							Toast.makeText(ncLayout.this, "로그인해주세요!",
									Toast.LENGTH_SHORT).show();
						}
						nccomment.setText("");
					}
				});
			}
		});
		CheckBox ncRbutton2 = (CheckBox) findViewById(R.id.nccheckBox2);
		ncRbutton2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				ImageButton ncButton1 = (ImageButton) findViewById(R.id.nctweet);
				ncButton1.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						hash.ResultTag = "#기아타이거즈2";
						String commentString = hash.ResultTag + " "
								+ nccomment.getText().toString();
						if (STATICVALUES.token != null
								&& STATICVALUES.tokenSecret != null) {
							RegistAsyncTask registAsyncTask = new RegistAsyncTask(
									ncLayout.this);
							registAsyncTask.execute(STATICVALUES.token,
									STATICVALUES.tokenSecret, commentString);
						} else {
							Toast.makeText(ncLayout.this, "로그인해주세요",
									Toast.LENGTH_SHORT).show();
						}
						nccomment.setText("");
					}
				});
			}
		});
		ImageButton ncTbutton = (ImageButton) findViewById(R.id.nctweet);
		ncTbutton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				ncmenu2.setVisibility(View.VISIBLE);
				ncmenu3.setVisibility(View.VISIBLE);

			}
		});

		ImageButton ncTimebutton = (ImageButton) findViewById(R.id.nctimeline);
		ncTimebutton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				//ncmenu2.setVisibility(View.VISIBLE);
				// menu3.setVisibility(View.VISIBLE);
				timeline.searchTerm="#두산베어스";
				Intent intent = new Intent(ncLayout.this, timeline.class);
				startActivity(intent);
				// top.setVisibility(View.INVISIBLE);
				// menu2.setVisibility(View.VISIBLE);
				//ncmapView.setVisibility(View.INVISIBLE);
				// TimeLine2.setVisibility(View.VISIBLE);
				//nclistview.setVisibility(View.INVISIBLE);

			}
		});

		/** 구글 맵 버튼 */
		ImageButton ncMbutton4 = (ImageButton) findViewById(R.id.ncbutton4);
		ncMbutton4.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// top.setVisibility(View.INVISIBLE);
				ncmenu2.setVisibility(View.INVISIBLE);
				ncmenu3.setVisibility(View.INVISIBLE);
				ncmapView.setVisibility(View.VISIBLE);
				ncmapView.setBuiltInZoomControls(true);// zoom controller
				ncmc = ncmapView.getController();

				if (ncLayout.ncSelect == true) {
					ncpoint = new GeoPoint(35193819, 129061471);// 기아 경기장 좌표
																	// 설정
					ncmc.animateTo(ncpoint);// 좌표점 이동
					ncmc.setZoom(17);// 확대 1~21

					nclist = ncmapView.getOverlays();
					// 위치에 이미지 설정한다.
					Drawable drawable = getResources().getDrawable(
							R.drawable.mapnc);
					OverlayItem item = new OverlayItem(ncpoint, "부산사직야구장",
							"롯데자이언츠");
					MapOverlay map = new MapOverlay(drawable,ncLayout.this);
					map.addItem(item);
					nclist.add(map);

					ncpoint2 = new GeoPoint(35196074, 129059442);// 기아 경기장 좌표
																	// 설정
					Drawable drawable2 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item2 = new OverlayItem(ncpoint2, "맛집",
							"주문진막국수");
					MapOverlay map2 = new MapOverlay(drawable2,ncLayout.this);
					map2.addItem(item2);
					nclist.add(map2);

					ncpoint3 = new GeoPoint(35195541, 129058750);// 기아 경기장 좌표
																	// 설정
					Drawable drawable3 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item3 = new OverlayItem(ncpoint3, "맛집",
							"안양해물탕");
					MapOverlay map3 = new MapOverlay(drawable3,ncLayout.this);
					map3.addItem(item3);
					nclist.add(map3);

					ncpoint4 = new GeoPoint(35198551, 129064490);// 기아 경기장 좌표
																	// 설정
					Drawable drawable4 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item4 = new OverlayItem(ncpoint4, "맛집",
							"금강만두");
					MapOverlay map4 = new MapOverlay(drawable4,ncLayout.this);
					map4.addItem(item4);
					nclist.add(map4);

					ncpoint5 = new GeoPoint(35193521,129062002);
					Drawable drawable5 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item5 = new OverlayItem(ncpoint5, "맛집",
							"롯데리아");
					MapOverlay map5 = new MapOverlay(drawable5,ncLayout.this);
					map5.addItem(item5);
					nclist.add(map5);

					ncpoint6 = new GeoPoint(35193706,129064776);
					Drawable drawable6 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item6 = new OverlayItem(ncpoint6, "맛집",
							"서영식당");
					MapOverlay map6 = new MapOverlay(drawable6,ncLayout.this);
					map6.addItem(item6);
					nclist.add(map6);

					ncpoint7 = new GeoPoint(35195506,129064009);
					Drawable drawable7 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item7 = new OverlayItem(ncpoint7, "맛집",
							"마포진연탄구이");
					MapOverlay map7 = new MapOverlay(drawable7,ncLayout.this);
					map7.addItem(item7);
					nclist.add(map7);

					ncpoint8 = new GeoPoint(35195726,129063939);
					Drawable drawable8 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item8 = new OverlayItem(ncpoint8, "맛집",
							"비비큐치킨");
					MapOverlay map8 = new MapOverlay(drawable8,ncLayout.this);
					map8.addItem(item8);
					nclist.add(map8);

					ncpoint9 = new GeoPoint(35195423,129065452);
					Drawable drawable9 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item9 = new OverlayItem(ncpoint9, "맛집",
							"류가네더덕삼겹살");
					MapOverlay map9 = new MapOverlay(drawable9,ncLayout.this);
					map9.addItem(item9);
					nclist.add(map9);

					ncpoint10 = new GeoPoint(35195528,129064164);
					Drawable drawable10 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item10 = new OverlayItem(ncpoint10, "맛집",
							"조방낙지");
					MapOverlay map10 = new MapOverlay(drawable10,ncLayout.this);
					map10.addItem(item10);
					nclist.add(map10);

					ncpoint11 = new GeoPoint(35195436,129064577);
					Drawable drawable11 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item11 = new OverlayItem(ncpoint11, "맛집",
							"서원숯불바베큐");
					MapOverlay map11 = new MapOverlay(drawable11,ncLayout.this);
					map11.addItem(item11);
					nclist.add(map11);

					ncpoint12 = new GeoPoint(35193871,129064577);
					Drawable drawable12 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item12 = new OverlayItem(ncpoint12, "맛집",
							"대구막창왕갈비");
					MapOverlay map12 = new MapOverlay(drawable12,ncLayout.this);
					map12.addItem(item12);
					nclist.add(map12);

					ncpoint13 = new GeoPoint(35193538,129064583);
					Drawable drawable13 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item13 = new OverlayItem(ncpoint13, "맛집",
							"진우식당");
					MapOverlay map13 = new MapOverlay(drawable13,ncLayout.this);
					map13.addItem(item13);
					nclist.add(map13);

					ncpoint14 = new GeoPoint(35195940,129060618);
					Drawable drawable14 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item14 = new OverlayItem(ncpoint14, "맛집",
							"맥도날드");
					MapOverlay map14 = new MapOverlay(drawable14,ncLayout.this);
					map14.addItem(item14);
					nclist.add(map14);

					ncpoint15 = new GeoPoint(35196300,129060640);
					Drawable drawable15 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item15 = new OverlayItem(ncpoint15, "맛집",
							"미스터피자");
					MapOverlay map15 = new MapOverlay(drawable15,ncLayout.this);
					map15.addItem(item15);
					nclist.add(map15);

					ncpoint16 = new GeoPoint(35195971,129059567);
					Drawable drawable16 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item16 = new OverlayItem(ncpoint16, "맛집",
							"주문진식당");
					MapOverlay map16 = new MapOverlay(drawable16,ncLayout.this);
					map16.addItem(item16);
					nclist.add(map16);



				}

				// startLocationService();

			}

		});
		/** 홈으로 가기 버튼 */
		ImageButton ncHbutton5 = (ImageButton) findViewById(R.id.ncbutton5);
		ncHbutton5.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent myIntent3 = new Intent(ncLayout.this,
						MainActivity.class);
				myIntent3.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				myIntent3.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
				startActivity(myIntent3);
				finish();
			}
		});
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

	}

	private LocalActivityManager getLocalActivityManager() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected boolean isRouteDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}

	private void startLocationService() {
		LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		Location location = manager
				.getLastKnownLocation(LocationManager.GPS_PROVIDER);

		GPSListener gpsListener = new GPSListener();
		long minTime = 10000;
		float minDistance = 0;

		manager.requestLocationUpdates(LocationManager.GPS_PROVIDER, minTime,
				minDistance, gpsListener);

	}

	private class GPSListener implements LocationListener {

		public void onLocationChanged(Location location) {
			Double latitude = location.getLatitude();
			Double longitude = location.getLongitude();

			showCurrentLocation(latitude, longitude);
		}

		public void onProviderDisabled(String provider) {
		}

		public void onProviderEnabled(String provider) {
		}

		public void onStatusChanged(String provider, int status, Bundle extras) {
		}
	}

	/**
	 * void showCurrentLocation
	 * 
	 * @param latitude
	 *            : Double type
	 * @param longitude
	 *            : Double type
	 */

	private void showCurrentLocation(Double latitude, Double longitude) {
		double intLatitude = latitude.doubleValue() * 1000000;
		double inLongitude = longitude.doubleValue() * 1000000;

		GeoPoint geoPt = new GeoPoint((int) intLatitude, (int) inLongitude);

		MapController controller = ncmapView.getController();
		controller.animateTo(geoPt);

		int maxZoomlevel = ncmapView.getMaxZoomLevel();
		int zoomLevel = (int) ((maxZoomlevel + 1) / 1.15);

		controller.setZoom(zoomLevel);
		controller.setCenter(geoPt);
		ncmapView.setSatellite(false);
		ncmapView.setTraffic(false);

	}

	/**
	 * void onActivityResult save result of twitter activity
	 * 
	 * @param requestCode
	 *            : int type
	 * @param resultCode
	 *            : int type
	 * @param data
	 *            : Intent type
	 */
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		Log.e("DAY", "Resultcode : " + resultCode);

		if (resultCode == RESULT_OK) {

			STATICVALUES.token = data.getStringExtra("token");
			STATICVALUES.tokenSecret = data.getStringExtra("tokenSecret");
			STATICVALUES.nick = data.getStringExtra("nick");

			Toast.makeText(this, STATICVALUES.nick + "로그인 되었습니다.!",
					Toast.LENGTH_SHORT).show();

			Log.e("DAY", STATICVALUES.token);
			Log.e("DAY", STATICVALUES.tokenSecret);
			Log.e("DAY", STATICVALUES.nick);

		}

	}

	// 버튼 리스너
	OnClickListener listener = new OnClickListener() {

		@Override
		public void onClick(View arg0) {
			nchp.open(385);
		}

	};

	// 업데이트하기
	private void listUpdate() {
		ncsa.notifyDataSetChanged();
	}

}
