
package com.example.BaseBallSNS;

import twitter4j.Twitter;

import twitter4j.TwitterException;
import twitter4j.http.RequestToken;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.webkit.WebView;
/**
 * class RequestTokenAsyncTask
 * requestToken 요청에 대한 클래스
 * @see : AsynTask<String, Void, AccessToken>, TwitterActivity.java
 * @author : <Open Source> Day young ( dryudryu@gmail.com )
 * @version : 3.0
 * @since : 2011. 3. 4. 오후 5:51:49 update history
 */
public class RequestTokenAsyncTask extends AsyncTask<String, Void, RequestToken> {
	
	private static final String TAG = "RequestTokenAsyncTask";	
	
	Twitter twitter;	
	
	Context context;	
	
	WebView webView;
	
	ProgressDialog dialog;
	
	Handler mHandler;
	
	/**RequestTokenAsyncTask
	 * 생성자
	 * @param : context : Context type
	 * @param : twitter : Twitter type
	 * @param : mHandler : Handler type
	 */
	public RequestTokenAsyncTask(Context context ,Twitter twitter, Handler mHandler) {		
		
		Log.e(TAG, "RequestTokenAsyncTask");
		
		this.context = context;
		this.mHandler = mHandler;
		this.twitter = twitter;
	}		
	/**void onPreExecute
	 * requestToken에 대한 프로그래스
	 */
	@Override
	protected void onPreExecute() {
		
		super.onPreExecute();	
		
		dialog = new ProgressDialog(context);		
		dialog.setMessage("Request to RequestToken . . .");		
		dialog.show();	
		
	}	
	/**void onCancelled
	 * 요청 취소
	 */

	@Override
	protected void onCancelled() {		
		super.onCancelled();
		
		dialog.dismiss();
	}
	
	/**RequestToken doInBackground
	 * background에서 수행할 작업을 구현해야 함. execute(…) 메소드에 입력된 인자들을 전달 받음.
	 * @param params[] : String type
	 * @return requestToken
	 */
	
	@Override
	protected RequestToken doInBackground(String... params) {
		// TODO Auto-generated method stub		
		
		twitter.setOAuthConsumer(params[0],params[1]);		
		
		RequestToken requestToken = null;
			
		try {
			requestToken = twitter.getOAuthRequestToken();
		} 		
		catch (TwitterException e) {
			e.printStackTrace();
		}		
		
		return requestToken;
		
	}

	
	/**void onPostExecute
	 * doInBackground(…)가 리턴하는 값을 바탕으로 UI스레드에 background 작업 결과를 표현하도록 구현 함. 
	 * @param requestToken : RequestToken type 
	 */	
	@Override
	protected void onPostExecute(RequestToken requestToken) {
		super.onPostExecute(requestToken);
		
		if(requestToken==null)
		{		
			Message sendMsg = Message.obtain();			
			sendMsg.what = 0;
			mHandler.sendMessage(sendMsg);
		}
		else
		{
			Message sendMsg = Message.obtain();			
			sendMsg.what = 1;
			sendMsg.obj = requestToken; 
			mHandler.sendMessage(sendMsg);
		}			
		dialog.dismiss();
	}
}
