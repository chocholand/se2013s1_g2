package com.example.BaseBallSNS;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.DataSetObserver;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.SoftReference;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class timeline extends ListActivity {
	/** Called when the activity is first created. */

	public ListView lv;
	public ImageView image;
	public ImageButton rbutton;
	public ImageButton sbutton;
	public EditText textResult;
	public static String searchTerm;
	public CharSequence searchEntry;
	public ArrayList<Tweet> myTweets;
	public ArrayList<Tweet> update;

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.timeline);

		

		myTweets = fetchTwitterTimeline(searchTerm);

		lv = (ListView) findViewById(R.id.twitterList);
		lv.setAdapter(new TweetAdapter(this, R.layout.timeline2, myTweets));
		//lv.set(new TweetAdapter(this, R.layout.timeline2, myTweets));
		lv.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

			}

		});

		rbutton = (ImageButton) findViewById(R.id.buttonRefresh);
		rbutton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				refreshFeed(searchTerm);
			}
		});

		sbutton = (ImageButton) findViewById(R.id.buttonSearch);
		sbutton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String searchTest = textResult.getText().toString()
						.replace(" ", ",");
				if (searchTest.equals("")) {
					Toast empty = Toast
							.makeText(
									timeline.this,
									"Whoops! You didn't search for anything! Please retry!",
									Toast.LENGTH_SHORT);
					empty.show();
				} else {
					searchTerm = searchTest;
					refreshFeed(searchTerm);
				}
				if (searchTest.startsWith("@")) {
					searchTerm = searchTest.substring(1);
					refreshFeed("from:" + searchTerm);
				}
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(textResult.getWindowToken(), 0);
				textResult.setText("");
			}
		});

		TextView title = (TextView) findViewById(R.id.textTitle);
		title.setText("\"" + searchTerm + "\"");

		textResult = (EditText) findViewById(R.id.text_result);
		textResult.requestFocus();
	}

	public void refreshFeed(String searchTerm) {

		update = fetchTwitterTimeline(searchTerm);

		lv = (ListView) findViewById(R.id.twitterList);
		lv.setAdapter(new TweetAdapter(this, R.layout.timeline2, update));
		lv.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
			}
		});

		TextView title = (TextView) findViewById(R.id.textTitle);
		title.setText("\"" + searchTerm.replace(",", " ") + "\"");

	}

	public ArrayList<Tweet> fetchTwitterTimeline(String searchItem) {

		ArrayList<Tweet> listItems = new ArrayList<Tweet>();

		try {
			// Increase default timeout length
			HttpParams httpParameters = new BasicHttpParams();
			HttpConnectionParams.setConnectionTimeout(httpParameters, 5000);
			HttpConnectionParams.setSoTimeout(httpParameters, 2000);
			// Create a new HTTP Client
			DefaultHttpClient defaultClient = new DefaultHttpClient(
					httpParameters);
			// Setup the get request
			HttpGet httpGetRequest = new HttpGet(
					"http://search.twitter.com/search.json?q="
							+ searchItem
							+ "&rpp=10&include_entities=true&result_type=recent");
			// Execute the request in the client
			HttpResponse httpResponse = defaultClient.execute(httpGetRequest);
			// Grab the response
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					httpResponse.getEntity().getContent(), "UTF-8"));
			String json = reader.readLine();
			// Instantiate a JSON Array from the request response
			JSONObject obj = new JSONObject(json);
			JSONArray jArray = obj.getJSONArray("results");
			// feed into ArrayList
			for (int i = 0; i < jArray.length(); i++) {
				JSONObject oneObject = jArray.getJSONObject(i);
				Tweet mytweet = new Tweet(oneObject.getString("text"),
						oneObject.getString("from_user"),
						oneObject.getString("profile_image_url"));
				listItems.add(mytweet);
				System.out.println("LOOK HEEERRRREEE!!!!!!");
				System.out.println(mytweet.toString());
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return listItems;
	}

	public User fetchUserInfoOnce(String username) {

		User myuser = null;

		try {
			// Create a new HTTP Client
			DefaultHttpClient defaultClient = new DefaultHttpClient();
			// Setup the get request
			HttpGet httpGetRequest = new HttpGet(
					"https://api.twitter.com/1/users/show.json?screen_name="
							+ username + "&include_entities=true");
			// Execute the request in the client
			HttpResponse httpResponse = defaultClient.execute(httpGetRequest);
			// Grab the response
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					httpResponse.getEntity().getContent(), "UTF-8"));
			String json = reader.readLine();
			// Instantiate a JSON Object from the request response
			JSONObject oneObject = new JSONObject(json);
			// Retrieve USER data and put into USER object
			myuser = new User(oneObject.getString("name"),
					oneObject.getString("screen_name"),
					oneObject.getString("location"),
					oneObject.getString("description"),
					oneObject.getString("profile_image_url"));
			// LOGCAT TEST
			System.out.println("LOOK HEEERRRREEE!!!!!!");
			System.out.println(myuser.toString());

		} catch (Exception e) {
			e.printStackTrace();
		}
		return myuser;
	}

	public class TweetAdapter extends ArrayAdapter<Tweet> {

		private ArrayList<Tweet> tweets;
		private AsyncImageLoader asyncImageLoader;

		public TweetAdapter(Context context, int textViewResourceId,
				ArrayList<Tweet> tweets) {
			super(context, textViewResourceId, tweets);
			this.tweets = tweets;
			asyncImageLoader = new AsyncImageLoader();

		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			View v = convertView;

			// inflate view from XML
			if (v == null) {
				LayoutInflater vi = (LayoutInflater) getContext()
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				v = vi.inflate(R.layout.timeline2, null);
			}

			Tweet tweet = tweets.get(position);

			if (tweet != null) {
				TextView username = (TextView) v.findViewById(R.id.username);
				TextView message = (TextView) v.findViewById(R.id.message);
				final ImageView imageView = (ImageView) v
						.findViewById(R.id.avatar);

				if (username != null) {
					username.setText("@" + tweet.username);
				}
				if (message != null) {
					message.setText(tweet.message);
				}
				if (imageView != null) {
					Drawable cachedImage = asyncImageLoader.loadDrawable(
							tweet.image_url, new ImageCallback() {
								public void imageLoaded(Drawable imageDrawable,
										String imageUrl) {
									imageView.setImageDrawable(imageDrawable);
								}
							});
					imageView.setImageDrawable(cachedImage);
				}
			}

			return v;

		}

		public class AsyncImageLoader {

			private HashMap<String, SoftReference<Drawable>> imageCache;

			public AsyncImageLoader() {
				imageCache = new HashMap<String, SoftReference<Drawable>>();
			}

			public Drawable loadDrawable(final String imageUrl,
					final ImageCallback imageCallback) {
				if (imageCache.containsKey(imageUrl)) {
					SoftReference<Drawable> softReference = imageCache
							.get(imageUrl);
					Drawable drawable = softReference.get();
					if (drawable != null) {
						return drawable;
					}
				}
				final Handler handler = new Handler() {
					@Override
					public void handleMessage(Message message) {
						imageCallback.imageLoaded((Drawable) message.obj,
								imageUrl);
					}
				};
				new Thread() {
					@Override
					public void run() {
						Drawable drawable = loadImageFromUrl(imageUrl);
						imageCache.put(imageUrl, new SoftReference<Drawable>(
								drawable));
						Message message = handler.obtainMessage(0, drawable);
						handler.sendMessage(message);
					}
				}.start();
				return null;
			}

			public Drawable loadImageFromUrl(String url) {
				InputStream inputStream;
				try {
					inputStream = new URL(url).openStream();
				} catch (IOException e) {
					throw new RuntimeException(e);
				}
				return Drawable.createFromStream(inputStream, "src");
			}

			// public interface ImageCallback {
			// public void imageLoaded(Drawable imageDrawable, String imageUrl);
			// }

		}

		public class ImageCallback {
			public void imageLoaded(Drawable imageDrawable, String imageUrl) {
			}
		}

	}

	public class Tweet {

		public String username;
		public String message;
		public String image_url;

		public Tweet(String message, String username, String image_url) {
			this.username = username;
			this.message = message;
			this.image_url = image_url;
		}

		@Override
		public String toString() {
			return getClass().getName() + "[" + "username: " + username + ", "
					+ "message: " + message + ", " + "image url: " + image_url
					+ "]";
		}

	}

	public class User {

		public String name;
		public String screen_name;
		public String location;
		public String description;
		public String profile_image_url;

		public User(String name, String screen_name, String location,
				String description, String profile_image_url) {
			this.name = name;
			this.screen_name = screen_name;
			this.location = location;
			this.description = description;
			this.profile_image_url = profile_image_url;
		}

		@Override
		public String toString() {
			return getClass().getName() + "[" + "name: " + name + ", "
					+ "screen name: " + screen_name + ", " + "location: "
					+ location + ", " + "description: " + description + ", "
					+ "profile image: " + profile_image_url + "]";
		}
	}
}