package com.example.BaseBallSNS;

import java.util.*;
import java.util.zip.Inflater;

import com.example.BaseBallSNS.R;
import com.google.android.maps.*;

import android.app.*;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import android.location.*;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.*;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.*;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.graphics.drawable.Drawable;

import com.google.android.maps.*;
import com.google.android.maps.MapView.LayoutParams;

/**
 * @author LeeJungsu
 * @param kiaSelect
 *            : boolean Type.
 */

public class heroesLayout extends MapActivity implements OnClickListener {
	View heroesTimeLine2, heroesmenu2, heroesmenu3;
	ListView heroeslistview;
	MapView heroesmapView; // //맵뷰 객체
	MapController heroesmc;
	GeoPoint heroespoint, heroespoint2, heroespoint3, heroespoint4, heroespoint5,
			heroespoint6, heroespoint7, heroespoint8, heroespoint9, heroespoint10,
			heroespoint11, heroespoint12, heroespoint13, heroespoint14,
			heroespoint15, heroespoint16, heroespoint17;
	List<Overlay> heroeslist; // 맵 오버레이 포함하는 리스트
	EditText heroescomment;
	ArrayList<MyItem> heroesarItem;
	private ArrayList<HashMap<String, String>> heroesdata;
	private Button heroesbt_open;
	private ListView heroeslistview2;
	private SimpleAdapter heroessa;
	private Net_HTMLParse heroeshp;
	public static HashMap<String, String> heroesmap = new HashMap<String, String>();
	private final Handler heroeshandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			listUpdate();
		}
	};

	public static boolean heroesSelect = true;

	@Override
	/**
	 * onCreate()
	 * 화면 생성 함수, void type
	 * @author Lee Jung Su
	 * @param Bundle savedInstanceState
	 */
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.heroes);

		// top = findViewById(R.id.top);
		// top.setVisibility(View.INVISIBLE);
		heroesmapView = (MapView) findViewById(R.id.heroesmapview);
		heroesmapView.setVisibility(View.INVISIBLE);
		// TimeLine2 = findViewById(R.id.list);
		// TimeLine2.setVisibility(View.INVISIBLE);
		heroesmenu2 = findViewById(R.id.heroesmenu2);
		heroesmenu2.setVisibility(View.INVISIBLE);
		heroesmenu3 = findViewById(R.id.heroesmenu3);
		heroesmenu3.setVisibility(View.INVISIBLE);
		heroeslistview = (ListView) findViewById(R.id.heroeslist);
		heroeslistview.setVisibility(View.INVISIBLE);
		heroeslistview2 = (ListView) findViewById(R.id.heroeslist2);
		heroeslistview2.setVisibility(View.VISIBLE);

		// hp.open();

		heroesdata = new ArrayList<HashMap<String, String>>();

		// bt_open = (Button)findViewById(R.id.bt_open);

		heroeshp = new Net_HTMLParse(this, heroeshandler, heroesdata);

		heroessa = new SimpleAdapter(this, heroesdata, R.layout.homeview, new String[] {
				"date", "team1", "score","team2","time","where" }, new int[] { R.id.date, R.id.team1,
				R.id.score, R.id.team2, R.id.time, R.id.where });

		// listview2.setOnClickListener(listener);
		heroeslistview2.setAdapter(heroessa);
		heroeshp.open(385);
		/* 홈버튼 */
		ImageButton heroesbutton1 = (ImageButton) findViewById(R.id.heroesbutton1);
		heroesbutton1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				heroeslistview.setVisibility(View.INVISIBLE);
				heroesmapView.setVisibility(View.INVISIBLE);
				// top.setVisibility(View.INVISIBLE);
				heroesmenu2.setVisibility(View.INVISIBLE);
				heroesmenu3.setVisibility(View.INVISIBLE);

				heroeshp.open(385);

				heroeslistview2.setVisibility(View.VISIBLE);
				// startActivity(myIntent);
			}
		});
//		TeamAudio Kiaaudio = new TeamAudio();
//		heroesarItem = new ArrayList<MyItem>();
//		MyItem mi;
//		mi = new MyItem(R.drawable.heroeskim, "김진욱", "감독", "1960년 08월05일",
//				"182cm / 87kg",heroesmap.get("선동열"),"가사가사가사");
//		heroesarItem.add(mi);
//		mi = new MyItem(R.drawable.heroesplayer1, "정재훈", "투수", "1980년 01월01일",
//				"179cm / 81kg",heroesmap.get("선동열"),"가사가사가사");
//		heroesarItem.add(mi);
//		mi = new MyItem(R.drawable.heroesplayer2, "김선우", "투수", "1977년 09월04일",
//				"184cm / 87kg",heroesmap.get("선동열"),"가사가사가사");
//		heroesarItem.add(mi);
//		mi = new MyItem(R.drawable.heroesplayer3, "김상현", "투수", "1980년04월07일",
//				"181cm / 82kg",heroesmap.get("선동열"),"가사가사가사");
//		heroesarItem.add(mi);
//		mi = new MyItem(R.drawable.heroesplayer4, "김강률", "투수", "1988년 08월28일",
//				"187cm / 95kg",heroesmap.get("선동열"),"가사가사가사");
//		heroesarItem.add(mi);
//		mi = new MyItem(R.drawable.heroesplayer5, "올슨", "투수", "1983년 10월18일",
//				"185cm / 93kg",heroesmap.get("선동열"),"가사가사가사");
//		heroesarItem.add(mi);
//		mi = new MyItem(R.drawable.heroesplayer6, "임태훈", "투수", " 1988년 09월28일",
//				"182cm / 83kg",heroesmap.get("선동열"),"가사가사가사");
//		heroesarItem.add(mi);
//		mi = new MyItem(R.drawable.heroesplayer7, "이재우", "투수", " 1980년 02월09일",
//				"183cm / 82kg",heroesmap.get("선동열"),"가사가사가사");
//		heroesarItem.add(mi);
//		mi = new MyItem(R.drawable.heroesplayer8, "니퍼드", "투수", "1981년 05월06일",
//				"203cm / 103kg",heroesmap.get("선동열"),"가사가사가사");
//		heroesarItem.add(mi);
//		mi = new MyItem(R.drawable.heroesplayer9, "오현택", "투수", "1985년 07월17일",
//				"180cm / 73kg",heroesmap.get("선동열"),"가사가사가사");
//		heroesarItem.add(mi);
//		mi = new MyItem(R.drawable.heroesplayer10, "양의지	", "포수", "1987년 06월05일",
//				"179cm / 85kg",heroesmap.get("선동열"),"가사가사가사");
//		heroesarItem.add(mi);
//		mi = new MyItem(R.drawable.heroesplayer11, "최재훈", "포수",
//				"1989년 08월 27일", "178cm / 76kg",heroesmap.get("선동열"),"가사가사가사");
//		heroesarItem.add(mi);
//		mi = new MyItem(R.drawable.heroesplayer12, "최준석", "내야수",
//				"1983년 02월15일", "185cm / 115kg",heroesmap.get("선동열"),"가사가사가사");
//		heroesarItem.add(mi);
//		mi = new MyItem(R.drawable.heroesplayer13, "김재호", "내야수",
//				"1985년 03월21일", "181cm / 75kg",heroesmap.get("선동열"),"가사가사가사");
//		heroesarItem.add(mi);
//		mi = new MyItem(R.drawable.heroesplayer14, "홍성흔", "내야수",
//				"1977년 02월28일", "180cm / 96kg",heroesmap.get("선동열"),"가사가사가사");
//		heroesarItem.add(mi);
//		mi = new MyItem(R.drawable.heroesplayer15, "최주환", "내야수",
//				"1988년 02월28일", "178cm / 73kg",heroesmap.get("선동열"),"가사가사가사");
//		heroesarItem.add(mi);
//		mi = new MyItem(R.drawable.heroesplayer16, "허경민", "내야수",
//				"1990년 08월26일", "176cm / 69kg",heroesmap.get("선동열"),"가사가사가사");
//		heroesarItem.add(mi);
//		mi = new MyItem(R.drawable.heroesplayer17, "윤석민", "내야수",
//				"1985년 09월04일", "180cm / 86kg",heroesmap.get("선동열"),"가사가사가사");
//		heroesarItem.add(mi);
//		mi = new MyItem(R.drawable.heroesplayer18, "오재원", "내야수",
//				"1985년 02월09일", "185cm / 75kg",heroesmap.get("선동열"),"가사가사가사");
//		heroesarItem.add(mi);
//		mi = new MyItem(R.drawable.heroesplayer19, "박건우", "외야수",
//				"1990년 09월08일", "184cm / 80kg",heroesmap.get("선동열"),"가사가사가사");
//		heroesarItem.add(mi);
//		mi = new MyItem(R.drawable.heroesplayer20, "민병헌", "외야수",
//				"1987년 03월10일", "178cm / 78kg",heroesmap.get("선동열"),"가사가사가사");
//		heroesarItem.add(mi);
//		mi = new MyItem(R.drawable.heroesplayer21, "김현수", "외야수",
//				"1988년 01월12일", "188cm / 100kg",heroesmap.get("선동열"),"가사가사가사");
//		heroesarItem.add(mi);
//		mi = new MyItem(R.drawable.heroesplayer22, "이종욱", "외야수",
//				"1980년 06월18일", "176cm / 77kg",heroesmap.get("선동열"),"가사가사가사");
//		heroesarItem.add(mi);
//		mi = new MyItem(R.drawable.heroesplayer23, "정수빈", "외야수",
//				"1990년 10월07일", "175cm / 70kg",heroesmap.get("선동열"),"가사가사가사");
//		heroesarItem.add(mi);
//		MyListAdapter MyAdapter = new MyListAdapter(this, R.layout.kiaplayer,
//				heroesarItem);
//
//		ListView heroesMyList;
//		heroesMyList = (ListView) findViewById(R.id.heroeslist);
//		heroesMyList.setAdapter(MyAdapter);

		/** 선수정보 버튼 */
		ImageButton heroesbutton2 = (ImageButton) findViewById(R.id.heroesbutton2);
		heroesbutton2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				heroeslistview.setVisibility(View.VISIBLE);
				heroesmapView.setVisibility(View.INVISIBLE);
				// top.setVisibility(View.INVISIBLE);
				heroesmenu2.setVisibility(View.INVISIBLE);
				heroesmenu3.setVisibility(View.INVISIBLE);

				// startActivity(myIntent);
			}
		});

		/** 트위터 버튼 */
		ImageButton heroesbutton3 = (ImageButton) findViewById(R.id.heroesbutton3);
		heroesbutton3.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				heroesmenu2.setVisibility(View.VISIBLE);
				heroesmenu3.setVisibility(View.VISIBLE);
				heroesmapView.setVisibility(View.INVISIBLE);
				heroeslistview.setVisibility(View.INVISIBLE);
				heroeslistview2.setVisibility(View.INVISIBLE);
				// TimeLine2.setVisibility(View.INVISIBLE);

			}
		});
		/** 트위터 로그인버튼 */
		ImageButton button = (ImageButton) findViewById(R.id.heroeslogin);
		button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent myIntent = new Intent(heroesLayout.this,
						TwitterActivity.class);
				startActivityForResult(myIntent, 1004);
				// startActivity(myIntent);
			}
		});
		/** 트위터 글쓰기버튼 */
		final HashTag hash = new HashTag("");
		ImageButton heroesButton1 = (ImageButton) findViewById(R.id.heroestweet);
		heroescomment = (EditText) findViewById(R.id.heroescomment);
		heroesButton1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				String commentString = heroescomment.getText().toString();
				if (STATICVALUES.token != null
						&& STATICVALUES.tokenSecret != null) {
					RegistAsyncTask registAsyncTask = new RegistAsyncTask(
							heroesLayout.this);
					registAsyncTask.execute(STATICVALUES.token,
							STATICVALUES.tokenSecret, commentString);
				} else {
					Toast.makeText(heroesLayout.this, "로그인해주세요!",
							Toast.LENGTH_SHORT).show();
				}
				heroescomment.setText("");

			}
		});
		CheckBox heroesRbutton1 = (CheckBox) findViewById(R.id.heroescheckBox1);
		heroesRbutton1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				ImageButton heroesButton1 = (ImageButton) findViewById(R.id.heroestweet);
				heroesButton1.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						hash.ResultTag = "#기아타이거즈";
						String commentString = hash.MakeHash("#기아타이거즈") + " "
								+ heroescomment.getText().toString();
						if (STATICVALUES.token != null
								&& STATICVALUES.tokenSecret != null) {
							RegistAsyncTask registAsyncTask = new RegistAsyncTask(
									heroesLayout.this);
							registAsyncTask.execute(STATICVALUES.token,
									STATICVALUES.tokenSecret, commentString);
						} else {
							Toast.makeText(heroesLayout.this, "로그인해주세요!",
									Toast.LENGTH_SHORT).show();
						}
						heroescomment.setText("");
					}
				});
			}
		});
		CheckBox heroesRbutton2 = (CheckBox) findViewById(R.id.heroescheckBox2);
		heroesRbutton2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				ImageButton heroesButton1 = (ImageButton) findViewById(R.id.heroestweet);
				heroesButton1.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						hash.ResultTag = "#기아타이거즈2";
						String commentString = hash.ResultTag + " "
								+ heroescomment.getText().toString();
						if (STATICVALUES.token != null
								&& STATICVALUES.tokenSecret != null) {
							RegistAsyncTask registAsyncTask = new RegistAsyncTask(
									heroesLayout.this);
							registAsyncTask.execute(STATICVALUES.token,
									STATICVALUES.tokenSecret, commentString);
						} else {
							Toast.makeText(heroesLayout.this, "로그인해주세요",
									Toast.LENGTH_SHORT).show();
						}
						heroescomment.setText("");
					}
				});
			}
		});
		ImageButton heroesTbutton = (ImageButton) findViewById(R.id.heroestweet);
		heroesTbutton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				heroesmenu2.setVisibility(View.VISIBLE);
				heroesmenu3.setVisibility(View.VISIBLE);

			}
		});

		ImageButton heroesTimebutton = (ImageButton) findViewById(R.id.heroestimeline);
		heroesTimebutton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				//heroesmenu2.setVisibility(View.VISIBLE);
				// menu3.setVisibility(View.VISIBLE);
				timeline.searchTerm="#두산베어스";
				Intent intent = new Intent(heroesLayout.this, timeline.class);
				startActivity(intent);
				// top.setVisibility(View.INVISIBLE);
				// menu2.setVisibility(View.VISIBLE);
				//heroesmapView.setVisibility(View.INVISIBLE);
				// TimeLine2.setVisibility(View.VISIBLE);
				//heroeslistview.setVisibility(View.INVISIBLE);

			}
		});

		/** 구글 맵 버튼 */
		ImageButton heroesMbutton4 = (ImageButton) findViewById(R.id.heroesbutton4);
		heroesMbutton4.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// top.setVisibility(View.INVISIBLE);
				heroesmenu2.setVisibility(View.INVISIBLE);
				heroesmenu3.setVisibility(View.INVISIBLE);
				heroesmapView.setVisibility(View.VISIBLE);
				heroesmapView.setBuiltInZoomControls(true);// zoom controller
				heroesmc = heroesmapView.getController();

				if (heroesLayout.heroesSelect == true) {
					heroespoint = new GeoPoint(35193819, 129061471);// 기아 경기장 좌표
																	// 설정
					heroesmc.animateTo(heroespoint);// 좌표점 이동
					heroesmc.setZoom(17);// 확대 1~21

					heroeslist = heroesmapView.getOverlays();
					// 위치에 이미지 설정한다.
					Drawable drawable = getResources().getDrawable(
							R.drawable.mapheroes);
					OverlayItem item = new OverlayItem(heroespoint, "부산사직야구장",
							"롯데자이언츠");
					MapOverlay map = new MapOverlay(drawable,heroesLayout.this);
					map.addItem(item);
					heroeslist.add(map);

					heroespoint2 = new GeoPoint(35196074, 129059442);// 기아 경기장 좌표
																	// 설정
					Drawable drawable2 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item2 = new OverlayItem(heroespoint2, "맛집",
							"주문진막국수");
					MapOverlay map2 = new MapOverlay(drawable2,heroesLayout.this);
					map2.addItem(item2);
					heroeslist.add(map2);

					heroespoint3 = new GeoPoint(35195541, 129058750);// 기아 경기장 좌표
																	// 설정
					Drawable drawable3 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item3 = new OverlayItem(heroespoint3, "맛집",
							"안양해물탕");
					MapOverlay map3 = new MapOverlay(drawable3,heroesLayout.this);
					map3.addItem(item3);
					heroeslist.add(map3);

					heroespoint4 = new GeoPoint(35198551, 129064490);// 기아 경기장 좌표
																	// 설정
					Drawable drawable4 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item4 = new OverlayItem(heroespoint4, "맛집",
							"금강만두");
					MapOverlay map4 = new MapOverlay(drawable4,heroesLayout.this);
					map4.addItem(item4);
					heroeslist.add(map4);

					heroespoint5 = new GeoPoint(35193521,129062002);
					Drawable drawable5 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item5 = new OverlayItem(heroespoint5, "맛집",
							"롯데리아");
					MapOverlay map5 = new MapOverlay(drawable5,heroesLayout.this);
					map5.addItem(item5);
					heroeslist.add(map5);

					heroespoint6 = new GeoPoint(35193706,129064776);
					Drawable drawable6 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item6 = new OverlayItem(heroespoint6, "맛집",
							"서영식당");
					MapOverlay map6 = new MapOverlay(drawable6,heroesLayout.this);
					map6.addItem(item6);
					heroeslist.add(map6);

					heroespoint7 = new GeoPoint(35195506,129064009);
					Drawable drawable7 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item7 = new OverlayItem(heroespoint7, "맛집",
							"마포진연탄구이");
					MapOverlay map7 = new MapOverlay(drawable7,heroesLayout.this);
					map7.addItem(item7);
					heroeslist.add(map7);

					heroespoint8 = new GeoPoint(35195726,129063939);
					Drawable drawable8 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item8 = new OverlayItem(heroespoint8, "맛집",
							"비비큐치킨");
					MapOverlay map8 = new MapOverlay(drawable8,heroesLayout.this);
					map8.addItem(item8);
					heroeslist.add(map8);

					heroespoint9 = new GeoPoint(35195423,129065452);
					Drawable drawable9 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item9 = new OverlayItem(heroespoint9, "맛집",
							"류가네더덕삼겹살");
					MapOverlay map9 = new MapOverlay(drawable9,heroesLayout.this);
					map9.addItem(item9);
					heroeslist.add(map9);

					heroespoint10 = new GeoPoint(35195528,129064164);
					Drawable drawable10 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item10 = new OverlayItem(heroespoint10, "맛집",
							"조방낙지");
					MapOverlay map10 = new MapOverlay(drawable10,heroesLayout.this);
					map10.addItem(item10);
					heroeslist.add(map10);

					heroespoint11 = new GeoPoint(35195436,129064577);
					Drawable drawable11 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item11 = new OverlayItem(heroespoint11, "맛집",
							"서원숯불바베큐");
					MapOverlay map11 = new MapOverlay(drawable11,heroesLayout.this);
					map11.addItem(item11);
					heroeslist.add(map11);

					heroespoint12 = new GeoPoint(35193871,129064577);
					Drawable drawable12 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item12 = new OverlayItem(heroespoint12, "맛집",
							"대구막창왕갈비");
					MapOverlay map12 = new MapOverlay(drawable12,heroesLayout.this);
					map12.addItem(item12);
					heroeslist.add(map12);

					heroespoint13 = new GeoPoint(35193538,129064583);
					Drawable drawable13 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item13 = new OverlayItem(heroespoint13, "맛집",
							"진우식당");
					MapOverlay map13 = new MapOverlay(drawable13,heroesLayout.this);
					map13.addItem(item13);
					heroeslist.add(map13);

					heroespoint14 = new GeoPoint(35195940,129060618);
					Drawable drawable14 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item14 = new OverlayItem(heroespoint14, "맛집",
							"맥도날드");
					MapOverlay map14 = new MapOverlay(drawable14,heroesLayout.this);
					map14.addItem(item14);
					heroeslist.add(map14);

					heroespoint15 = new GeoPoint(35196300,129060640);
					Drawable drawable15 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item15 = new OverlayItem(heroespoint15, "맛집",
							"미스터피자");
					MapOverlay map15 = new MapOverlay(drawable15,heroesLayout.this);
					map15.addItem(item15);
					heroeslist.add(map15);

					heroespoint16 = new GeoPoint(35195971,129059567);
					Drawable drawable16 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item16 = new OverlayItem(heroespoint16, "맛집",
							"주문진식당");
					MapOverlay map16 = new MapOverlay(drawable16,heroesLayout.this);
					map16.addItem(item16);
					heroeslist.add(map16);



				}

				// startLocationService();

			}

		});
		/** 홈으로 가기 버튼 */
		ImageButton heroesHbutton5 = (ImageButton) findViewById(R.id.heroesbutton5);
		heroesHbutton5.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent myIntent3 = new Intent(heroesLayout.this,
						MainActivity.class);
				myIntent3.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				myIntent3.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
				startActivity(myIntent3);
				finish();
			}
		});
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

	}

	private LocalActivityManager getLocalActivityManager() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected boolean isRouteDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}

	private void startLocationService() {
		LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		Location location = manager
				.getLastKnownLocation(LocationManager.GPS_PROVIDER);

		GPSListener gpsListener = new GPSListener();
		long minTime = 10000;
		float minDistance = 0;

		manager.requestLocationUpdates(LocationManager.GPS_PROVIDER, minTime,
				minDistance, gpsListener);

	}

	private class GPSListener implements LocationListener {

		public void onLocationChanged(Location location) {
			Double latitude = location.getLatitude();
			Double longitude = location.getLongitude();

			showCurrentLocation(latitude, longitude);
		}

		public void onProviderDisabled(String provider) {
		}

		public void onProviderEnabled(String provider) {
		}

		public void onStatusChanged(String provider, int status, Bundle extras) {
		}
	}

	/**
	 * void showCurrentLocation
	 * 
	 * @param latitude
	 *            : Double type
	 * @param longitude
	 *            : Double type
	 */

	private void showCurrentLocation(Double latitude, Double longitude) {
		double intLatitude = latitude.doubleValue() * 1000000;
		double inLongitude = longitude.doubleValue() * 1000000;

		GeoPoint geoPt = new GeoPoint((int) intLatitude, (int) inLongitude);

		MapController controller = heroesmapView.getController();
		controller.animateTo(geoPt);

		int maxZoomlevel = heroesmapView.getMaxZoomLevel();
		int zoomLevel = (int) ((maxZoomlevel + 1) / 1.15);

		controller.setZoom(zoomLevel);
		controller.setCenter(geoPt);
		heroesmapView.setSatellite(false);
		heroesmapView.setTraffic(false);

	}

	/**
	 * void onActivityResult save result of twitter activity
	 * 
	 * @param requestCode
	 *            : int type
	 * @param resultCode
	 *            : int type
	 * @param data
	 *            : Intent type
	 */
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		Log.e("DAY", "Resultcode : " + resultCode);

		if (resultCode == RESULT_OK) {

			STATICVALUES.token = data.getStringExtra("token");
			STATICVALUES.tokenSecret = data.getStringExtra("tokenSecret");
			STATICVALUES.nick = data.getStringExtra("nick");

			Toast.makeText(this, STATICVALUES.nick + "로그인 되었습니다.!",
					Toast.LENGTH_SHORT).show();

			Log.e("DAY", STATICVALUES.token);
			Log.e("DAY", STATICVALUES.tokenSecret);
			Log.e("DAY", STATICVALUES.nick);

		}

	}

	// 버튼 리스너
	OnClickListener listener = new OnClickListener() {

		@Override
		public void onClick(View arg0) {
			heroeshp.open(385);
		}

	};

	// 업데이트하기
	private void listUpdate() {
		heroessa.notifyDataSetChanged();
	}

}
