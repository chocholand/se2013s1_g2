package com.example.BaseBallSNS;

import java.util.*;
import java.util.zip.Inflater;

import com.example.BaseBallSNS.R;
import com.google.android.maps.*;

import android.app.*;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import android.location.*;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.*;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.*;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.graphics.drawable.Drawable;
import com.google.android.maps.*;
import com.google.android.maps.MapView.LayoutParams;


/**
 * @author LeeJungsu
 * @param kiaSelect
 *            : boolean Type.
 */

public class SKLayout extends MapActivity implements OnClickListener {
	View top, TimeLine2, menu2, menu3;
	ListView listview;
	MapView SKmapView; // //맵뷰 객체
	MapController mc;
	GeoPoint SKpoint, SKpoint2, SKpoint3, SKpoint4,
			SKpoint5, SKpoint6, SKpoint7, SKpoint8,
			SKpoint9, SKpoint10, SKpoint11, SKpoint12,
			SKpoint13, SKpoint14, SKpoint15, SKpoint16,
			SKpoint17, SKpoint18, SKpoint19, SKpoint20,
			SKpoint21, SKpoint22;
	List<Overlay> SKlist; // 맵 오버레이 포함하는 리스트
	EditText comment;
	ArrayList<MyItem> arItem;
	private ArrayList<HashMap<String, String>> data;
	private Button bt_open;
	private ListView listview2;
	private SimpleAdapter sa;
	private Net_HTMLParse hp;
	public static HashMap<String, String> skmap = new HashMap<String, String>();
	private final Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			listUpdate();
		}
	};

	public static boolean SKSelect = true;

	@Override
	/**
	 * onCreate()
	 * 화면 생성 함수, void type
	 * @author Lee Jung Su
	 * @param Bundle savedInstanceState
	 */
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sk);

		// top = findViewById(R.id.top);
		// top.setVisibility(View.INVISIBLE);
		SKmapView = (MapView) findViewById(R.id.skmapview);
		SKmapView.setVisibility(View.INVISIBLE);
		// TimeLine2 = findViewById(R.id.list);
		// TimeLine2.setVisibility(View.INVISIBLE);
		menu2 = findViewById(R.id.skmenu2);
		menu2.setVisibility(View.INVISIBLE);
		menu3 = findViewById(R.id.skmenu3);
		menu3.setVisibility(View.INVISIBLE);
		listview = (ListView) findViewById(R.id.sklist);
		listview.setVisibility(View.INVISIBLE);
		listview2 = (ListView) findViewById(R.id.sklist2);
		listview2.setVisibility(View.VISIBLE);

		// hp.open();

		data = new ArrayList<HashMap<String, String>>();

		// bt_open = (Button)findViewById(R.id.bt_open);

		hp = new Net_HTMLParse(this, handler, data);

		sa = new SimpleAdapter(this, data, R.layout.homeview, new String[] {
				"date", "team1", "score","team2","time","where" }, new int[] { R.id.date, R.id.team1,
				R.id.score, R.id.team2, R.id.time, R.id.where });

		// listview2.setOnClickListener(listener);
		listview2.setAdapter(sa);
		hp.open(384);
		/* 홈버튼 */
		ImageButton ssskbutton1 = (ImageButton) findViewById(R.id.skbutton1);
		ssskbutton1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				listview.setVisibility(View.INVISIBLE);
				SKmapView.setVisibility(View.INVISIBLE);
				// top.setVisibility(View.INVISIBLE);
				menu2.setVisibility(View.INVISIBLE);
				menu3.setVisibility(View.INVISIBLE);

				hp.open(384);

				listview2.setVisibility(View.VISIBLE);
				// startActivity(myIntent);
			}
		});
		TeamAudio Kiaaudio = new TeamAudio();
		arItem = new ArrayList<MyItem>();
		MyItem mi;
		mi = new MyItem(R.drawable.sklee, "류중일", "감독", "1963년 04월28일",
				"176cm / 70kg",skmap.get("이승엽"),"가사가사가사");
		arItem.add(mi);
		mi = new MyItem(R.drawable.skplayer1, "김광현", "투수", "1981년 03월26일",
				"182cm / 82kg",skmap.get("이승엽"),"가사가사가사");
		arItem.add(mi);
		mi = new MyItem(R.drawable.skplayer2, "레이예스", "투수",
				"1987년 12월13일", "183cm / 91kg",skmap.get("이승엽"),"가사가사가사");
		arItem.add(mi);
		mi = new MyItem(R.drawable.skplayer3, "문승원", "투수", "1981년 05월04일",
				"184cm / 84kg",skmap.get("이승엽"),"가사가사가사");
		arItem.add(mi);
		mi = new MyItem(R.drawable.skplayer4, "박희수", "투수", "1987년 07월13일",
				"184cm / 80kg",skmap.get("이승엽"),"가사가사가사");
		arItem.add(mi);
		mi = new MyItem(R.drawable.skplayer5, "백인식", "투수",
				"1985년 05월 22일", "196cm / 98kg",skmap.get("이승엽"),"가사가사가사");
		arItem.add(mi);
		mi = new MyItem(R.drawable.skplayer6, "세든", "투수", "1983년 10월 1일",
				"180cm / 83kg",skmap.get("이승엽"),"가사가사가사");
		arItem.add(mi);
		mi = new MyItem(R.drawable.skplayer7, "윤희상", "투수",
				"1982년 07월 15일", "178cm / 91kg",skmap.get("이승엽"),"가사가사가사");
		arItem.add(mi);
		mi = new MyItem(R.drawable.skplayer8, "이재영", "투수", "1981년 10월 8일",
				"193cm / 88kg",skmap.get("이승엽"),"가사가사가사");
		arItem.add(mi);
		mi = new MyItem(R.drawable.skplayer9, "이한진", "투수", "1983년 06월 9일",
				"181cm / 81kg",skmap.get("이승엽"),"가사가사가사");
		arItem.add(mi);
		mi = new MyItem(R.drawable.skplayer10, "박경완", "포수",
				"1974년 05월 08일", "182cm / 90kg",skmap.get("이승엽"),"가사가사가사");
		arItem.add(mi);
		mi = new MyItem(R.drawable.skplayer11, "조인성", "포수",
				"1974년 05월 08일", "182cm / 90kg",skmap.get("이승엽"),"가사가사가사");
		arItem.add(mi);
		mi = new MyItem(R.drawable.skplayer12, "김성현", "내야수",
				"1980년 10월 25일", "181cm / 73kg",skmap.get("이승엽"),"가사가사가사");
		arItem.add(mi);
		mi = new MyItem(R.drawable.skplayer13, "박승욱", "내야수",
				"1990년 03월 23일", "177cm / 68kg",skmap.get("이승엽"),"가사가사가사");
		arItem.add(mi);
		mi = new MyItem(R.drawable.skplayer14, "박정권", "내야수",
				"1981년 09월 19일", "174cm / 81kg",skmap.get("이승엽"),"가사가사가사");
		arItem.add(mi);
		mi = new MyItem(R.drawable.skplayer15, "박진만", "내야수",
				"1985년 06월 22일", "178cm / 98kg",skmap.get("이승엽"),"가사가사가사");
		arItem.add(mi);
		mi = new MyItem(R.drawable.skplayer16, "정근우", "내야수",
				"1978년 08월 06일", "181cm / 80kg",skmap.get("이승엽"),"가사가사가사");
		arItem.add(mi);
		mi = new MyItem(R.drawable.skplayer17, "조성우", "내야수",
				"1976년 08월 18일", "183cm / 93kg",skmap.get("이승엽"),"가사가사가사");
		arItem.add(mi);
		mi = new MyItem(R.drawable.skplayer18, "최정", "내야수",
				"1983년 07월 27일", "185cm / 88kg",skmap.get("이승엽"),"가사가사가사");
		arItem.add(mi);
		mi = new MyItem(R.drawable.skplayer19, "김강민", "외야수",
				"1982년 10월 11일", "188cm / 100kg",skmap.get("이승엽"),"가사가사가사");
		arItem.add(mi);
		mi = new MyItem(R.drawable.skplayer20, "깅상현", "외야수",
				"1983년 12월 16일", "179cm / 86kg",skmap.get("이승엽"),"가사가사가사");
		arItem.add(mi);
		mi = new MyItem(R.drawable.skplayer21, "박재상", "외야수",
				"1986년 06월 27일", "179cm / 86kg",skmap.get("이승엽"),"가사가사가사");
		arItem.add(mi);
		mi = new MyItem(R.drawable.skplayer22, "조동화", "외야수",
				"1991년 01월 28일", "179cm / 86kg",skmap.get("이승엽"),"가사가사가사");

		MyListAdapter MyAdapter = new MyListAdapter(this, R.layout.kiaplayer,
				arItem);

		ListView MyList;
		MyList = (ListView) findViewById(R.id.sklist);
		MyList.setAdapter(MyAdapter);

		/** 선수정보 버튼 */
		ImageButton ssskbutton2 = (ImageButton) findViewById(R.id.skbutton2);
		ssskbutton2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				listview.setVisibility(View.VISIBLE);
				SKmapView.setVisibility(View.INVISIBLE);
				// top.setVisibility(View.INVISIBLE);
				menu2.setVisibility(View.INVISIBLE);
				menu3.setVisibility(View.INVISIBLE);

				// startActivity(myIntent);
			}
		});

		/** 트위터 버튼 */
		ImageButton ssskbutton3 = (ImageButton) findViewById(R.id.skbutton3);
		ssskbutton3.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				menu2.setVisibility(View.VISIBLE);
				menu3.setVisibility(View.VISIBLE);
				SKmapView.setVisibility(View.INVISIBLE);
				listview.setVisibility(View.INVISIBLE);
				listview2.setVisibility(View.INVISIBLE);
				// TimeLine2.setVisibility(View.INVISIBLE);

			}
		});
		/** 트위터 로그인버튼 */
		ImageButton button = (ImageButton) findViewById(R.id.sktlogin);
		button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent myIntent = new Intent(SKLayout.this,
						TwitterActivity.class);
				startActivityForResult(myIntent, 1004);
				// startActivity(myIntent);
			}
		});
		/** 트위터 글쓰기버튼 */
		final HashTag hash = new HashTag("");
		ImageButton button1 = (ImageButton) findViewById(R.id.sktweet);
		comment = (EditText) findViewById(R.id.scomment);
		button1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				String commentString = comment.getText().toString();
				if (STATICVALUES.token != null
						&& STATICVALUES.tokenSecret != null) {
					RegistAsyncTask registAsyncTask = new RegistAsyncTask(
							SKLayout.this);
					registAsyncTask.execute(STATICVALUES.token,
							STATICVALUES.tokenSecret, commentString);
				} else {
					Toast.makeText(SKLayout.this, "로그인해주세요!",
							Toast.LENGTH_SHORT).show();
				}
				comment.setText("");

			}
		});
		CheckBox Rbutton1 = (CheckBox) findViewById(R.id.skcheckBox1);
		Rbutton1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				ImageButton button1 = (ImageButton) findViewById(R.id.sktweet);
				button1.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						hash.ResultTag = "#기아타이거즈";
						String commentString = hash.MakeHash("#기아타이거즈") + " "
								+ comment.getText().toString();
						if (STATICVALUES.token != null
								&& STATICVALUES.tokenSecret != null) {
							RegistAsyncTask registAsyncTask = new RegistAsyncTask(
									SKLayout.this);
							registAsyncTask.execute(STATICVALUES.token,
									STATICVALUES.tokenSecret, commentString);
						} else {
							Toast.makeText(SKLayout.this, "로그인해주세요!",
									Toast.LENGTH_SHORT).show();
						}
						comment.setText("");
					}
				});
			}
		});
		CheckBox Rbutton2 = (CheckBox) findViewById(R.id.skcheckBox2);
		Rbutton2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				ImageButton button1 = (ImageButton) findViewById(R.id.sktweet);
				button1.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						hash.ResultTag = "#기아타이거즈2";
						String commentString = hash.ResultTag + " "
								+ comment.getText().toString();
						if (STATICVALUES.token != null
								&& STATICVALUES.tokenSecret != null) {
							RegistAsyncTask registAsyncTask = new RegistAsyncTask(
									SKLayout.this);
							registAsyncTask.execute(STATICVALUES.token,
									STATICVALUES.tokenSecret, commentString);
						} else {
							Toast.makeText(SKLayout.this, "로그인해주세요",
									Toast.LENGTH_SHORT).show();
						}
						comment.setText("");
					}
				});
			}
		});
		ImageButton tbutton = (ImageButton) findViewById(R.id.sktweet2);
		tbutton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				menu2.setVisibility(View.VISIBLE);
				menu3.setVisibility(View.VISIBLE);

			}
		});

		ImageButton Timebutton = (ImageButton) findViewById(R.id.sktimeline);
		Timebutton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// menu2.setVisibility(View.VISIBLE);
				// menu3.setVisibility(View.VISIBLE);
				AlertDialog diaBox = createDialogBox();
				diaBox.show();
				// top.setVisibility(View.INVISIBLE);
				// menu2.setVisibility(View.VISIBLE);
				// mapView.setVisibility(View.INVISIBLE);
				// TimeLine2.setVisibility(View.VISIBLE);
				// listview.setVisibility(View.INVISIBLE);

			}
		});

		/** 구글 맵 버튼 */
		ImageButton ssskbutton4 = (ImageButton) findViewById(R.id.skbutton4);
		ssskbutton4.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// top.setVisibility(View.INVISIBLE);
				menu2.setVisibility(View.INVISIBLE);
				menu3.setVisibility(View.INVISIBLE);
				SKmapView.setVisibility(View.VISIBLE);
				SKmapView.setBuiltInZoomControls(true);// zoom controller
				mc = SKmapView.getController();

				if (SKLayout.SKSelect == true) {
					SKpoint = new GeoPoint(35881227, 128586211);// 삼성 경기장
																		// 좌표 설정
					mc.animateTo(SKpoint);// 좌표점 이동
					mc.setZoom(18);// 확대 1~21

					SKlist = SKmapView.getOverlays();
					// 위치에 이미지 설정한다.
					Drawable drawable = getResources().getDrawable(
							R.drawable.maplotte);
					OverlayItem item = new OverlayItem(SKpoint,
							"대구시민야구경기장", "삼성라이온즈");
					MapOverlay map = new MapOverlay(drawable,SKLayout.this);
					map.addItem(item);
					SKlist.add(map);

					SKpoint2 = new GeoPoint(35880238, 128586076);// 우성식당
					Drawable drawable2 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item2 = new OverlayItem(SKpoint2, "맛집",
							"우성식당");
					MapOverlay map2 = new MapOverlay(drawable2,SKLayout.this);
					map2.addItem(item2);
					SKlist.add(map2);

					SKpoint3 = new GeoPoint(35880153, 128586318);
					Drawable drawable3 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item3 = new OverlayItem(SKpoint3, "맛집",
							"아송식당");
					MapOverlay map3 = new MapOverlay(drawable3,SKLayout.this);
					map3.addItem(item3);
					SKlist.add(map3);

					SKpoint4 = new GeoPoint(35880255, 128585706);
					Drawable drawable4 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item4 = new OverlayItem(SKpoint4, "맛집",
							"청정우");
					MapOverlay map4 = new MapOverlay(drawable4,SKLayout.this);
					map4.addItem(item4);
					SKlist.add(map4);

					SKpoint5 = new GeoPoint(35880179, 128585055);
					Drawable drawable5 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item5 = new OverlayItem(SKpoint5, "맛집",
							"종국이두마리치킨");
					MapOverlay map5 = new MapOverlay(drawable5,SKLayout.this);
					map5.addItem(item5);
					SKlist.add(map5);

					SKpoint6 = new GeoPoint(35880277, 128584886);
					Drawable drawable6 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item6 = new OverlayItem(SKpoint6, "맛집",
							"영천식당");
					MapOverlay map6 = new MapOverlay(drawable6,SKLayout.this);
					map6.addItem(item6);
					SKlist.add(map6);

					SKpoint7 = new GeoPoint(35880536, 129594709);
					Drawable drawable7 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item7 = new OverlayItem(SKpoint7, "맛집",
							"석미궁");
					MapOverlay map7 = new MapOverlay(drawable7,SKLayout.this);
					map7.addItem(item7);
					SKlist.add(map7);

					SKpoint8 = new GeoPoint(35880364, 128584352);
					Drawable drawable8 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item8 = new OverlayItem(SKpoint8, "맛집",
							"지붕위에파닭");
					MapOverlay map8 = new MapOverlay(drawable8,SKLayout.this);
					map8.addItem(item8);
					SKlist.add(map8);

					SKpoint9 = new GeoPoint(35880673, 128584065);
					Drawable drawable9 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item9 = new OverlayItem(SKpoint9, "맛집",
							"본가조방낙지");
					MapOverlay map9 = new MapOverlay(drawable9,SKLayout.this);
					map9.addItem(item9);
					SKlist.add(map9);

					SKpoint10 = new GeoPoint(35880734, 128584719);
					Drawable drawable10 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item10 = new OverlayItem(SKpoint10, "맛집",
							"솔밭");
					MapOverlay map10 = new MapOverlay(drawable10,SKLayout.this);
					map10.addItem(item10);
					SKlist.add(map10);

					SKpoint11 = new GeoPoint(35881233, 128585100);
					Drawable drawable11 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item11 = new OverlayItem(SKpoint11, "맛집",
							"예가손가손칼국수");
					MapOverlay map11 = new MapOverlay(drawable11,SKLayout.this);
					map11.addItem(item11);
					SKlist.add(map11);

					SKpoint12 = new GeoPoint(35882231, 128586557);
					Drawable drawable12 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item12 = new OverlayItem(SKpoint12, "맛집",
							"셔브티쓰리");
					MapOverlay map12 = new MapOverlay(drawable12,SKLayout.this);
					map12.addItem(item12);
					SKlist.add(map12);

					SKpoint13 = new GeoPoint(35883974, 128586323);
					Drawable drawable13 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item13 = new OverlayItem(SKpoint13, "맛집",
							"놀부부대찌게엔철판구이");
					MapOverlay map13 = new MapOverlay(drawable13,SKLayout.this);
					map13.addItem(item13);
					SKlist.add(map13);

					SKpoint14 = new GeoPoint(35883898, 128586066);
					Drawable drawable14 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item14 = new OverlayItem(SKpoint14, "맛집",
							"왕가네손짜장");
					MapOverlay map14 = new MapOverlay(drawable14,SKLayout.this);
					map14.addItem(item14);
					SKlist.add(map14);

					SKpoint15 = new GeoPoint(35883391, 128588757);
					Drawable drawable15 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item15 = new OverlayItem(SKpoint15, "맛집",
							"둘둘치킨");
					MapOverlay map15 = new MapOverlay(drawable15,SKLayout.this);
					map15.addItem(item15);
					SKlist.add(map15);

					SKpoint16 = new GeoPoint(3588261, 128590422);
					Drawable drawable16 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item16 = new OverlayItem(SKpoint16, "맛집",
							"대루강자탕");
					MapOverlay map16 = new MapOverlay(drawable16,SKLayout.this);
					map16.addItem(item16);
					SKlist.add(map16);

					SKpoint17 = new GeoPoint(35882913, 128590856);
					Drawable drawable17 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item17 = new OverlayItem(SKpoint17, "맛집",
							"어븐에꾸운닭");
					MapOverlay map17 = new MapOverlay(drawable17,SKLayout.this);
					map17.addItem(item17);
					SKlist.add(map17);

					SKpoint18 = new GeoPoint(35882896, 128591071);
					Drawable drawable18 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item18 = new OverlayItem(SKpoint18, "맛집",
							"피자헛");
					MapOverlay map18 = new MapOverlay(drawable18,SKLayout.this);
					map18.addItem(item18);
					SKlist.add(map18);

					SKpoint19 = new GeoPoint(35879949, 128587149);
					Drawable drawable19 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item19 = new OverlayItem(SKpoint19, "맛집",
							"조아조아");
					MapOverlay map19 = new MapOverlay(drawable19,SKLayout.this);
					map19.addItem(item19);
					SKlist.add(map19);

					SKpoint20 = new GeoPoint(35880058, 128586903);
					Drawable drawable20 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item20 = new OverlayItem(SKpoint20, "맛집",
							"부국통닭");
					MapOverlay map20 = new MapOverlay(drawable20,SKLayout.this);
					map20.addItem(item20);
					SKlist.add(map20);

					SKpoint21 = new GeoPoint(35881985, 128584776);
					Drawable drawable21 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item21 = new OverlayItem(SKpoint21, "맛집",
							"안동찜닭");
					MapOverlay map21 = new MapOverlay(drawable21,SKLayout.this);
					map21.addItem(item21);
					SKlist.add(map21);

					SKpoint22 = new GeoPoint(35882231, 128587375);
					Drawable drawable22 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item22 = new OverlayItem(SKpoint22, "맛집",
							"미즈치킨");
					MapOverlay map22 = new MapOverlay(drawable22,SKLayout.this);
					map22.addItem(item22);
					SKlist.add(map22);

				}

				// startLocationService();

			}

		});
		/** 홈으로 가기 버튼 */
		ImageButton skbutton5 = (ImageButton) findViewById(R.id.skbutton5);
		skbutton5.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent myIntent3 = new Intent(SKLayout.this,
						MainActivity.class);
				myIntent3.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				myIntent3.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
				startActivity(myIntent3);
				finish();
			}
		});
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

	}

	private LocalActivityManager getLocalActivityManager() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected boolean isRouteDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}

	private void startLocationService() {
		LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		Location location = manager
				.getLastKnownLocation(LocationManager.GPS_PROVIDER);

		GPSListener gpsListener = new GPSListener();
		long minTime = 10000;
		float minDistance = 0;

		manager.requestLocationUpdates(LocationManager.GPS_PROVIDER, minTime,
				minDistance, gpsListener);

	}

	private class GPSListener implements LocationListener {

		public void onLocationChanged(Location location) {
			Double latitude = location.getLatitude();
			Double longitude = location.getLongitude();

			showCurrentLocation(latitude, longitude);
		}

		public void onProviderDisabled(String provider) {
		}

		public void onProviderEnabled(String provider) {
		}

		public void onStatusChanged(String provider, int status, Bundle extras) {
		}
	}

	/**
	 * void showCurrentLocation
	 * 
	 * @param latitude
	 *            : Double type
	 * @param longitude
	 *            : Double type
	 */

	private void showCurrentLocation(Double latitude, Double longitude) {
		double intLatitude = latitude.doubleValue() * 1000000;
		double inLongitude = longitude.doubleValue() * 1000000;

		GeoPoint geoPt = new GeoPoint((int) intLatitude, (int) inLongitude);

		MapController controller = SKmapView.getController();
		controller.animateTo(geoPt);

		int maxZoomlevel = SKmapView.getMaxZoomLevel();
		int zoomLevel = (int) ((maxZoomlevel + 1) / 1.15);

		controller.setZoom(zoomLevel);
		controller.setCenter(geoPt);
		SKmapView.setSatellite(false);
		SKmapView.setTraffic(false);

	}

	/**
	 * void onActivityResult save result of twitter activity
	 * 
	 * @param requestCode
	 *            : int type
	 * @param resultCode
	 *            : int type
	 * @param data
	 *            : Intent type
	 */
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		Log.e("DAY", "Resultcode : " + resultCode);

		if (resultCode == RESULT_OK) {

			STATICVALUES.token = data.getStringExtra("token");
			STATICVALUES.tokenSecret = data.getStringExtra("tokenSecret");
			STATICVALUES.nick = data.getStringExtra("nick");

			Toast.makeText(this, STATICVALUES.nick + "로그인 되었습니다.!",
					Toast.LENGTH_SHORT).show();

			Log.e("DAY", STATICVALUES.token);
			Log.e("DAY", STATICVALUES.tokenSecret);
			Log.e("DAY", STATICVALUES.nick);

		}

	}

	// 버튼 리스너
	OnClickListener listener = new OnClickListener() {

		@Override
		public void onClick(View arg0) {
			hp.open(384);
		}

	};

	// 업데이트하기
	private void listUpdate() {
		sa.notifyDataSetChanged();
	}
	public AlertDialog createDialogBox() {
		// TODO Auto-generated method stub
		AlertDialog myDialog = new AlertDialog.Builder(this){
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(SKLayout.this, timeline.class);
				startActivityForResult(intent, 1004);
			}
		}
		.create();

		return myDialog;
	}
	

}
