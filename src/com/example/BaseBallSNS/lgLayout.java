package com.example.BaseBallSNS;

import java.util.*;
import java.util.zip.Inflater;

import com.example.BaseBallSNS.R;
import com.google.android.maps.*;

import android.app.*;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import android.location.*;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.*;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.*;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.graphics.drawable.Drawable;

import com.google.android.maps.*;
import com.google.android.maps.MapView.LayoutParams;

/**
 * @author LeeJungsu
 * @param kiaSelect
 *            : boolean Type.
 */

public class lgLayout extends MapActivity implements OnClickListener {
	View lgTimeLine2, lgmenu2, lgmenu3;
	ListView lglistview;
	MapView lgmapView; // //맵뷰 객체
	MapController lgmc;
	GeoPoint lgpoint, lgpoint2, lgpoint3, lgpoint4, lgpoint5,
			lgpoint6, lgpoint7, lgpoint8, lgpoint9, lgpoint10,
			lgpoint11, lgpoint12, lgpoint13, lgpoint14,
			lgpoint15, lgpoint16, lgpoint17;
	List<Overlay> lglist; // 맵 오버레이 포함하는 리스트
	EditText lgcomment;
	ArrayList<MyItem> lgarItem;
	private ArrayList<HashMap<String, String>> lgdata;
	private Button lgbt_open;
	private ListView lglistview2;
	private SimpleAdapter lgsa;
	private Net_HTMLParse lghp;
	public static HashMap<String, String> lgmap = new HashMap<String, String>();

	
	private final Handler lghandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			listUpdate();
		}
	};

	public static boolean lgSelect = true;

	@Override
	/**
	 * onCreate()
	 * 화면 생성 함수, void type
	 * @author Lee Jung Su
	 * @param Bundle savedInstanceState
	 */
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lg);

		// top = findViewById(R.id.top);
		// top.setVisibility(View.INVISIBLE);
		lgmapView = (MapView) findViewById(R.id.lgmapview);
		lgmapView.setVisibility(View.INVISIBLE);
		// TimeLine2 = findViewById(R.id.list);
		// TimeLine2.setVisibility(View.INVISIBLE);
		lgmenu2 = findViewById(R.id.lgmenu2);
		lgmenu2.setVisibility(View.INVISIBLE);
		lgmenu3 = findViewById(R.id.lgmenu3);
		lgmenu3.setVisibility(View.INVISIBLE);
		lglistview = (ListView) findViewById(R.id.lglist);
		lglistview.setVisibility(View.INVISIBLE);
		lglistview2 = (ListView) findViewById(R.id.lglist2);
		lglistview2.setVisibility(View.VISIBLE);

		// hp.open();

		lgdata = new ArrayList<HashMap<String, String>>();

		// bt_open = (Button)findViewById(R.id.bt_open);

		lghp = new Net_HTMLParse(this, lghandler, lgdata);

		lgsa = new SimpleAdapter(this, lgdata, R.layout.homeview, new String[] {
				"date", "team1", "score","team2","time","where" }, new int[] { R.id.date, R.id.team1,
				R.id.score, R.id.team2, R.id.time, R.id.where });

		// listview2.setOnClickListener(listener);
		lglistview2.setAdapter(lgsa);
		lghp.open(385);
		/* 홈버튼 */
		ImageButton lgbutton1 = (ImageButton) findViewById(R.id.lgbutton1);
		lgbutton1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				lglistview.setVisibility(View.INVISIBLE);
				lgmapView.setVisibility(View.INVISIBLE);
				// top.setVisibility(View.INVISIBLE);
				lgmenu2.setVisibility(View.INVISIBLE);
				lgmenu3.setVisibility(View.INVISIBLE);

				lghp.open(385);

				lglistview2.setVisibility(View.VISIBLE);
				// startActivity(myIntent);
			}
		});
//		TeamAudio Kiaaudio = new TeamAudio();
//		lgarItem = new ArrayList<MyItem>();
//		MyItem mi;
//		mi = new MyItem(R.drawable.lgkim, "김진욱", "감독", "1960년 08월05일",
//				"182cm / 87kg",lgmap.get("이승엽"),"가사가사가사");
//		lgarItem.add(mi);
//		mi = new MyItem(R.drawable.lgplayer1, "정재훈", "투수", "1980년 01월01일",
//				"179cm / 81kg",lgmap.get("이승엽"),"가사가사가사");
//		lgarItem.add(mi);
//		mi = new MyItem(R.drawable.lgplayer2, "김선우", "투수", "1977년 09월04일",
//				"184cm / 87kg",lgmap.get("이승엽"),"가사가사가사");
//		lgarItem.add(mi);
//		mi = new MyItem(R.drawable.lgplayer3, "김상현", "투수", "1980년04월07일",
//				"181cm / 82kg",lgmap.get("이승엽"),"가사가사가사");
//		lgarItem.add(mi);
//		mi = new MyItem(R.drawable.lgplayer4, "김강률", "투수", "1988년 08월28일",
//				"187cm / 95kg",lgmap.get("이승엽"),"가사가사가사");
//		lgarItem.add(mi);
//		mi = new MyItem(R.drawable.lgplayer5, "올슨", "투수", "1983년 10월18일",
//				"185cm / 93kg",lgmap.get("이승엽"),"가사가사가사");
//		lgarItem.add(mi);
//		mi = new MyItem(R.drawable.lgplayer6, "임태훈", "투수", " 1988년 09월28일",
//				"182cm / 83kg",lgmap.get("이승엽"),"가사가사가사");
//		lgarItem.add(mi);
//		mi = new MyItem(R.drawable.lgplayer7, "이재우", "투수", " 1980년 02월09일",
//				"183cm / 82kg",lgmap.get("이승엽"),"가사가사가사");
//		lgarItem.add(mi);
//		mi = new MyItem(R.drawable.lgplayer8, "니퍼드", "투수", "1981년 05월06일",
//				"203cm / 103kg",lgmap.get("이승엽"),"가사가사가사");
//		lgarItem.add(mi);
//		mi = new MyItem(R.drawable.lgplayer9, "오현택", "투수", "1985년 07월17일",
//				"180cm / 73kg",lgmap.get("이승엽"),"가사가사가사");
//		lgarItem.add(mi);
//		mi = new MyItem(R.drawable.lgplayer10, "양의지	", "포수", "1987년 06월05일",
//				"179cm / 85kg",lgmap.get("이승엽"),"가사가사가사");
//		lgarItem.add(mi);
//		mi = new MyItem(R.drawable.lgplayer11, "최재훈", "포수",
//				"1989년 08월 27일", "178cm / 76kg",lgmap.get("이승엽"),"가사가사가사");
//		lgarItem.add(mi);
//		mi = new MyItem(R.drawable.lgplayer12, "최준석", "내야수",
//				"1983년 02월15일", "185cm / 115kg",lgmap.get("이승엽"),"가사가사가사");
//		lgarItem.add(mi);
//		mi = new MyItem(R.drawable.lgplayer13, "김재호", "내야수",
//				"1985년 03월21일", "181cm / 75kg",lgmap.get("이승엽"),"가사가사가사");
//		lgarItem.add(mi);
//		mi = new MyItem(R.drawable.lgplayer14, "홍성흔", "내야수",
//				"1977년 02월28일", "180cm / 96kg",lgmap.get("이승엽"),"가사가사가사");
//		lgarItem.add(mi);
//		mi = new MyItem(R.drawable.lgplayer15, "최주환", "내야수",
//				"1988년 02월28일", "178cm / 73kg",lgmap.get("이승엽"),"가사가사가사");
//		lgarItem.add(mi);
//		mi = new MyItem(R.drawable.lgplayer16, "허경민", "내야수",
//				"1990년 08월26일", "176cm / 69kg",lgmap.get("이승엽"),"가사가사가사");
//		lgarItem.add(mi);
//		mi = new MyItem(R.drawable.lgplayer17, "윤석민", "내야수",
//				"1985년 09월04일", "180cm / 86kg",lgmap.get("이승엽"),"가사가사가사");
//		lgarItem.add(mi);
//		mi = new MyItem(R.drawable.lgplayer18, "오재원", "내야수",
//				"1985년 02월09일", "185cm / 75kg",lgmap.get("이승엽"),"가사가사가사");
//		lgarItem.add(mi);
//		mi = new MyItem(R.drawable.lgplayer19, "박건우", "외야수",
//				"1990년 09월08일", "184cm / 80kg",lgmap.get("이승엽"),"가사가사가사");
//		lgarItem.add(mi);
//		mi = new MyItem(R.drawable.lgplayer20, "민병헌", "외야수",
//				"1987년 03월10일", "178cm / 78kg",lgmap.get("이승엽"),"가사가사가사");
//		lgarItem.add(mi);
//		mi = new MyItem(R.drawable.lgplayer21, "김현수", "외야수",
//				"1988년 01월12일", "188cm / 100kg",lgmap.get("이승엽"),"가사가사가사");
//		lgarItem.add(mi);
//		mi = new MyItem(R.drawable.lgplayer22, "이종욱", "외야수",
//				"1980년 06월18일", "176cm / 77kg",lgmap.get("이승엽"),"가사가사가사");
//		lgarItem.add(mi);
//		mi = new MyItem(R.drawable.lgplayer23, "정수빈", "외야수",
//				"1990년 10월07일", "175cm / 70kg",lgmap.get("이승엽"),"가사가사가사");
//		lgarItem.add(mi);
//		MyListAdapter MyAdapter = new MyListAdapter(this, R.layout.kiaplayer,
//				lgarItem);
//
//		ListView lgMyList;
//		lgMyList = (ListView) findViewById(R.id.lglist);
//		lgMyList.setAdapter(MyAdapter);

		/** 선수정보 버튼 */
		ImageButton lgbutton2 = (ImageButton) findViewById(R.id.lgbutton2);
		lgbutton2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				lglistview.setVisibility(View.VISIBLE);
				lgmapView.setVisibility(View.INVISIBLE);
				// top.setVisibility(View.INVISIBLE);
				lgmenu2.setVisibility(View.INVISIBLE);
				lgmenu3.setVisibility(View.INVISIBLE);

				// startActivity(myIntent);
			}
		});

		/** 트위터 버튼 */
		ImageButton lgbutton3 = (ImageButton) findViewById(R.id.lgbutton3);
		lgbutton3.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				lgmenu2.setVisibility(View.VISIBLE);
				lgmenu3.setVisibility(View.VISIBLE);
				lgmapView.setVisibility(View.INVISIBLE);
				lglistview.setVisibility(View.INVISIBLE);
				lglistview2.setVisibility(View.INVISIBLE);
				// TimeLine2.setVisibility(View.INVISIBLE);

			}
		});
		/** 트위터 로그인버튼 */
		ImageButton button = (ImageButton) findViewById(R.id.lgtlogin);
		button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent myIntent = new Intent(lgLayout.this,
						TwitterActivity.class);
				startActivityForResult(myIntent, 1004);
				// startActivity(myIntent);
			}
		});
		/** 트위터 글쓰기버튼 */
		final HashTag hash = new HashTag("");
		ImageButton lgButton1 = (ImageButton) findViewById(R.id.lgtweet);
		lgcomment = (EditText) findViewById(R.id.lgcomment);
		lgButton1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				String commentString = lgcomment.getText().toString();
				if (STATICVALUES.token != null
						&& STATICVALUES.tokenSecret != null) {
					RegistAsyncTask registAsyncTask = new RegistAsyncTask(
							lgLayout.this);
					registAsyncTask.execute(STATICVALUES.token,
							STATICVALUES.tokenSecret, commentString);
				} else {
					Toast.makeText(lgLayout.this, "로그인해주세요!",
							Toast.LENGTH_SHORT).show();
				}
				lgcomment.setText("");

			}
		});
		CheckBox lgRbutton1 = (CheckBox) findViewById(R.id.lgcheckBox1);
		lgRbutton1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				ImageButton lgButton1 = (ImageButton) findViewById(R.id.lgtweet);
				lgButton1.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						hash.ResultTag = "#기아타이거즈";
						String commentString = hash.MakeHash("#기아타이거즈") + " "
								+ lgcomment.getText().toString();
						if (STATICVALUES.token != null
								&& STATICVALUES.tokenSecret != null) {
							RegistAsyncTask registAsyncTask = new RegistAsyncTask(
									lgLayout.this);
							registAsyncTask.execute(STATICVALUES.token,
									STATICVALUES.tokenSecret, commentString);
						} else {
							Toast.makeText(lgLayout.this, "로그인해주세요!",
									Toast.LENGTH_SHORT).show();
						}
						lgcomment.setText("");
					}
				});
			}
		});
		CheckBox lgRbutton2 = (CheckBox) findViewById(R.id.lgcheckBox2);
		lgRbutton2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				ImageButton lgButton1 = (ImageButton) findViewById(R.id.lgtweet);
				lgButton1.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						hash.ResultTag = "#기아타이거즈2";
						String commentString = hash.ResultTag + " "
								+ lgcomment.getText().toString();
						if (STATICVALUES.token != null
								&& STATICVALUES.tokenSecret != null) {
							RegistAsyncTask registAsyncTask = new RegistAsyncTask(
									lgLayout.this);
							registAsyncTask.execute(STATICVALUES.token,
									STATICVALUES.tokenSecret, commentString);
						} else {
							Toast.makeText(lgLayout.this, "로그인해주세요",
									Toast.LENGTH_SHORT).show();
						}
						lgcomment.setText("");
					}
				});
			}
		});
		ImageButton lgTbutton = (ImageButton) findViewById(R.id.lgtweet);
		lgTbutton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				lgmenu2.setVisibility(View.VISIBLE);
				lgmenu3.setVisibility(View.VISIBLE);

			}
		});

		ImageButton lgTimebutton = (ImageButton) findViewById(R.id.lgtimeline);
		lgTimebutton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				//lgmenu2.setVisibility(View.VISIBLE);
				// menu3.setVisibility(View.VISIBLE);
				timeline.searchTerm="#두산베어스";
				Intent intent = new Intent(lgLayout.this, timeline.class);
				startActivity(intent);
				// top.setVisibility(View.INVISIBLE);
				// menu2.setVisibility(View.VISIBLE);
				//lgmapView.setVisibility(View.INVISIBLE);
				// TimeLine2.setVisibility(View.VISIBLE);
				//lglistview.setVisibility(View.INVISIBLE);

			}
		});

		/** 구글 맵 버튼 */
		ImageButton lgMbutton4 = (ImageButton) findViewById(R.id.lgbutton4);
		lgMbutton4.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// top.setVisibility(View.INVISIBLE);
				lgmenu2.setVisibility(View.INVISIBLE);
				lgmenu3.setVisibility(View.INVISIBLE);
				lgmapView.setVisibility(View.VISIBLE);
				lgmapView.setBuiltInZoomControls(true);// zoom controller
				lgmc = lgmapView.getController();

				if (lgLayout.lgSelect == true) {
					lgpoint = new GeoPoint(35193819, 129061471);// 기아 경기장 좌표
																	// 설정
					lgmc.animateTo(lgpoint);// 좌표점 이동
					lgmc.setZoom(17);// 확대 1~21

					lglist = lgmapView.getOverlays();
					// 위치에 이미지 설정한다.
					Drawable drawable = getResources().getDrawable(
							R.drawable.maplg);
					OverlayItem item = new OverlayItem(lgpoint, "부산사직야구장",
							"롯데자이언츠");
					MapOverlay map = new MapOverlay(drawable,lgLayout.this);
					map.addItem(item);
					lglist.add(map);

					lgpoint2 = new GeoPoint(35196074, 129059442);// 기아 경기장 좌표
																	// 설정
					Drawable drawable2 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item2 = new OverlayItem(lgpoint2, "맛집",
							"주문진막국수");
					MapOverlay map2 = new MapOverlay(drawable2,lgLayout.this);
					map2.addItem(item2);
					lglist.add(map2);

					lgpoint3 = new GeoPoint(35195541, 129058750);// 기아 경기장 좌표
																	// 설정
					Drawable drawable3 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item3 = new OverlayItem(lgpoint3, "맛집",
							"안양해물탕");
					MapOverlay map3 = new MapOverlay(drawable3,lgLayout.this);
					map3.addItem(item3);
					lglist.add(map3);

					lgpoint4 = new GeoPoint(35198551, 129064490);// 기아 경기장 좌표
																	// 설정
					Drawable drawable4 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item4 = new OverlayItem(lgpoint4, "맛집",
							"금강만두");
					MapOverlay map4 = new MapOverlay(drawable4,lgLayout.this);
					map4.addItem(item4);
					lglist.add(map4);

					lgpoint5 = new GeoPoint(35193521,129062002);
					Drawable drawable5 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item5 = new OverlayItem(lgpoint5, "맛집",
							"롯데리아");
					MapOverlay map5 = new MapOverlay(drawable5,lgLayout.this);
					map5.addItem(item5);
					lglist.add(map5);

					lgpoint6 = new GeoPoint(35193706,129064776);
					Drawable drawable6 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item6 = new OverlayItem(lgpoint6, "맛집",
							"서영식당");
					MapOverlay map6 = new MapOverlay(drawable6,lgLayout.this);
					map6.addItem(item6);
					lglist.add(map6);

					lgpoint7 = new GeoPoint(35195506,129064009);
					Drawable drawable7 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item7 = new OverlayItem(lgpoint7, "맛집",
							"마포진연탄구이");
					MapOverlay map7 = new MapOverlay(drawable7,lgLayout.this);
					map7.addItem(item7);
					lglist.add(map7);

					lgpoint8 = new GeoPoint(35195726,129063939);
					Drawable drawable8 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item8 = new OverlayItem(lgpoint8, "맛집",
							"비비큐치킨");
					MapOverlay map8 = new MapOverlay(drawable8,lgLayout.this);
					map8.addItem(item8);
					lglist.add(map8);

					lgpoint9 = new GeoPoint(35195423,129065452);
					Drawable drawable9 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item9 = new OverlayItem(lgpoint9, "맛집",
							"류가네더덕삼겹살");
					MapOverlay map9 = new MapOverlay(drawable9,lgLayout.this);
					map9.addItem(item9);
					lglist.add(map9);

					lgpoint10 = new GeoPoint(35195528,129064164);
					Drawable drawable10 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item10 = new OverlayItem(lgpoint10, "맛집",
							"조방낙지");
					MapOverlay map10 = new MapOverlay(drawable10,lgLayout.this);
					map10.addItem(item10);
					lglist.add(map10);

					lgpoint11 = new GeoPoint(35195436,129064577);
					Drawable drawable11 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item11 = new OverlayItem(lgpoint11, "맛집",
							"서원숯불바베큐");
					MapOverlay map11 = new MapOverlay(drawable11,lgLayout.this);
					map11.addItem(item11);
					lglist.add(map11);

					lgpoint12 = new GeoPoint(35193871,129064577);
					Drawable drawable12 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item12 = new OverlayItem(lgpoint12, "맛집",
							"대구막창왕갈비");
					MapOverlay map12 = new MapOverlay(drawable12,lgLayout.this);
					map12.addItem(item12);
					lglist.add(map12);

					lgpoint13 = new GeoPoint(35193538,129064583);
					Drawable drawable13 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item13 = new OverlayItem(lgpoint13, "맛집",
							"진우식당");
					MapOverlay map13 = new MapOverlay(drawable13,lgLayout.this);
					map13.addItem(item13);
					lglist.add(map13);

					lgpoint14 = new GeoPoint(35195940,129060618);
					Drawable drawable14 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item14 = new OverlayItem(lgpoint14, "맛집",
							"맥도날드");
					MapOverlay map14 = new MapOverlay(drawable14,lgLayout.this);
					map14.addItem(item14);
					lglist.add(map14);

					lgpoint15 = new GeoPoint(35196300,129060640);
					Drawable drawable15 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item15 = new OverlayItem(lgpoint15, "맛집",
							"미스터피자");
					MapOverlay map15 = new MapOverlay(drawable15,lgLayout.this);
					map15.addItem(item15);
					lglist.add(map15);

					lgpoint16 = new GeoPoint(35195971,129059567);
					Drawable drawable16 = getResources().getDrawable(
							R.drawable.marker);
					OverlayItem item16 = new OverlayItem(lgpoint16, "맛집",
							"주문진식당");
					MapOverlay map16 = new MapOverlay(drawable16,lgLayout.this);
					map16.addItem(item16);
					lglist.add(map16);



				}

				// startLocationService();

			}

		});
		/** 홈으로 가기 버튼 */
		ImageButton lgHbutton5 = (ImageButton) findViewById(R.id.lgbutton5);
		lgHbutton5.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent myIntent3 = new Intent(lgLayout.this,
						MainActivity.class);
				myIntent3.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				myIntent3.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
				startActivity(myIntent3);
				finish();
			}
		});
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

	}

	private LocalActivityManager getLocalActivityManager() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected boolean isRouteDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}

	private void startLocationService() {
		LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		Location location = manager
				.getLastKnownLocation(LocationManager.GPS_PROVIDER);

		GPSListener gpsListener = new GPSListener();
		long minTime = 10000;
		float minDistance = 0;

		manager.requestLocationUpdates(LocationManager.GPS_PROVIDER, minTime,
				minDistance, gpsListener);

	}

	private class GPSListener implements LocationListener {

		public void onLocationChanged(Location location) {
			Double latitude = location.getLatitude();
			Double longitude = location.getLongitude();

			showCurrentLocation(latitude, longitude);
		}

		public void onProviderDisabled(String provider) {
		}

		public void onProviderEnabled(String provider) {
		}

		public void onStatusChanged(String provider, int status, Bundle extras) {
		}
	}

	/**
	 * void showCurrentLocation
	 * 
	 * @param latitude
	 *            : Double type
	 * @param longitude
	 *            : Double type
	 */

	private void showCurrentLocation(Double latitude, Double longitude) {
		double intLatitude = latitude.doubleValue() * 1000000;
		double inLongitude = longitude.doubleValue() * 1000000;

		GeoPoint geoPt = new GeoPoint((int) intLatitude, (int) inLongitude);

		MapController controller = lgmapView.getController();
		controller.animateTo(geoPt);

		int maxZoomlevel = lgmapView.getMaxZoomLevel();
		int zoomLevel = (int) ((maxZoomlevel + 1) / 1.15);

		controller.setZoom(zoomLevel);
		controller.setCenter(geoPt);
		lgmapView.setSatellite(false);
		lgmapView.setTraffic(false);

	}

	/**
	 * void onActivityResult save result of twitter activity
	 * 
	 * @param requestCode
	 *            : int type
	 * @param resultCode
	 *            : int type
	 * @param data
	 *            : Intent type
	 */
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		Log.e("DAY", "Resultcode : " + resultCode);

		if (resultCode == RESULT_OK) {

			STATICVALUES.token = data.getStringExtra("token");
			STATICVALUES.tokenSecret = data.getStringExtra("tokenSecret");
			STATICVALUES.nick = data.getStringExtra("nick");

			Toast.makeText(this, STATICVALUES.nick + "로그인 되었습니다.!",
					Toast.LENGTH_SHORT).show();

			Log.e("DAY", STATICVALUES.token);
			Log.e("DAY", STATICVALUES.tokenSecret);
			Log.e("DAY", STATICVALUES.nick);

		}

	}

	// 버튼 리스너
	OnClickListener listener = new OnClickListener() {

		@Override
		public void onClick(View arg0) {
			lghp.open(385);
		}

	};

	// 업데이트하기
	private void listUpdate() {
		lgsa.notifyDataSetChanged();
	}

}
