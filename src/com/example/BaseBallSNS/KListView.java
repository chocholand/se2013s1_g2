package com.example.BaseBallSNS;

import java.util.*;

import com.example.BaseBallSNS.R;
import android.app.*;
import android.content.*;
import android.media.*;
import android.os.*;
import android.view.*;
import android.widget.*;



class MyItem {
	MyItem(int aIcon, String aName,String apositions, String abirthday, String awheight,String songs, String lyn) {
		Icon = aIcon;
		Name = aName;
		positions = apositions;
		birthday = abirthday;
		wheight = awheight;
		song = songs;
		this.lyn=lyn;
	}
	int Icon;
	String Name;
	String positions;
	String birthday;
	String wheight;
	String song;
	String lyn;
	
	public static void goLyn(Context maincon, String Name, String lyn){
		new AlertDialog.Builder(maincon)
		.setTitle(Name + " 응원가사")
		.setMessage(lyn)
		.setPositiveButton("종료", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
			
			}
		})
		.show();
	} 
}

class MyListAdapter extends BaseAdapter {
	Context maincon;
	LayoutInflater Inflater;
	ArrayList<MyItem> arSrc;
	int layout;
	ImageButton btn;

	public MyListAdapter(Context context, int alayout, ArrayList<MyItem> aarSrc) {
		maincon = context;
		Inflater = (LayoutInflater)context.getSystemService(
				Context.LAYOUT_INFLATER_SERVICE);
		arSrc = aarSrc;
		layout = alayout;
	}

	public int getCount() {
		return arSrc.size();
	}

	public String getItem(int position) {
		return arSrc.get(position).Name;
	}

	public long getItemId(int position) {
		return position;
	}

	// 媛���ぉ��酉����
	public View getView(int position, View convertView, ViewGroup parent) {
		final int pos = position;
		if (convertView == null) {
			convertView = Inflater.inflate(layout, parent, false);
		}
		ImageView img = (ImageView)convertView.findViewById(R.id.picture);
		img.setImageResource(arSrc.get(position).Icon);

		TextView txt = (TextView)convertView.findViewById(R.id.name);
		txt.setText(arSrc.get(position).Name);
		
		TextView txt1 = (TextView)convertView.findViewById(R.id.position);
		txt1.setText(arSrc.get(position).positions);
		
		TextView txt2 = (TextView)convertView.findViewById(R.id.birthday);
		txt2.setText(arSrc.get(position).birthday);
		
		TextView txt3 = (TextView)convertView.findViewById(R.id.wheight);
		txt3.setText(arSrc.get(position).wheight);

		btn = (ImageButton)convertView.findViewById(R.id.btnlike);
		btn.setOnClickListener(new Button.OnClickListener() {
			public void onClick(View v) {
				arSrc.get(pos);
				MyItem.goLyn(maincon, arSrc.get(pos).Name, arSrc.get(pos).lyn);
			}
		});
		
		ImageButton btn1 = (ImageButton)convertView.findViewById(R.id.btnsnd);
		btn1.setOnClickListener(new Button.OnClickListener() {
			public void onClick(View v) {
				if(MainActivity.player != null) {
					MainActivity.player.release();
					MainActivity.player=null;
				}
			 

			    
			
				try{
			        MainActivity.player = new MediaPlayer();
			        MainActivity.player.setDataSource(arSrc.get(pos).song);
			        MainActivity.player.prepare();					
					MainActivity.player.start();
				}catch(Exception e){
					Toast.makeText(maincon,"error : " + e.getMessage(),0).show();
				}
			}
		});
		
		
			
		
		return convertView;
	}
}
