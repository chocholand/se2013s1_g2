
package com.example.BaseBallSNS;

import twitter4j.Twitter;


import twitter4j.TwitterException;
import twitter4j.http.AccessToken;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

/**
 * 클래스 설명
 *@see :AsynTask<String, Void, AccessToken>, TwitterActivity.java
 *@author : Day young ( dryudryu@gmail.com )
 *@version : 3.0
 *@since : 2011. 3. 4. 오후 5:51:49 update history
 */

public class OAuthTokenAsyncTask extends AsyncTask<String, Void, AccessToken> {
	
	private static final String TAG = "OAuthTokenAsyncTask";	
	
	Twitter twitter;	
	
	Context context;	
	
	ProgressDialog dialog;
	
	Handler mHandler;
	
	
	/**OAuthTokenAsyncTask
	 * 생성자
	 * @param context : Context type
	 * @param twitter : Twitter type
	 * @param mHandler : Handler type
	 */

	public OAuthTokenAsyncTask(Context context , Twitter twitter , Handler mHandler) {		
		
		Log.e(TAG, "OAuthTokenAsyncTask");
		
		this.context = context;
		this.mHandler = mHandler;
		this.twitter = twitter;
	}		

	/**void onPreExcute
	 * OAuthToken 요청에 대한 프로그래스
	 */
	@Override
	protected void onPreExecute() {
		
		super.onPreExecute();	
		
		dialog = new ProgressDialog(context);		
		dialog.setMessage("Request to OAuthToken . . .");		
		dialog.show();	
		
	}	
	
	/**void onCancelled
	 * 요청 취소
	 */
	@Override
	protected void onCancelled() {		
		super.onCancelled();
		
		dialog.dismiss();
	}
	
	
	/**AccessToken doInBackground
	 * background에서 수행할 작업을 구현해야 함. execute(…) 메소드에 입력된 인자들을 전달 받음.
	 * @param params[] : String type
	 * @return accessToken
	 */
	
	@Override
	protected AccessToken doInBackground(String... params) {
		
		// TODO Auto-generated method stub
				
		AccessToken accessToken = null;
			
		try {
			accessToken = twitter.getOAuthAccessToken(params[0],params[1], params[2]);
		} 		
		catch (TwitterException e) {
			e.printStackTrace();
		}		
		
		return accessToken;
		
	}

	/**void onPostExecute
	 * doInBackground(…)가 리턴하는 값을 바탕으로 UI스레드에 background 작업 결과를 표현하도록 구현 함.
	 * @param accessToken : AccessToken type
	 */
	
	@Override
	protected void onPostExecute(AccessToken accessToken) {
		super.onPostExecute(accessToken);
		
		if(accessToken==null)
		{		
			Message sendMsg = Message.obtain();			
			sendMsg.what = 0;
			mHandler.sendMessage(sendMsg);
		}
		else
		{
			Message sendMsg = Message.obtain();			
			sendMsg.what = 2;
			sendMsg.obj = accessToken;
			mHandler.sendMessage(sendMsg);
		}		
		
		dialog.dismiss();
	}
}
