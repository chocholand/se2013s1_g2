<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>jsonTest1</title>
<script type="text/javascript" src="json.js"></script>
<script type="text/javascript" src="xmlHttpRequest.js"></script>
<script type="text/javascript">
	var httpRequest;
	function getData(url){
		httpRequest=getXMLHttpRequest();
		httpRequest.onreadystatechange=callback;
		httpRequest.open("GET",url,true);
		httpRequest.send(null);
	}
	function callback(){
		if(httpRequest.readyState==4){
			if(httpRequest.status==200){
				var result=httpRequest.responseText;
				//json라이브러리 사용
				var data=result.parseJSON();
				for(var i=0;i<data.info.length;i++){
				var reData=data.info[i].id +","+ data.info[i].pwd;
				document.getElementById("sp").innerHTML+=reData +"<br/>";
				}
			}
		}
	}
</script>
</head>
<body>
<input type="button" value="push" onclick="getData('test3.jsp')"/><br/>
<span id="sp"></span>
</body>
</html>


