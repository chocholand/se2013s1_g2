<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script type="text/javascript" src="xmlHttpRequest.js"></script>
<script type="text/javascript">
	var httpRequest;
	function getData(url){
		httpRequest = getXMLHttpRequest();
		httpRequest.onreadystatechange=callback;
		httpRequest.open("GET", url, true);
		httpRequest.send(null);
	}	
	function callback(){
		if(httpRequest.readyState==4){
			if(httpRequest.status==200){
				//응답을 텍스트 형태로 받음
				var result=httpRequest.responseText;
				//텍스트형태의 데이터를 json 형태로 변환 -->eval()메서드
				var data = eval("("+result+")");
				var reData = data.id;
				document.getElementById("sp").innerText=reData;
			}
		}
	}
</script>
</head>
<body>
<input type="button" value="push" onclick="getData('test1.jsp')"/>
<br/>
<span id="sp">
</span>

</body>
</html>